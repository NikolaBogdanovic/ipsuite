﻿namespace IPSuite.ICMP4
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.DNS;
    using IPSuite.IP4;

    /// <summary>
    /// Constants that represent ICMP4 header types.
    /// </summary>
    public enum Icmp4HeaderType
    {
        /// <summary>
        /// Echo Reply (RFC 792)
        /// </summary>
        EchoReply = 0,

        /// <summary>
        /// Destination Unreachable (RFC 792)
        /// </summary>
        DestinationUnreachable = 3,

        /// <summary>
        /// Source Quench (RFC 792, RFC 6633)
        /// </summary>
        SourceQuench = 4,

        /// <summary>
        /// Redirect (RFC 792)
        /// </summary>
        Redirect = 5,

        /// <summary>
        /// Alternate Host Address (RFC 6918)
        /// </summary>
        AlternateHostAddress = 6,

        /// <summary>
        /// Echo Request (RFC 792)
        /// </summary>
        EchoRequest = 8,

        /// <summary>
        /// Router Advertisement (RFC 1256)
        /// </summary>
        RouterAdvertisement = 9,

        /// <summary>
        /// Router Solicitation (RFC 1256)
        /// </summary>
        RouterSolicitation = 10,

        /// <summary>
        /// Time Exceeded (RFC 792)
        /// </summary>
        TimeExceeded = 11,

        /// <summary>
        /// Parameter Problem (RFC 792)
        /// </summary>
        ParameterProblem = 12,

        /// <summary>
        /// Time Stamp Request (RFC 792)
        /// </summary>
        TimeStampRequest = 13,

        /// <summary>
        /// Time Stamp Reply (RFC 792)
        /// </summary>
        TimeStampReply = 14,

        /// <summary>
        /// Information Request (RFC 792, RFC 6918)
        /// </summary>
        InformationRequest = 15,

        /// <summary>
        /// Information Reply (RFC 792, RFC 6918)
        /// </summary>
        InformationReply = 16,

        /// <summary>
        /// Address Mask Request (RFC 792, RFC 6918)
        /// </summary>
        AddressMaskRequest = 17,

        /// <summary>
        /// Address Mask Reply (RFC 792, RFC 6918)
        /// </summary>
        AddressMaskReply = 18,

        /// <summary>
        /// Trace Route (RFC 1393, RFC 6918)
        /// </summary>
        TraceRoute = 30,

        /// <summary>
        /// Conversion Failed (RFC 1475, RFC 6918)
        /// </summary>
        ConversionFailed = 31,

        /// <summary>
        /// Domain Name Request (RFC 1788, RFC 6918)
        /// </summary>
        DomainNameRequest = 37,

        /// <summary>
        /// Domain Name Reply (RFC 1788, RFC 6918)
        /// </summary>
        DomainNameReply = 38,

        /// <summary>
        /// Photuris (RFC 2521)
        /// </summary>
        Photuris = 40,

        /// <summary>
        /// Seamoby (RFC 4065)
        /// </summary>
        Seamoby = 41
    }

    /// <summary>
    /// Constants that represent ICMP4 header classes.
    /// </summary>
    public enum Icmp4HeaderClass
    {
        /// <summary>
        /// Error Message
        /// </summary>
        ErrorMessage = 0,

        /// <summary>
        /// Query Message
        /// </summary>
        QueryMessage = 1
    }

    /// <summary>
    /// Internet Control Message Protocol version 4 header (RFC 792).
    /// </summary>
    public abstract class Icmp4Header
    {
        /// <summary>
        /// Gets the header type.
        /// </summary>
        public Icmp4HeaderType HeaderType { get; private set; }

        /// <summary>
        /// Gets the header class.
        /// </summary>
        public Icmp4HeaderClass HeaderClass { get; private set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public byte Code { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the data length in bytes.
        /// </summary>
        public ushort DataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Icmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        protected Icmp4Header(BinaryReader reader, Icmp4HeaderType type)
        {
            Contract.Requires(reader != null);

            HeaderType = type;
            switch(HeaderType)
            {
                case Icmp4HeaderType.DestinationUnreachable:
                case Icmp4HeaderType.SourceQuench:
                case Icmp4HeaderType.Redirect:
                case Icmp4HeaderType.TimeExceeded:
                case Icmp4HeaderType.ParameterProblem:
                case Icmp4HeaderType.ConversionFailed:
                {
                    HeaderClass = Icmp4HeaderClass.ErrorMessage;
                    break;
                }
                default:
                {
                    HeaderClass = Icmp4HeaderClass.QueryMessage;
                    break;
                }
            }
            Code = reader.ReadByte();
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DataLength = (ushort)(reader.BaseStream.Length - 4);
        }

        /// <summary>
        /// Creates an ICMP4 header.
        /// </summary>
        /// <param name="buffer">The binary buffer.</param>
        /// <returns>
        /// An ICMP4 header.
        /// </returns>
        public static Icmp4Header Create(byte[] buffer)
        {
            Contract.Requires(buffer != null);
            Contract.Requires(buffer.Length >= 4);

            using(var ms = new MemoryStream(buffer, false))
            {
                var reader = new BinaryReader(ms);
                var type = (Icmp4HeaderType)reader.ReadByte();
                switch(type)
                {
                    case Icmp4HeaderType.EchoReply:
                    case Icmp4HeaderType.EchoRequest:
                        return new EchoRequestOrReplyIcmp4Header(reader, type);
                    case Icmp4HeaderType.DestinationUnreachable:
                        return new DestinationUnreachableErrorIcmp4Header(reader);
                    case Icmp4HeaderType.SourceQuench:
                        return new SourceQuenchErrorIcmp4Header(reader);
                    case Icmp4HeaderType.Redirect:
                        return new RedirectErrorIcmp4Header(reader);
                    case Icmp4HeaderType.RouterAdvertisement:
                        return new RouterAdvertisementIcmp4Header(reader);
                    case Icmp4HeaderType.TimeExceeded:
                        return new TimeExceededErrorIcmp4Header(reader);
                    case Icmp4HeaderType.ParameterProblem:
                        return new ParameterProblemErrorIcmp4Header(reader);
                    case Icmp4HeaderType.TimeStampRequest:
                    case Icmp4HeaderType.TimeStampReply:
                        return new TimeStampRequestOrReplyIcmp4Header(reader, type);
                    case Icmp4HeaderType.InformationRequest:
                    case Icmp4HeaderType.InformationReply:
                    case Icmp4HeaderType.DomainNameRequest:
                        return new RequestOrReplyIcmp4Header(reader, type);
                    case Icmp4HeaderType.DomainNameReply:
                        return new DomainNameRequestOrReplyIcmp4Header(reader);
                    case Icmp4HeaderType.AddressMaskRequest:
                    case Icmp4HeaderType.AddressMaskReply:
                        return new AddressMaskRequestOrReplyIcmp4Header(reader, type);
                    case Icmp4HeaderType.TraceRoute:
                        return new TraceRouteIcmp4Header(reader);
                    case Icmp4HeaderType.ConversionFailed:
                        return new ConversionFailedErrorIcmp4Header(reader);
                    case Icmp4HeaderType.Photuris:
                        return new PhoturisIcmp4Header(reader);
                    case Icmp4HeaderType.Seamoby:
                        return new SeamobyIcmp4Header(reader);
                    default:
                        return new GenericIcmp4Header(reader, type);
                }
            }
        }
    }

    /// <summary>
    /// Generic ICMP4 header (RFC 792).
    /// </summary>
    public sealed class GenericIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the header data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        public GenericIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Request Or Reply ICMP4 header (RFC 792, RFC 950, RFC 1788, RFC 6918).
    /// </summary>
    public class RequestOrReplyIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestOrReplyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of request or reply header types.</param>
        public RequestOrReplyIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp4HeaderType.InformationRequest || type == Icmp4HeaderType.InformationReply || type == Icmp4HeaderType.DomainNameRequest || type == Icmp4HeaderType.DomainNameReply || type == Icmp4HeaderType.EchoRequest || type == Icmp4HeaderType.EchoReply || type == Icmp4HeaderType.TimeStampRequest || type == Icmp4HeaderType.TimeStampReply || type == Icmp4HeaderType.AddressMaskRequest || type == Icmp4HeaderType.AddressMaskReply);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Domain Name Request Or Reply ICMP4 header (RFC 1788, RFC 6918).
    /// </summary>
    public sealed class DomainNameRequestOrReplyIcmp4Header : RequestOrReplyIcmp4Header
    {
        /// <summary>
        /// Gets the time to live in seconds.
        /// </summary>
        public uint TimeToLive { get; private set; }

        /// <summary>
        /// Gets the Fully Qualified Domain Names.
        /// </summary>
        public ReadOnlyCollection<string> Names { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainNameRequestOrReplyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DomainNameRequestOrReplyIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.DomainNameReply)
        {
            Contract.Requires(reader != null);

            TimeToLive = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            var names = new List<string>();
            long i = reader.BaseStream.Position + (DataLength - 4 - 4);
            while(reader.BaseStream.Position < i)
            {
                var n = DnsMessage.DomainName(reader);
                if(n != null)
                    names.Add(n);
            }
            Names = new ReadOnlyCollection<string>(names.ToArray());
        }
    }

    /// <summary>
    /// Echo Request Or Reply ICMP4 header (RFC 792).
    /// </summary>
    public sealed class EchoRequestOrReplyIcmp4Header : RequestOrReplyIcmp4Header
    {
        /// <summary>
        /// Gets the echo data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EchoRequestOrReplyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of echo header types.</param>
        public EchoRequestOrReplyIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp4HeaderType.EchoRequest || type == Icmp4HeaderType.EchoReply);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4));
        }
    }

    /// <summary>
    /// Error ICMP4 header (RFC 792, RFC 1475, RFC 6633, RFC 6918).
    /// </summary>
    public abstract class ErrorIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the IP4 header.
        /// </summary>
        public IP4Header IP4Header { get; private set; }

        /// <summary>
        /// Gets the error data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of error header types.</param>
        protected ErrorIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp4HeaderType.DestinationUnreachable || type == Icmp4HeaderType.SourceQuench || type == Icmp4HeaderType.Redirect || type == Icmp4HeaderType.TimeExceeded || type == Icmp4HeaderType.ParameterProblem || type == Icmp4HeaderType.ConversionFailed);
        }

        /// <summary>
        /// Sets the IPV4 header.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        protected void SetIP4Header(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            IP4Header = new IP4Header(reader);
            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4 - (IP4Header.InternetHeaderLength * 4)));
        }
    }

    /// <summary>
    /// Constants that represent destination unreachable codes.
    /// </summary>
    public enum DestinationUnreachableCode
    {
        /// <summary>
        /// Network unreachable.
        /// </summary>
        NetworkUnreachable = 0,

        /// <summary>
        /// Host unreachable.
        /// </summary>
        HostUnreachable = 1,

        /// <summary>
        /// Protocol unreachable.
        /// </summary>
        ProtocolUnreachable = 2,

        /// <summary>
        /// Port unreachable.
        /// </summary>
        PortUnreachable = 3,

        /// <summary>
        /// Fragmentation needed and DF flag is set.
        /// </summary>
        FragmentationNeededAndDoNotFragmentSet = 4,

        /// <summary>
        /// Source route failed.
        /// </summary>
        SourceRouteFailed = 5,

        /// <summary>
        /// Destination network unknown.
        /// </summary>
        DestinationNetworkUnknown = 6,

        /// <summary>
        /// Destination host unknown.
        /// </summary>
        DestinationHostUnknown = 7,

        /// <summary>
        /// Source host isolated.
        /// </summary>
        SourceHostIsolated = 8,

        /// <summary>
        /// Communication with destination network administratively prohibited.
        /// </summary>
        CommunicationWithDestinationNetworkAdministrativelyProhibited = 9,

        /// <summary>
        /// Communication with destination host administratively prohibited.
        /// </summary>
        CommunicationWithDestinationHostAdministrativelyProhibited = 10,

        /// <summary>
        /// Destination network unreachable for type of service.
        /// </summary>
        DestinationNetworkUnreachableForTypeOfService = 11,

        /// <summary>
        /// Destination host unreachable for type of service.
        /// </summary>
        DestinationHostUnreachableForTypeOfService = 12,

        /// <summary>
        /// Communication administratively prohibited.
        /// </summary>
        CommunicationAdministrativelyProhibited = 13,

        /// <summary>
        /// Host precedence violation.
        /// </summary>
        HostPrecedenceViolation = 14,

        /// <summary>
        /// Precedence cutoff in effect.
        /// </summary>
        PrecedenceCutoffInEffect = 15
    }

    /// <summary>
    /// Destination Unreachable Error ICMP4 header (RFC 792).
    /// </summary>
    public sealed class DestinationUnreachableErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new DestinationUnreachableCode Code { get; private set; }

        /// <summary>
        /// Gets the unused 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Gets the next hop maximum transmission unit in bytes.
        /// </summary>
        public ushort NextHopMtu { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationUnreachableErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationUnreachableErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.DestinationUnreachable)
        {
            Contract.Requires(reader != null);

            Code = (DestinationUnreachableCode)base.Code;
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            NextHopMtu = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Source Quench Error ICMP4 header (RFC 792, RFC 6633).
    /// </summary>
    public sealed class SourceQuenchErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the unused 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceQuenchErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SourceQuenchErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.SourceQuench)
        {
            Contract.Requires(reader != null);

            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Constants that represent redirect codes.
    /// </summary>
    public enum RedirectCode
    {
        /// <summary>
        /// Network
        /// </summary>
        Network = 0,

        /// <summary>
        /// Host
        /// </summary>
        Host = 1,

        /// <summary>
        /// Type Of Service And Network
        /// </summary>
        TypeOfServiceAndNetwork = 2,

        /// <summary>
        /// Type Of Service And Host
        /// </summary>
        TypeOfServiceAndHost = 3
    }

    /// <summary>
    /// Redirect Error ICMP4 header (RFC 792).
    /// </summary>
    public sealed class RedirectErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new RedirectCode Code { get; private set; }

        /// <summary>
        /// Gets the gateway IP4 address.
        /// </summary>
        public IPAddress GatewayInternetAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedirectErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RedirectErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.Redirect)
        {
            Contract.Requires(reader != null);

            Code = (RedirectCode)base.Code;
            GatewayInternetAddress = new IPAddress(reader.ReadBytes(4));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Constants that represent router advertisement codes.
    /// </summary>
    public enum RouterAdvertisementCode
    {
        /// <summary>
        /// Normal router advertisement.
        /// </summary>
        NormalRouterAdvertisement = 0,

        /// <summary>
        /// Does not route common traffic.
        /// </summary>
        DoesNotRouteCommonTraffic = 16
    }

    /// <summary>
    /// Router Advertisement ICMP4 header (RFC 1256).
    /// </summary>
    public sealed class RouterAdvertisementIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new RouterAdvertisementCode Code { get; private set; }

        /// <summary>
        /// Gets the number of the <see cref="RouterAddresses"/>.
        /// </summary>
        public byte NumberOfAddresses { get; private set; }

        /// <summary>
        /// Gets the <see cref="RouterAddresses"/> entry size in 4-byte units.
        /// </summary>
        public byte AddressEntrySize { get; private set; }

        /// <summary>
        /// Gets the lifetime in seconds.
        /// </summary>
        public ushort Lifetime { get; private set; }

        /// <summary>
        /// Gets the router IP4 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> RouterAddresses { get; private set; }

        /// <summary>
        /// Gets the preference levels.
        /// </summary>
        public ReadOnlyCollection<uint> PreferenceLevels { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterAdvertisementIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterAdvertisementIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.RouterAdvertisement)
        {
            Contract.Requires(reader != null);

            Code = (RouterAdvertisementCode)base.Code;
            NumberOfAddresses = reader.ReadByte();
            AddressEntrySize = reader.ReadByte();
            Lifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var ips = new IPAddress[NumberOfAddresses];
            var prefs = new uint[ips.Length];
            for(int i = 0, j = ips.Length; i < j; i++)
            {
                ips[i] = new IPAddress(reader.ReadBytes(4));
                prefs[i] = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            }
            RouterAddresses = new ReadOnlyCollection<IPAddress>(ips);
            PreferenceLevels = new ReadOnlyCollection<uint>(prefs);
        }
    }

    /// <summary>
    /// Constants that represent time exceeded codes.
    /// </summary>
    public enum TimeExceededCode
    {
        /// <summary>
        /// Time to live exceeded in transit.
        /// </summary>
        TimeToLiveExceededInTransit = 0,

        /// <summary>
        /// Fragment reassembly time exceeded.
        /// </summary>
        FragmentReassemblyTimeExceeded = 1
    }

    /// <summary>
    /// Time Exceeded Error ICMP4 header (RFC 792).
    /// </summary>
    public sealed class TimeExceededErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new TimeExceededCode Code { get; private set; }

        /// <summary>
        /// Gets the unused 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeExceededErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeExceededErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.TimeExceeded)
        {
            Contract.Requires(reader != null);

            Code = (TimeExceededCode)base.Code;
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Constants that represent parameter problem code.
    /// </summary>
    public enum ParameterProblemCode
    {
        /// <summary>
        /// Pointer indicates the error.
        /// </summary>
        PointerIndicatesTheError = 0,

        /// <summary>
        /// Missing required option.
        /// </summary>
        MissingRequiredOption = 1,

        /// <summary>
        /// Bad length.
        /// </summary>
        BadLength = 2
    }

    /// <summary>
    /// Parameter Problem Error ICMP4 header (RFC 792).
    /// </summary>
    public sealed class ParameterProblemErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new ParameterProblemCode Code { get; private set; }

        /// <summary>
        /// Gets the pointer to the byte where an error was detected.
        /// </summary>
        public byte Pointer { get; private set; }

        /// <summary>
        /// Gets the unused 24 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterProblemErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ParameterProblemErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.ParameterProblem)
        {
            Contract.Requires(reader != null);

            Code = (ParameterProblemCode)base.Code;
            Pointer = reader.ReadByte();
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(3));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Time Stamp Request Or Reply ICMP4 header (RFC 792).
    /// </summary>
    public sealed class TimeStampRequestOrReplyIcmp4Header : RequestOrReplyIcmp4Header
    {
        /// <summary>
        /// Gets the originate time stamp.
        /// </summary>
        public uint OriginateTimeStamp { get; private set; }

        /// <summary>
        /// Gets the receive time stamp.
        /// </summary>
        public uint ReceiveTimeStamp { get; private set; }

        /// <summary>
        /// Gets the transmit time stamp.
        /// </summary>
        public uint TransmitTimeStamp { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeStampRequestOrReplyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of time stamp header types.</param>
        public TimeStampRequestOrReplyIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp4HeaderType.TimeStampRequest || type == Icmp4HeaderType.TimeStampReply);

            OriginateTimeStamp = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            ReceiveTimeStamp = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            TransmitTimeStamp = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Address Mask Request Or Reply ICMP4 header (RFC 792, RFC 6918).
    /// </summary>
    public sealed class AddressMaskRequestOrReplyIcmp4Header : RequestOrReplyIcmp4Header
    {
        /// <summary>
        /// Gets the IP4 address mask.
        /// </summary>
        public uint AddressMask { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressMaskRequestOrReplyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of address mask header types.</param>
        public AddressMaskRequestOrReplyIcmp4Header(BinaryReader reader, Icmp4HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp4HeaderType.AddressMaskRequest || type == Icmp4HeaderType.AddressMaskReply);

            AddressMask = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Constants that represent trace route codes.
    /// </summary>
    public enum TraceRouteCode
    {
        /// <summary>
        /// Outbound packet successfully forwarded.
        /// </summary>
        OutboundPacketSuccessfullyForwarded = 0,

        /// <summary>
        /// No route for outbound packet.
        /// </summary>
        NoRouteForOutboundPacket = 1
    }

    /// <summary>
    /// Trace Route ICMP4 header (RFC 1393, RFC 6918).
    /// </summary>
    public sealed class TraceRouteIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new TraceRouteCode Code { get; private set; }

        /// <summary>
        /// Gets the identifier number.
        /// </summary>
        public ushort IdNumber { get; private set; }

        /// <summary>
        /// Gets the unused 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Gets OHC, the outbound hop count.
        /// </summary>
        public ushort OutboundHopCount { get; private set; }

        /// <summary>
        /// Gets RHC, the return hop count.
        /// </summary>
        public ushort ReturnHopCount { get; private set; }

        /// <summary>
        /// Gets the output link speed in bytes-per-second.
        /// </summary>
        public uint OutputLinkSpeed { get; private set; }

        /// <summary>
        /// Gets the output link maximum transmission unit in bytes.
        /// </summary>
        public uint OutputLinkMtu { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceRouteIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TraceRouteIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.TraceRoute)
        {
            Contract.Requires(reader != null);

            Code = (TraceRouteCode)base.Code;
            IdNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            OutboundHopCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ReturnHopCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            OutputLinkSpeed = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            OutputLinkMtu = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Constants that represent conversion failed code.
    /// </summary>
    public enum ConversionFailedCode
    {
        /// <summary>
        /// Unknown or unspecified error.
        /// </summary>
        UnknownOrUnspecifiedError = 0,

        /// <summary>
        /// Do Not Convert option present.
        /// </summary>
        DoNotConvertOptionPresent = 1,

        /// <summary>
        /// Unknown mandatory option present.
        /// </summary>
        UnknownMandatoryOptionPresent = 2,

        /// <summary>
        /// Known unsupported option present.
        /// </summary>
        KnownUnsupportedOptionPresent = 3,

        /// <summary>
        /// Unsupported transport protocol.
        /// </summary>
        UnsupportedTransportProtocol = 4,

        /// <summary>
        /// Overall length exceeded.
        /// </summary>
        OverallLengthExceeded = 5,

        /// <summary>
        /// IP header length exceeded.
        /// </summary>
        IPHeaderLengthExceeded = 6,

        /// <summary>
        /// Transport protocol out of range.
        /// </summary>
        TransportProtocolOutOfRange = 7,

        /// <summary>
        /// Port conversion out of range.
        /// </summary>
        PortConversionOutOfRange = 8,

        /// <summary>
        /// Transport header length exceeded.
        /// </summary>
        TransportHeaderLengthExceeded = 9,

        /// <summary>
        /// Rollover missing and ACK flag is set.
        /// </summary>
        RolloverMissingAndAcknowledgmentSet = 10,

        /// <summary>
        /// Unknown mandatory transport option present.
        /// </summary>
        UnknownMandatoryTransportOptionPresent = 11
    }

    /// <summary>
    /// Conversion Failed Error ICMP4 header (RFC 1475, RFC 6918).
    /// </summary>
    public sealed class ConversionFailedErrorIcmp4Header : ErrorIcmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new ConversionFailedCode Code { get; private set; }

        /// <summary>
        /// Gets the pointer to the byte at the beginning of the offending field.
        /// </summary>
        public uint Pointer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionFailedErrorIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ConversionFailedErrorIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.ConversionFailed)
        {
            Contract.Requires(reader != null);

            Code = (ConversionFailedCode)base.Code;
            Pointer = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SetIP4Header(reader);
        }
    }

    /// <summary>
    /// Constants that represent photuris codes.
    /// </summary>
    public enum PhoturisCode
    {
        /// <summary>
        /// Bad security parameter index.
        /// </summary>
        BadSecurityParametersIndex = 0,

        /// <summary>
        /// Authentication failed.
        /// </summary>
        AuthenticationFailed = 1,

        /// <summary>
        /// Decompression failed.
        /// </summary>
        DecompressionFailed = 2,

        /// <summary>
        /// Decryption failed.
        /// </summary>
        DecryptionFailed = 3,

        /// <summary>
        /// Need authentication.
        /// </summary>
        NeedAuthentication = 4,

        /// <summary>
        /// Need authorization.
        /// </summary>
        NeedAuthorization = 5
    }

    /// <summary>
    /// Photuris ICMP4 header (RFC 2521).
    /// </summary>
    public sealed class PhoturisIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new PhoturisCode Code { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the pointer to the most significant byte of the offending Security Parameters Index.
        /// </summary>
        public ushort Pointer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoturisIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public PhoturisIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.Photuris)
        {
            Contract.Requires(reader != null);

            Code = (PhoturisCode)base.Code;
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            Pointer = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Constants that represent Seamoby subtypes.
    /// </summary>
    public enum SeamobySubType
    {
        /// <summary>
        /// Candidate Access Router Discovery
        /// </summary>
        CARD = 0,

        /// <summary>
        /// Context Transfer Protocol
        /// </summary>
        CXTP = 1
    }

    /// <summary>
    /// Seamoby ICMP4 header (RFC 4065).
    /// </summary>
    public sealed class SeamobyIcmp4Header : Icmp4Header
    {
        /// <summary>
        /// Gets the subtype.
        /// </summary>
        public SeamobySubType SubType { get; private set; }

        /// <summary>
        /// Gets the reserved 24 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the mobility options.
        /// </summary>
        public ReadOnlyCollection<byte> MobilityOptions { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeamobyIcmp4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SeamobyIcmp4Header(BinaryReader reader) : base(reader, Icmp4HeaderType.Seamoby)
        {
            Contract.Requires(reader != null);

            SubType = (SeamobySubType)reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(3));
            MobilityOptions = new ReadOnlyCollection<byte>(reader.ReadBytes((int)(reader.BaseStream.Length - reader.BaseStream.Position)));
        }
    }
}