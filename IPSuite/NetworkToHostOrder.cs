﻿namespace IPSuite
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Numerics;

    /// <summary>
    /// Converts numbers from network byte order to host byte order.
    /// </summary>
    public static class NetworkToHostOrder
    {
        /// <summary>
        /// Converts a short integer value from network byte order to host byte order.
        /// </summary>
        /// <param name="octets">The short integer value in network byte order.</param>
        /// <returns>
        /// A short integer value in host byte order.
        /// </returns>
        public static ushort ToUInt16(byte[] octets)
        {
            Contract.Requires(octets != null);
            Contract.Requires(octets.Length <= 2);

            if(BitConverter.IsLittleEndian)
                Array.Reverse(octets);
            return BitConverter.ToUInt16(octets, 0);
        }

        /// <summary>
        /// Converts an integer value from network byte order to host byte order.
        /// </summary>
        /// <param name="octets">The integer value in network byte order.</param>
        /// <returns>
        /// A integer value in host byte order.
        /// </returns>
        public static uint ToUInt32(byte[] octets)
        {
            Contract.Requires(octets != null);
            Contract.Requires(octets.Length <= 4);

            if(BitConverter.IsLittleEndian)
                Array.Reverse(octets);
            return BitConverter.ToUInt32(octets, 0);
        }

        /// <summary>
        /// Converts a long integer value from network byte order to host byte order.
        /// </summary>
        /// <param name="octets">The long integer value in network byte order.</param>
        /// <returns>
        /// A long integer value in host byte order.
        /// </returns>
        public static ulong ToUInt64(byte[] octets)
        {
            Contract.Requires(octets != null);
            Contract.Requires(octets.Length <= 8);

            if(BitConverter.IsLittleEndian)
                Array.Reverse(octets);
            return BitConverter.ToUInt64(octets, 0);
        }

        /// <summary>
        /// Converts a big integer value from network byte order to host byte order.
        /// </summary>
        /// <param name="octets">The big integer value in network byte order.</param>
        /// <returns>
        /// A big integer value in host byte order.
        /// </returns>
        public static BigInteger ToUInt128(byte[] octets)
        {
            Contract.Requires(octets != null);
            Contract.Requires(octets.Length <= 16);

            if(BitConverter.IsLittleEndian)
                Array.Reverse(octets);
            if((octets[octets.Length - 1] & 128) != 0)
                Array.Resize(ref octets, octets.Length + 1);
            return new BigInteger(octets);
        }
    }
}