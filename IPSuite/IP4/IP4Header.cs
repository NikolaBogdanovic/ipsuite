﻿namespace IPSuite.IP4
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Constants that represent types of service (TOS).
    /// </summary>
    public enum TypeOfService
    {
        /// <summary>
        /// Default
        /// </summary>
        Default = 0,

        /// <summary>
        /// Minimize Monetary Cost
        /// </summary>
        MinimizeMonetaryCost = 1,

        /// <summary>
        /// Maximize Reliability
        /// </summary>
        MaximizeReliability = 2,

        /// <summary>
        /// Maximize Throughput
        /// </summary>
        MaximizeThroughput = 4,

        /// <summary>
        /// Minimize Delay
        /// </summary>
        MinimizeDelay = 8,

        /// <summary>
        /// Maximize Security
        /// </summary>
        MaximizeSecurity = 15
    }

    /// <summary>
    /// Constants that represent explicit congestion notifications (ECN).
    /// </summary>
    public enum ExplicitCongestionNotification
    {
        /// <summary>
        /// Not ECN-Capable Transport
        /// </summary>
        Not_ECT = 0,

        /// <summary>
        /// ECN-Capable Transport 1
        /// </summary>
        ECT1 = 1,

        /// <summary>
        /// ECN-Capable Transport 0
        /// </summary>
        ECT0 = 2,

        /// <summary>
        /// Congestion Experienced
        /// </summary>
        CE = 3
    }

    /// <summary>
    /// Constants that represent internet protocols (IP).
    /// </summary>
    public enum InternetProtocol
    {
        /// <summary>
        /// Hop-by-Hop Options IP6 extension (RFC 2460)
        /// </summary>
        IP6_HopOpt = 0,

        /// <summary>
        /// Internet Control Message Protocol version 4 (RFC 792)
        /// </summary>
        ICMP4 = 1,

        /// <summary>
        /// Internet Group Management Protocol version 4 (RFC 1112)
        /// </summary>
        IGMP4 = 2,

        /// <summary>
        /// Internet Group Authentication Protocol version 4
        /// </summary>
        IGAP4 = 2,

        /// <summary>
        /// Gateway-to-Gateway Protocol (RFC 823)
        /// </summary>
        GGP = 3,

        /// <summary>
        /// Internet Protocol version 4 encapsulation (RFC 2003)
        /// </summary>
        IP4 = 4,

        /// <summary>
        /// Stream (RFC 1190, RFC 1819)
        /// </summary>
        ST = 5,

        /// <summary>
        /// Transmission Control Protocol (RFC 793)
        /// </summary>
        TCP = 6,

        /// <summary>
        /// Core Based Trees
        /// </summary>
        CBT = 7,

        /// <summary>
        /// Exterior Gateway Protocol (RFC 888)
        /// </summary>
        EGP = 8,

        /// <summary>
        /// Interior Gateway Protocol
        /// </summary>
        IGP = 9,

        /// <summary>
        /// BBN RCC Monitoring
        /// </summary>
        BBN_RCC_MON = 10,

        /// <summary>
        /// Network Voice Protocol (RFC 741)
        /// </summary>
        NVP = 11,

        /// <summary>
        /// PUP
        /// </summary>
        PUP = 12,

        /// <summary>
        /// ARGUS
        /// </summary>
        ARGUS = 13,

        /// <summary>
        /// Emission Control
        /// </summary>
        EMCON = 14,

        /// <summary>
        /// Cross Net
        /// </summary>
        XNET = 15,

        /// <summary>
        /// Chaos
        /// </summary>
        CHAOS = 16,

        /// <summary>
        /// User Datagram Protocol (RFC 768)
        /// </summary>
        UDP = 17,

        /// <summary>
        /// Multiplexing
        /// </summary>
        MUX = 18,

        /// <summary>
        /// DCN Measurement Subsystems
        /// </summary>
        DCN_MEAS = 19,

        /// <summary>
        /// Host Monitoring Protocol (RFC 869)
        /// </summary>
        HMP = 20,

        /// <summary>
        /// Packet Radio Measurement
        /// </summary>
        PRM = 21,

        /// <summary>
        /// Xerox NS IDP
        /// </summary>
        XNS_IDP = 22,

        /// <summary>
        /// Trunk-1
        /// </summary>
        TRUNK_1 = 23,

        /// <summary>
        /// Trunk-2
        /// </summary>
        TRUNK_2 = 24,

        /// <summary>
        /// Leaf-1
        /// </summary>
        LEAF_1 = 25,

        /// <summary>
        /// Leaf-2
        /// </summary>
        LEAF_2 = 26,

        /// <summary>
        /// Reliable Data Protocol (RFC 908)
        /// </summary>
        RDP = 27,

        /// <summary>
        /// Internet Reliable Transaction Protocol (RFC 938)
        /// </summary>
        IRTP = 28,

        /// <summary>
        /// ISO Transport Protocol Class 4 (RFC 905)
        /// </summary>
        ISO_TP4 = 29,

        /// <summary>
        /// Network Block Transfer (RFC 969)
        /// </summary>
        NETBLT = 30,

        /// <summary>
        /// MFE Network Services Protocol
        /// </summary>
        MFE_NSP = 31,

        /// <summary>
        /// MERIT Inter-Nodal Protocol
        /// </summary>
        MERIT_INP = 32,

        /// <summary>
        /// Datagram Congestion Control Protocol (RFC 4340)
        /// </summary>
        DCCP = 33,

        /// <summary>
        /// Third Party Connect
        /// </summary>
        TPC = 34,

        /// <summary>
        /// Inter-Domain Policy Routing
        /// </summary>
        IDPR = 35,

        /// <summary>
        /// Xpress Transfer Protocol
        /// </summary>
        XTP = 36,

        /// <summary>
        /// Datagram Delivery Protocol
        /// </summary>
        DDP = 37,

        /// <summary>
        /// IDPR Control Message Transport Protocol
        /// </summary>
        IDPR_CMTP = 38,

        /// <summary>
        /// TP++
        /// </summary>
        TPPP = 39,

        /// <summary>
        /// IL
        /// </summary>
        IL = 40,

        /// <summary>
        /// Internet Protocol version 6 encapsulation (RFC 2473)
        /// </summary>
        IP6 = 41,

        /// <summary>
        /// Source Demand Routing Protocol
        /// </summary>
        SDRP = 42,

        /// <summary>
        /// Routing IP6 extension (RFC 2460)
        /// </summary>
        IP6_Route = 43,

        /// <summary>
        /// Fragment IP6 extension (RFC 2460)
        /// </summary>
        IP6_Frag = 44,

        /// <summary>
        /// Inter-Domain Routing Protocol
        /// </summary>
        IDRP = 45,

        /// <summary>
        /// Reservation Protocol (RFC 2205, RFC 3209)
        /// </summary>
        RSVP = 46,

        /// <summary>
        /// Generic Routing Encapsulation (RFC 1701)
        /// </summary>
        GRE = 47,

        /// <summary>
        /// Dynamic Source Routing (RFC 4728)
        /// </summary>
        DSR = 48,

        /// <summary>
        /// BNA
        /// </summary>
        BNA = 49,

        /// <summary>
        /// Encapsulating Security Payload IP6 extension (RFC 4303)
        /// </summary>
        IP6_ESP = 50,

        /// <summary>
        /// Authentication Header IP6 extension (RFC 4302)
        /// </summary>
        IP6_AH = 51,

        /// <summary>
        /// Integrated Net Layer Security Protocol
        /// </summary>
        I_NLSP = 52,

        /// <summary>
        /// IP Encryption
        /// </summary>
        SWIPE = 53,

        /// <summary>
        /// NBMA Address Resolution Protocol (RFC 1735)
        /// </summary>
        NARP = 54,

        /// <summary>
        /// Minimal Encapsulation (RFC 2004)
        /// </summary>
        MOBILE = 55,

        /// <summary>
        /// Transport Layer Security Protocol
        /// </summary>
        TLSP = 56,

        /// <summary>
        /// SKIP
        /// </summary>
        SKIP = 57,

        /// <summary>
        /// Internet Control Message Protocol version 6 (RFC 2460)
        /// </summary>
        ICMP6 = 58,

        /// <summary>
        /// No Next IP6 extension (RFC 2460)
        /// </summary>
        IP6_NoNxt = 59,

        /// <summary>
        /// Destination Options IP6 extension (RFC 2460)
        /// </summary>
        IP6_Opts = 60,

        /// <summary>
        /// Internal Host Protocol
        /// </summary>
        IHP = 61,

        /// <summary>
        /// CFTP
        /// </summary>
        CFTP = 62,

        /// <summary>
        /// Local Network
        /// </summary>
        LN = 63,

        /// <summary>
        /// SATNET and BackRoom EXPAK
        /// </summary>
        SAT_EXPAK = 64,

        /// <summary>
        /// Kryptolan
        /// </summary>
        KRYPTOLAN = 65,

        /// <summary>
        /// Remote Virtual Disk
        /// </summary>
        RVD = 66,

        /// <summary>
        /// Internet Pluribus Packet Core
        /// </summary>
        IPPC = 67,

        /// <summary>
        /// Distributed File System
        /// </summary>
        DFS = 68,

        /// <summary>
        /// SATNET Monitoring
        /// </summary>
        SAT_MON = 69,

        /// <summary>
        /// VISA
        /// </summary>
        VISA = 70,

        /// <summary>
        /// Internet Packet Core Utility
        /// </summary>
        IPCV = 71,

        /// <summary>
        /// Computer Protocol Network Executive
        /// </summary>
        CPNX = 72,

        /// <summary>
        /// Computer Protocol Heart Beat
        /// </summary>
        CPHB = 73,

        /// <summary>
        /// Wang Span Network
        /// </summary>
        WSN = 74,

        /// <summary>
        /// Packet Video Protocol
        /// </summary>
        PVP = 75,

        /// <summary>
        /// BackRoom SATNET Monitoring
        /// </summary>
        BR_SAT_MON = 76,

        /// <summary>
        /// SUN ND
        /// </summary>
        SUN_ND = 77,

        /// <summary>
        /// WIDEBAND Monitoring
        /// </summary>
        WB_MON = 78,

        /// <summary>
        /// WIDEBAND EXPAK
        /// </summary>
        WB_EXPAK = 79,

        /// <summary>
        /// ISO Internet Protocol
        /// </summary>
        ISO_IP = 80,

        /// <summary>
        /// Versatile Message Transaction Protocol
        /// </summary>
        VMTP = 81,

        /// <summary>
        /// Secure Versatile Message Transaction Protocol
        /// </summary>
        SECURE_VMTP = 82,

        /// <summary>
        /// VINES
        /// </summary>
        VINES = 83,

        /// <summary>
        /// Transaction Transport Protocol
        /// </summary>
        TTP = 84,

        /// <summary>
        /// Internet Protocol Traffic Manager
        /// </summary>
        IPTM = 84,

        /// <summary>
        /// NSFNET
        /// </summary>
        NSFNET = 85,

        /// <summary>
        /// Dissimilar Gateway Protocol
        /// </summary>
        DGP = 86,

        /// <summary>
        /// TCF
        /// </summary>
        TCF = 87,

        /// <summary>
        /// EIGRP
        /// </summary>
        EIGRP = 88,

        /// <summary>
        /// Open Shortest Path First (RFC 1583, RFC 2328, RFC 5340)
        /// </summary>
        OSPF = 89,

        /// <summary>
        /// Sprite RPC
        /// </summary>
        Sprite_RPC = 90,

        /// <summary>
        /// Locus Address Resolution Protocol
        /// </summary>
        LARP = 91,

        /// <summary>
        /// Multicast Transport Protocol
        /// </summary>
        MTP = 92,

        /// <summary>
        /// AX.25 Frames
        /// </summary>
        AX25 = 93,

        /// <summary>
        /// IP-within-IP encapsulation
        /// </summary>
        IPIP = 94,

        /// <summary>
        /// Mobile Inter-networking Control Protocol
        /// </summary>
        MICP = 95,

        /// <summary>
        /// Semaphore Communications Security Protocol
        /// </summary>
        SCC_SP = 96,

        /// <summary>
        /// Ethernet-within-IP encapsulation (RFC 3378)
        /// </summary>
        ETHERIP = 97,

        /// <summary>
        /// Encapsulation Header (RFC 1241)
        /// </summary>
        ENCAP = 98,

        /// <summary>
        /// Private Encryption Scheme
        /// </summary>
        PES = 99,

        /// <summary>
        /// GMTP
        /// </summary>
        GMTP = 100,

        /// <summary>
        /// Ipsilon Flow Management Protocol
        /// </summary>
        IFMP = 101,

        /// <summary>
        /// PNNI
        /// </summary>
        PNNI = 102,

        /// <summary>
        /// Protocol Independent Multicast (RFC 4601)
        /// </summary>
        PIM = 103,

        /// <summary>
        /// ARIS
        /// </summary>
        ARIS = 104,

        /// <summary>
        /// SCPS
        /// </summary>
        SCPS = 105,

        /// <summary>
        /// QNX
        /// </summary>
        QNX = 106,

        /// <summary>
        /// Active Networks
        /// </summary>
        AN = 107,

        /// <summary>
        /// IP Payload Compression (RFC 2393)
        /// </summary>
        IPComp = 108,

        /// <summary>
        /// Sitara Networks Protocol
        /// </summary>
        SNP = 109,

        /// <summary>
        /// Compaq Peer 
        /// </summary>
        Compaq_Peer = 110,

        /// <summary>
        /// IPX in IP
        /// </summary>
        IPX_in_IP = 111,

        /// <summary>
        /// Virtual Router Redundancy Protocol (RFC 5798)
        /// </summary>
        VRRP = 112,

        /// <summary>
        /// Pragmatic General Multicast
        /// </summary>
        PGM = 113,

        /// <summary>
        /// Zero-Hop Protocol
        /// </summary>
        ZHP = 114,

        /// <summary>
        /// Layer 2 Tunneling Protocol (RFC 3931)
        /// </summary>
        L2TP = 115,

        /// <summary>
        /// D-II Data Exchange
        /// </summary>
        DDX = 116,

        /// <summary>
        /// Interactive Agent Transfer Protocol
        /// </summary>
        IATP = 117,

        /// <summary>
        /// Schedule Transfer Protocol
        /// </summary>
        STP = 118,

        /// <summary>
        /// SpectraLink Radio Protocol
        /// </summary>
        SRP = 119,

        /// <summary>
        /// UTI
        /// </summary>
        UTI = 120,

        /// <summary>
        /// Simple Message Protocol
        /// </summary>
        SMP = 121,

        /// <summary>
        /// Simple Multicast
        /// </summary>
        SM = 122,

        /// <summary>
        /// Performance Transparency Protocol
        /// </summary>
        PTP = 123,

        /// <summary>
        /// ISIS
        /// </summary>
        ISIS = 124,

        /// <summary>
        /// FIRE
        /// </summary>
        FIRE = 125,

        /// <summary>
        /// Combat Radio Transport Protocol
        /// </summary>
        CRTP = 126,

        /// <summary>
        /// Combat Radio User Datagram Protocol
        /// </summary>
        CRUDP = 127,

        /// <summary>
        /// SSCOPMCE
        /// </summary>
        SSCOPMCE = 128,

        /// <summary>
        /// IPLT
        /// </summary>
        IPLT = 129,

        /// <summary>
        /// Secure Packet Shield
        /// </summary>
        SPS = 130,

        /// <summary>
        /// Private IP Encapsulation
        /// </summary>
        PIPE = 131,

        /// <summary>
        /// Stream Control Transmission Protocol
        /// </summary>
        SCTP = 132,

        /// <summary>
        /// Fibre Channel (RFC 6172)
        /// </summary>
        FC = 133,

        /// <summary>
        /// RSVP-E2E-IGNORE (RFC 3175)
        /// </summary>
        RSVP_E2E_IGNORE = 134,

        /// <summary>
        /// Mobility Header IP6 extension (RFC 6275)
        /// </summary>
        IP6_MH = 135,

        /// <summary>
        /// Lightweight User Datagram Protocol (RFC 3828)
        /// </summary>
        UDPLite = 136,

        /// <summary>
        /// MPLS in IP (RFC 4023)
        /// </summary>
        MPLS_in_IP = 137,

        /// <summary>
        /// MANET (RFC 5498)
        /// </summary>
        MANET = 138,

        /// <summary>
        /// Host Identity Protocol IP6 extension (RFC 5201)
        /// </summary>
        IP6_HIP = 139,

        /// <summary>
        /// Shim IP6 extension (RFC 5533)
        /// </summary>
        IP6_Shim = 140,

        /// <summary>
        /// Wrapped Encapsulating Security Payload (RFC 5840)
        /// </summary>
        WESP = 141,

        /// <summary>
        /// Robust Header Compression (RFC 5858)
        /// </summary>
        ROHC = 142
    }

    /// <summary>
    /// Internet Protocol version 4 header (RFC 791).
    /// </summary>
    public sealed class IP4Header
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        public byte Version { get; private set; }

        /// <summary>
        /// Gets IHL, the number of 4-byte units in the header.
        /// </summary>
        public byte InternetHeaderLength { get; private set; }

        /// <summary>
        /// Gets DS.
        /// </summary>
        public byte DifferentiatedServices { get; private set; }

        /// <summary>
        /// Gets ECN.
        /// </summary>
        public ExplicitCongestionNotification ExplicitCongestionNotification { get; private set; }

        /// <summary>
        /// Gets the total length in bytes.
        /// </summary>
        public ushort TotalLength { get; private set; }

        /// <summary>
        /// Gets Id.
        /// </summary>
        public ushort Identification { get; private set; }

        /// <summary>
        /// Gets the reserved 1 bit.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether DF flag is set.
        /// </summary>
        public bool DoNotFragment { get; private set; }

        /// <summary>
        /// Gets a value indicating whether MF flag is set.
        /// </summary>
        public bool MoreFragments { get; private set; }

        /// <summary>
        /// Gets the fragment offset, used to direct the reassembly of a fragmented datagram.
        /// </summary>
        public ushort FragmentOffset { get; private set; }

        /// <summary>
        /// Gets TTL, a timer field used to track the lifetime of the datagram.
        /// </summary>
        public byte TimeToLive { get; private set; }

        /// <summary>
        /// Gets the next encapsulated protocol.
        /// </summary>
        public InternetProtocol Protocol { get; private set; }

        /// <summary>
        /// Gets the header checksum.
        /// </summary>
        public ushort HeaderChecksum { get; private set; }

        /// <summary>
        /// Gets the IP4 address of the sender.
        /// </summary>
        public IPAddress SourceAddress { get; private set; }

        /// <summary>
        /// Gets the IP4 address of the intended receiver.
        /// </summary>
        public IPAddress DestinationAddress { get; private set; }

        /// <summary>
        /// Gets the list of IP4 options.
        /// </summary>
        public ReadOnlyCollection<IP4Option> Options { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Gets the payload length in bytes.
        /// </summary>
        public ushort PayloadLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP4Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public IP4Header(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Version = (byte)(bits >> 4);
            InternetHeaderLength = (byte)((byte)(bits << 4) >> 4);
            bits = reader.ReadByte();
            DifferentiatedServices = (byte)(bits >> 2);
            ExplicitCongestionNotification = (ExplicitCongestionNotification)(byte)((byte)(bits << 6) >> 6);
            TotalLength = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Identification = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            bits = reader.ReadByte();
            Reserved = (byte)(bits >> 7);
            DoNotFragment = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            MoreFragments = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            FragmentOffset = NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(bits << 3) >> 3), reader.ReadByte() });
            TimeToLive = reader.ReadByte();
            Protocol = (InternetProtocol)reader.ReadByte();
            HeaderChecksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SourceAddress = new IPAddress(reader.ReadBytes(4));
            DestinationAddress = new IPAddress(reader.ReadBytes(4));
            var opts = new List<IP4Option>();
            int i = 0, j = (InternetHeaderLength * 4) - 20;
            while(i < j)
            {
                var o = IP4Option.Create(reader);
                opts.Add(o);
                if(o.OptionType == IP4OptionType.EOOL)
                {
                    i += 1;
                    break;
                }
                if(o.OptionType == IP4OptionType.NOOP)
                    i += 1;
                else
                    i += o.Length;
            }
            Options = new ReadOnlyCollection<IP4Option>(opts.ToArray());
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(j - i));
            PayloadLength = (ushort)(TotalLength - (InternetHeaderLength * 4));
        }
    }
}