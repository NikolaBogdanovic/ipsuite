﻿namespace IPSuite.IP4
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Constants that represent IP4 option types.
    /// </summary>
    public enum IP4OptionType
    {
        /// <summary>
        /// End Of Options List (RFC 791)
        /// </summary>
        EOOL = 0,

        /// <summary>
        /// No Operation (RFC 791)
        /// </summary>
        NOOP = 1,

        /// <summary>
        /// Record Route (RFC 791)
        /// </summary>
        RR = 7,

        /// <summary>
        /// MTU Probe (RFC 1063, RFC 1191)
        /// </summary>
        MTUP = 11,

        /// <summary>
        /// MTU Reply (RFC 1063, RFC 1191)
        /// </summary>
        MTUR = 12,

        /// <summary>
        /// Experimental Encryption (RFC 6814)
        /// </summary>
        ENCODE = 15,

        /// <summary>
        /// Quick-Start (RFC 4782)
        /// </summary>
        QS = 25,

        /// <summary>
        /// Time Stamp (RFC 791)
        /// </summary>
        TS = 68,

        /// <summary>
        /// Trace Route (RFC 1393, RFC 6814)
        /// </summary>
        TR = 82,

        /// <summary>
        /// Security (RFC 1108)
        /// </summary>
        SEC = 130,

        /// <summary>
        /// Loose Source Route (RFC 791)
        /// </summary>
        LSR = 131,

        /// <summary>
        /// Extended Security (RFC 1108)
        /// </summary>
        E_SEC = 133,

        /// <summary>
        /// Stream Identifier (RFC 791, RFC 6814)
        /// </summary>
        SID = 136,

        /// <summary>
        /// Strict Source Route (RFC 791)
        /// </summary>
        SSR = 137,

        /// <summary>
        /// Experimental Access Control (RFC 6814)
        /// </summary>
        VISA = 142,

        /// <summary>
        /// Extended Internet Protocol (RFC 1385, RFC 6814)
        /// </summary>
        EIP = 145,

        /// <summary>
        /// Address Extension (RFC 6814)
        /// </summary>
        ADDEXT = 147,

        /// <summary>
        /// Router Alert (RFC 2113)
        /// </summary>
        RTRALT = 148,

        /// <summary>
        /// Selective Directed Broadcast Mode (RFC 6814)
        /// </summary>
        SDBM = 149,

        /// <summary>
        /// Dynamic Packet State (RFC 6814)
        /// </summary>
        DPS = 151,

        /// <summary>
        /// Upstream Multicast Packet (RFC 6814)
        /// </summary>
        UMP = 152
    }

    /// <summary>
    /// Constants that represent IP4 option classes.
    /// </summary>
    public enum IP4OptionClass
    {
        /// <summary>
        /// Control And Management
        /// </summary>
        ControlAndManagement = 0,

        /// <summary>
        /// Debugging And Measurement
        /// </summary>
        DebuggingAndMeasurement = 2
    }

    /// <summary>
    /// Internet Protocol version 4 option (RFC 791).
    /// </summary>
    public abstract class IP4Option
    {
        /// <summary>
        /// Gets the option type.
        /// </summary>
        public IP4OptionType OptionType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether C flag is set.
        /// </summary>
        public bool Copy { get; private set; }

        /// <summary>
        /// Gets the option class.
        /// </summary>
        public IP4OptionClass OptionClass { get; private set; }

        /// <summary>
        /// Gets the option number.
        /// </summary>
        public byte OptionNumber { get; private set; }

        /// <summary>
        /// Gets the length in bytes.
        /// </summary>
        public byte Length { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The option type.</param>
        protected IP4Option(BinaryReader reader, IP4OptionType type)
        {
            Contract.Requires(reader != null);

            OptionType = type;
            Copy = Convert.ToBoolean((byte)((byte)type >> 7));
            OptionClass = (IP4OptionClass)(byte)((byte)((byte)type << 1) >> 6);
            OptionNumber = (byte)((byte)((byte)type << 3) >> 3);
            if(OptionType != IP4OptionType.EOOL && OptionType != IP4OptionType.NOOP)
                Length = reader.ReadByte();
        }

        /// <summary>
        /// Creates an IP4 option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// An IP4 option.
        /// </returns>
        public static IP4Option Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (IP4OptionType)reader.ReadByte();
            switch(type)
            {
                case IP4OptionType.RR:
                    return new RecordRouteIP4Option(reader);
                case IP4OptionType.MTUP:
                case IP4OptionType.MTUR:
                    return new MaximumTransmissionUnitIP4Option(reader, type);
                case IP4OptionType.QS:
                    return new QuickStartIP4Option(reader);
                case IP4OptionType.TS:
                    return new TimeStampIP4Option(reader);
                case IP4OptionType.TR:
                    return new TraceRouteIP4Option(reader);
                case IP4OptionType.SEC:
                    return new SecurityIP4Option(reader);
                case IP4OptionType.LSR:
                case IP4OptionType.SSR:
                    return new SourceRouteIP4Option(reader, type);
                case IP4OptionType.E_SEC:
                    return new ExtendedSecurityIP4Option(reader);
                case IP4OptionType.SID:
                    return new StreamIdentifierIP4Option(reader);
                case IP4OptionType.RTRALT:
                    return new RouterAlertIP4Option(reader);
                case IP4OptionType.SDBM:
                    return new SelectiveDirectedBroadcastModeIP4Option(reader);
                default:
                    return new GenericIP4Option(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic IP4 option (RFC 791).
    /// </summary>
    public sealed class GenericIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The option type.</param>
        public GenericIP4Option(BinaryReader reader, IP4OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            if(Length > 0)
                Data = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - 2));
        }
    }

    /// <summary>
    /// Record Route IP4 option (RFC 791).
    /// </summary>
    public sealed class RecordRouteIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the pointer to the byte which begins the next area to store a route IP4 address in the <see cref="RouteData"/>.
        /// </summary>
        public byte Pointer { get; private set; }

        /// <summary>
        /// Gets the list of IP4 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> RouteData { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordRouteIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RecordRouteIP4Option(BinaryReader reader) : base(reader, IP4OptionType.RR)
        {
            Contract.Requires(reader != null);

            Pointer = reader.ReadByte();
            var rd = new IPAddress[(Pointer - 4) / 4];
            for(int i = 0, j = rd.Length; i < j; i++)
                rd[i] = new IPAddress(reader.ReadBytes(4));
            RouteData = new ReadOnlyCollection<IPAddress>(rd);
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - (Pointer - 1)));
        }
    }

    /// <summary>
    /// Maximum Transmission Unit IP4 option (RFC 1063, RFC 1191).
    /// </summary>
    public sealed class MaximumTransmissionUnitIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the lowest maximum transmission unit value detected.
        /// </summary>
        public ushort MaximumTransmissionUnit { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaximumTransmissionUnitIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of MTU option types.</param>
        public MaximumTransmissionUnitIP4Option(BinaryReader reader, IP4OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == IP4OptionType.MTUP || type == IP4OptionType.MTUR);

            MaximumTransmissionUnit = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Constants that represent rate types.
    /// </summary>
    public enum RateType
    {
        /// <summary>
        /// Request
        /// </summary>
        Request = 0,

        /// <summary>
        /// Report
        /// </summary>
        Report = 1
    }

    /// <summary>
    /// Quick-Start IP4 option (RFC 4782).
    /// </summary>
    public sealed class QuickStartIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the function.
        /// </summary>
        public RateType Function { get; private set; }

        /// <summary>
        /// Gets the rate.
        /// </summary>
        public byte Rate { get; private set; }

        /// <summary>
        /// Gets TTL, the time to live difference.
        /// </summary>
        public byte TimeToLive { get; private set; }

        /// <summary>
        /// Gets the nonce.
        /// </summary>
        public uint Nonce { get; private set; }

        /// <summary>
        /// Gets the reserved 2 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickStartIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public QuickStartIP4Option(BinaryReader reader) : base(reader, IP4OptionType.QS)
        {
            Contract.Requires(reader != null);

            byte bits = reader.ReadByte();
            Function = (RateType)(byte)(bits >> 4);
            Rate = (byte)((byte)(bits << 4) >> 4);
            TimeToLive = reader.ReadByte();
            var bytes = reader.ReadBytes(4);
            Reserved = (byte)((byte)(bytes[3] << 6) >> 6);
            bytes[3] = (byte)((byte)(bytes[3] >> 2) + (byte)(bytes[2] << 6));
            bytes[2] = (byte)((byte)(bytes[2] >> 2) + (byte)(bytes[1] << 6));
            bytes[1] = (byte)((byte)(bytes[1] >> 2) + (byte)(bytes[0] << 6));
            bytes[0] = (byte)(bytes[0] >> 2);
            Nonce = NetworkToHostOrder.ToUInt32(bytes);
        }
    }

    /// <summary>
    /// Constants that represent time stamp types.
    /// </summary>
    public enum TimeStampType
    {
        /// <summary>
        /// Time Only
        /// </summary>
        TimeOnly = 0,

        /// <summary>
        /// Address And Time
        /// </summary>
        AddressAndTime = 1,

        /// <summary>
        /// Prespecified Address
        /// </summary>
        PrespecifiedAddress = 3
    }

    /// <summary>
    /// Time Stamp IP4 option (RFC 791).
    /// </summary>
    public sealed class TimeStampIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the pointer to byte after the <see cref="TimeStamps"/>.
        /// </summary>
        public byte Pointer { get; private set; }

        /// <summary>
        /// Gets the number of IP addresses that cannot register time stamps due to lack of space.
        /// </summary>
        public byte Overflow { get; private set; }

        /// <summary>
        /// Gets the time stamp type.
        /// </summary>
        public TimeStampType TimeStampType { get; private set; }

        /// <summary>
        /// Gets the list of IP4 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> Addresses { get; private set; }

        /// <summary>
        /// Gets the list of time stamps.
        /// </summary>
        public ReadOnlyCollection<uint> TimeStamps { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeStampIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeStampIP4Option(BinaryReader reader) : base(reader, IP4OptionType.TS)
        {
            Contract.Requires(reader != null);

            Pointer = reader.ReadByte();
            byte bits = reader.ReadByte();
            Overflow = (byte)(bits >> 4);
            TimeStampType = (TimeStampType)(byte)((byte)(bits << 4) >> 4);
            switch(TimeStampType)
            {
                case TimeStampType.TimeOnly:
                {
                    var tss = new uint[(Pointer - 5) / 4];
                    for(int i = 0, j = tss.Length; i < j; i++)
                        tss[i] = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                    TimeStamps = new ReadOnlyCollection<uint>(tss);
                    Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - (Pointer - 1)));
                    break;
                }
                case TimeStampType.AddressAndTime:
                {
                    var ips = new IPAddress[(Pointer - 5) / 8];
                    var tss = new uint[ips.Length];
                    for(int i = 0, j = tss.Length; i < j; i++)
                    {
                        ips[i] = new IPAddress(reader.ReadBytes(4));
                        tss[i] = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                    }
                    Addresses = new ReadOnlyCollection<IPAddress>(ips);
                    TimeStamps = new ReadOnlyCollection<uint>(tss);
                    Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - (Pointer - 1)));
                    break;
                }
                case TimeStampType.PrespecifiedAddress:
                {
                    var ips = new IPAddress[(Length - 4) / 8];
                    var tss = new uint[ips.Length];
                    for(int i = 0, j = tss.Length; i < j; i++)
                    {
                        ips[i] = new IPAddress(reader.ReadBytes(4));
                        tss[i] = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                    }
                    Addresses = new ReadOnlyCollection<IPAddress>(ips);
                    TimeStamps = new ReadOnlyCollection<uint>(tss);
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException("reader", (int)TimeStampType, typeof(TimeStampType));
            }
        }
    }

    /// <summary>
    /// Trace Route IP4 option (RFC 1393, RFC 6814).
    /// </summary>
    public sealed class TraceRouteIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the identifier number.
        /// </summary>
        public ushort IdNumber { get; private set; }

        /// <summary>
        /// Gets OHC, the number of routers through which the outbound packet has passed.
        /// </summary>
        public ushort OutboundHopCount { get; private set; }

        /// <summary>
        /// Gets RHC, the number of routers through which the return packet has passed. 
        /// </summary>
        public ushort ReturnHopCount { get; private set; }

        /// <summary>
        /// Gets the IP4 address of the originator of the outbound packet.
        /// </summary>
        public IPAddress OriginatorIPAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceRouteIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TraceRouteIP4Option(BinaryReader reader) : base(reader, IP4OptionType.TR)
        {
            Contract.Requires(reader != null);

            IdNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            OutboundHopCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ReturnHopCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            OriginatorIPAddress = new IPAddress(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Constants that represent classification levels.
    /// </summary>
    public enum ClassificationLevel
    {
        /// <summary>
        /// Top Secret
        /// </summary>
        TopSecret = 61,

        /// <summary>
        /// Secret
        /// </summary>
        Secret = 90,

        /// <summary>
        /// Confidential
        /// </summary>
        Confidential = 150,

        /// <summary>
        /// Unclassified
        /// </summary>
        Unclassified = 171
    }

    /// <summary>
    /// Constants that represent protection authorities.
    /// </summary>
    [Flags]
    public enum ProtectionAuthorities
    {
        /// <summary>
        /// Department Of Energy
        /// </summary>
        DOE = 2048,

        /// <summary>
        /// National Security Agency
        /// </summary>
        NSA = 4096,

        /// <summary>
        /// Central Intelligence Agency
        /// </summary>
        SCI = 8192,

        /// <summary>
        /// Joint Chiefs Of Staff
        /// </summary>
        SIOP_ESI = 16384,

        /// <summary>
        /// Designated Approving Authority
        /// </summary>
        GENSER = 32768
    }

    /// <summary>
    /// Security IP4 option (RFC 1108).
    /// </summary>
    public sealed class SecurityIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the classification level at which the datagram must be protected.
        /// </summary>
        public ClassificationLevel ClassificationLevel { get; private set; }

        /// <summary>
        /// Gets the protection authority which specifies rules for transmission and processing of the information contained in the datagram.
        /// </summary>
        public ProtectionAuthorities ProtectionAuthority { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SecurityIP4Option(BinaryReader reader) : base(reader, IP4OptionType.SEC)
        {
            Contract.Requires(reader != null);

            ClassificationLevel = (ClassificationLevel)reader.ReadByte();
            switch(Length)
            {
                case 3:
                    return;
                case 4:
                {
                    ProtectionAuthority = (ProtectionAuthorities)NetworkToHostOrder.ToUInt16(new[] { reader.ReadByte(), new byte() });
                    break;
                }
                case 5:
                {
                    ProtectionAuthority = (ProtectionAuthorities)NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(reader.ReadByte() >> 1) << 1), reader.ReadByte() });
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException("reader", Length, @"Argument Out Of Range Exception");
            }
        }
    }

    /// <summary>
    /// Source Route IP4 option (RFC 791).
    /// </summary>
    public sealed class SourceRouteIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the pointer to the byte which begins the next source IP4 address to be processed in the <see cref="RouteData"/>.
        /// </summary>
        public byte Pointer { get; private set; }

        /// <summary>
        /// Gets the list of IP4 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> RouteData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceRouteIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of source route option types.</param>
        public SourceRouteIP4Option(BinaryReader reader, IP4OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == IP4OptionType.LSR || type == IP4OptionType.SSR);

            Pointer = (byte)(reader.ReadByte() / 4);
            var rd = new IPAddress[(Length - 3) / 4];
            for(int i = 0, j = rd.Length; i < j; i++)
                rd[i] = new IPAddress(reader.ReadBytes(4));
            RouteData = new ReadOnlyCollection<IPAddress>(rd);
        }
    }

    /// <summary>
    /// Extended Security IP4 option (RFC 1108).
    /// </summary>
    public sealed class ExtendedSecurityIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the syntax and semantics for the <see cref="AdditionalSecurityInformation"/>.
        /// </summary>
        public byte FormatCode { get; private set; }

        /// <summary>
        /// Gets the additional security labeling information.
        /// </summary>
        public ReadOnlyCollection<byte> AdditionalSecurityInformation { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedSecurityIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ExtendedSecurityIP4Option(BinaryReader reader) : base(reader, IP4OptionType.E_SEC)
        {
            Contract.Requires(reader != null);

            FormatCode = reader.ReadByte();
            AdditionalSecurityInformation = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - 3));
        }
    }

    /// <summary>
    /// Stream Identifier IP4 option (RFC 791, RFC 6814).
    /// </summary>
    public sealed class StreamIdentifierIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the stream identifier.
        /// </summary>
        public ushort StreamId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamIdentifierIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public StreamIdentifierIP4Option(BinaryReader reader) : base(reader, IP4OptionType.SID)
        {
            Contract.Requires(reader != null);

            StreamId = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Router Alert IP4 option (RFC 2113).
    /// </summary>
    public sealed class RouterAlertIP4Option : IP4Option
    {
        /// <summary>
        /// Gets the value specifying what action the router should take.
        /// </summary>
        public ushort Value { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterAlertIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterAlertIP4Option(BinaryReader reader) : base(reader, IP4OptionType.RTRALT)
        {
            Contract.Requires(reader != null);

            Value = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Selective Directed Broadcast Mode IP4 option (RFC 6814).
    /// </summary>
    public sealed class SelectiveDirectedBroadcastModeIP4Option : IP4Option
    {
        /// <summary>
        /// Gets a list of IP4 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> Addresses { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectiveDirectedBroadcastModeIP4Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SelectiveDirectedBroadcastModeIP4Option(BinaryReader reader) : base(reader, IP4OptionType.SDBM)
        {
            Contract.Requires(reader != null);

            var ips = new IPAddress[(Length - 2) / 4];
            for(int i = 0, j = ips.Length; i < j; i++)
                ips[i] = new IPAddress(reader.ReadBytes(4));
            Addresses = new ReadOnlyCollection<IPAddress>(ips);
        }
    }
}