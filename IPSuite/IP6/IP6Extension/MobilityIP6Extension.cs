﻿namespace IPSuite.IP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Constants that represent Mobility IP6 extension types.
    /// </summary>
    public enum MobilityIP6ExtensionType
    {
        /// <summary>
        /// Binding Refresh Request (RFC 6275)
        /// </summary>
        BRR = 0,

        /// <summary>
        /// Home Test Initiate (RFC 6275)
        /// </summary>
        HoTI = 1,

        /// <summary>
        /// Care-of Test Initiate (RFC 6275)
        /// </summary>
        CoTI = 2,

        /// <summary>
        /// Home Test (RFC 6275)
        /// </summary>
        HoT = 3,

        /// <summary>
        /// Care-of Test (RFC 6275)
        /// </summary>
        CoT = 4,

        /// <summary>
        /// Binding Update (RFC 6275)
        /// </summary>
        BU = 5,

        /// <summary>
        /// Binding Acknowledgment (RFC 6275)
        /// </summary>
        BA = 6,

        /// <summary>
        /// Binding Error (RFC 6275)
        /// </summary>
        BE = 7,

        /// <summary>
        /// Fast Binding Update (RFC 5568)
        /// </summary>
        FBU = 8,

        /// <summary>
        /// Fast Binding Acknowledgment (RFC 5568)
        /// </summary>
        FBack = 9,

        /// <summary>
        /// Unsolicited Neighbor Advertisement (RFC 5568)
        /// </summary>
        UNA = 10,

        /// <summary>
        /// Experimental (RFC 5096)
        /// </summary>
        Experimental = 11,

        /// <summary>
        /// Home Agent Switch (RFC 5142)
        /// </summary>
        HAS = 12,

        /// <summary>
        /// Heartbeat (RFC 5847)
        /// </summary>
        Hb = 13,

        /// <summary>
        /// Handover Initiate (RFC 5568)
        /// </summary>
        HI = 14,

        /// <summary>
        /// Handover Acknowledge (RFC 5568)
        /// </summary>
        HAck = 15,

        /// <summary>
        /// Binding Revocation (RFC 5846)
        /// </summary>
        BR = 16,

        /// <summary>
        /// Localized Routing Initiation (RFC 6705)
        /// </summary>
        LRI = 17,

        /// <summary>
        /// Localized Routing Acknowledgment (RFC 6705)
        /// </summary>
        LRA = 18,

        /// <summary>
        /// Update Notification (RFC 7077)
        /// </summary>
        UPN = 19,

        /// <summary>
        /// Update Notification Acknowledgment (RFC 7077)
        /// </summary>
        UPA = 20,

        /// <summary>
        /// Flow Binding (RFC 7109)
        /// </summary>
        FB = 21
    }

    /// <summary>
    /// Mobility IP6 extension (RFC 6725).
    /// </summary>
    public abstract class MobilityIP6Extension : IP6Extension
    {
        /// <summary>
        /// Gets the mobility type.
        /// </summary>
        public MobilityIP6ExtensionType MobilityType { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the list of Mobility IP6 options.
        /// </summary>
        public ReadOnlyCollection<MobilityIP6Option> MobilityOptions { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The mobility type.</param>
        protected MobilityIP6Extension(BinaryReader reader, MobilityIP6ExtensionType type) : base(reader)
        {
            Contract.Requires(reader != null);

            MobilityType = type;
            Reserved = reader.ReadByte();
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }

        /// <summary>
        /// Sets the Mobility IP6 options.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="length">The options length.</param>
        protected void SetMobilityOptions(BinaryReader reader, ushort length)
        {
            Contract.Requires(reader != null);

            var opts = new List<MobilityIP6Option>();
            int index = 0;
            while(index < length)
            {
                var o = MobilityIP6Option.Create(reader);
                opts.Add(o);
                if(o.MobilityType == MobilityIP6OptionType.Pad1)
                    index += 1;
                else
                    index += 2 + o.DataLength;
            }
            MobilityOptions = new ReadOnlyCollection<MobilityIP6Option>(opts.ToArray());
        }

        /// <summary>
        /// Creates a Mobility IP6 extension.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A Mobility IP6 extension.
        /// </returns>
        public static MobilityIP6Extension Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (MobilityIP6ExtensionType)reader.ReadByte();
            switch(type)
            {
                case MobilityIP6ExtensionType.BRR:
                    return new BindingRefreshRequestMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.HoTI:
                    return new HomeTestInitiateMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.CoTI:
                    return new CareOfTestInitiateMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.HoT:
                    return new HomeTestMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.CoT:
                    return new CareOfTestMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.BU:
                case MobilityIP6ExtensionType.FBU:
                    return new BindingUpdateMobilityIP6Extension(reader, type);
                case MobilityIP6ExtensionType.BA:
                    return new BindingAcknowledgmentMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.FBack:
                    return new FastBindingAcknowledgmentMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.BE:
                    return new BindingErrorMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.HI:
                    return new HandoverInitiateMobilityIP6Extension(reader);
                case MobilityIP6ExtensionType.HAck:
                    return new HandoverAcknowledgeMobilityIP6Extension(reader);
                default:
                    return new GenericMobilityIP6Extension(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class GenericMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the extension data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The mobility type.</param>
        public GenericMobilityIP6Extension(BinaryReader reader, MobilityIP6ExtensionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes((ExtensionLength * 8) + 2));
        }
    }

    /// <summary>
    /// Binding Refresh Request Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class BindingRefreshRequestMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingRefreshRequestMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public BindingRefreshRequestMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.BRR)
        {
            Contract.Requires(reader != null);

            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            SetMobilityOptions(reader, (ushort)(ExtensionLength * 8));
        }
    }

    /// <summary>
    /// Home Test Initiate Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class HomeTestInitiateMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Gets the home initiate cookie.
        /// </summary>
        public ulong HomeInitiateCookie { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeTestInitiateMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HomeTestInitiateMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.HoTI)
        {
            Contract.Requires(reader != null);

            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            HomeInitiateCookie = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 8));
        }
    }

    /// <summary>
    /// Care-of Test Initiate Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class CareOfTestInitiateMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Gets the care-of initiate cookie.
        /// </summary>
        public ulong CareOfInitiateCookie { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CareOfTestInitiateMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CareOfTestInitiateMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.CoTI)
        {
            Contract.Requires(reader != null);

            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            CareOfInitiateCookie = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 8));
        }
    }

    /// <summary>
    /// Home Test Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class HomeTestMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the home nonce index.
        /// </summary>
        public ushort HomeNonceIndex { get; private set; }

        /// <summary>
        /// Gets the home initiate cookie.
        /// </summary>
        public ulong HomeInitiateCookie { get; private set; }

        /// <summary>
        /// Gets the home keygen token used in the return route-ability procedure.
        /// </summary>
        public ulong HomeKeygenToken { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeTestMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HomeTestMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.HoT)
        {
            Contract.Requires(reader != null);

            HomeNonceIndex = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            HomeInitiateCookie = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            HomeKeygenToken = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 16));
        }
    }

    /// <summary>
    /// Care-of Test Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class CareOfTestMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the care-of nonce index.
        /// </summary>
        public ushort CareOfNonceIndex { get; private set; }

        /// <summary>
        /// Gets the care-of initiate cookie.
        /// </summary>
        public ulong CareOfInitiateCookie { get; private set; }

        /// <summary>
        /// Gets the care-of keygen token used in the return route-ability procedure.
        /// </summary>
        public ulong CareOfKeygenToken { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CareOfTestMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CareOfTestMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.CoT)
        {
            Contract.Requires(reader != null);

            CareOfNonceIndex = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            CareOfInitiateCookie = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            CareOfKeygenToken = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 16));
        }
    }

    /// <summary>
    /// Binding Update Mobility IP6 extension (RFC 6725, RFC 5568).
    /// </summary>
    public sealed class BindingUpdateMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool Acknowledge { get; private set; }

        /// <summary>
        /// Gets a value indicating whether H flag is set.
        /// </summary>
        public bool HomeRegistration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether L flag is set.
        /// </summary>
        public bool LinkLocalAddressCompatibility { get; private set; }

        /// <summary>
        /// Gets a value indicating whether K flag is set.
        /// </summary>
        public bool KeyManagementMobilityCapability { get; private set; }

        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool MobilityAnchorPointRegistration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool MobileRouter { get; private set; }

        /// <summary>
        /// Gets a value indicating whether P flag is set.
        /// </summary>
        public bool ProxyRegistration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether B flag is set.
        /// </summary>
        public bool Buffering { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the number of 4-second units for BU, or seconds for FBU.
        /// </summary>
        public ushort Lifetime { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingUpdateMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of binding update mobility types.</param>
        public BindingUpdateMobilityIP6Extension(BinaryReader reader, MobilityIP6ExtensionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == MobilityIP6ExtensionType.BU || type == MobilityIP6ExtensionType.FBU);

            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            Acknowledge = Convert.ToBoolean((byte)(bits >> 7));
            HomeRegistration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            LinkLocalAddressCompatibility = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            KeyManagementMobilityCapability = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            MobilityAnchorPointRegistration = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            MobileRouter = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            ProxyRegistration = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
            Buffering = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            Reserved2 = reader.ReadByte();
            Lifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 4));
        }
    }

    /// <summary>
    /// Acknowledgment Mobility IP6 extension (RFC 5568, RFC 6725).
    /// </summary>
    public abstract class AcknowledgmentMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public byte Status { get; private set; }

        /// <summary>
        /// Gets a value indicating whether K flag is set.
        /// </summary>
        public bool KeyManagementMobilityCapability { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool MobileRouter { get; private set; }

        /// <summary>
        /// Gets a value indicating whether P flag is set.
        /// </summary>
        public bool ProxyRegistration { get; private set; }

        /// <summary>
        /// Gets the reserved 5 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the number of 4-second units for BA, or seconds for FBA.
        /// </summary>
        public ushort Lifetime { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AcknowledgmentMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of acknowledgment mobility types.</param>
        protected AcknowledgmentMobilityIP6Extension(BinaryReader reader, MobilityIP6ExtensionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == MobilityIP6ExtensionType.BA || type == MobilityIP6ExtensionType.FBack);

            Status = reader.ReadByte();
            var bits = reader.ReadByte();
            KeyManagementMobilityCapability = Convert.ToBoolean((byte)(bits >> 7));
            MobileRouter = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            ProxyRegistration = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Reserved2 = (byte)((byte)(bits << 3) >> 3);
            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Lifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 4));
        }
    }

    /// <summary>
    /// Constants that represent binding acknowledgment statuses.
    /// </summary>
    public enum BindingAcknowledgmentStatus
    {
        /// <summary>
        /// Binding Update accepted.
        /// </summary>
        BindingUpdateAccepted = 0,

        /// <summary>
        /// Prefix discovery necessary.
        /// </summary>
        PrefixDiscoveryNecessary = 1,

        /// <summary>
        /// Reason unspecified.
        /// </summary>
        ReasonUnspecified = 128,

        /// <summary>
        /// Administratively prohibited.
        /// </summary>
        AdministrativelyProhibited = 129,

        /// <summary>
        /// Insufficient resources.
        /// </summary>
        InsufficientResources = 130,

        /// <summary>
        /// Home registration not supported.
        /// </summary>
        HomeRegistrationNotSupported = 131,

        /// <summary>
        /// Not home subnet.
        /// </summary>
        NotHomeSubnet = 132,

        /// <summary>
        /// Not home agent.
        /// </summary>
        NotHomeAgent = 133,

        /// <summary>
        /// Duplicate Address Detection failed.
        /// </summary>
        DuplicateAddressDetectionFailed = 134,

        /// <summary>
        /// Sequence number out of window.
        /// </summary>
        SequenceNumberOutOfWindow = 135,

        /// <summary>
        /// Expired home nonce index.
        /// </summary>
        ExpiredHomeNonceIndex = 136,

        /// <summary>
        /// Expired care-of nonce index.
        /// </summary>
        ExpiredCareOfNonceIndex = 137,

        /// <summary>
        /// Expired nonces.
        /// </summary>
        ExpiredNonces = 138,

        /// <summary>
        /// Registration type change disallowed.
        /// </summary>
        RegistrationTypeChangeDisallowed = 139,

        /// <summary>
        /// Mobile router operation not permitted.
        /// </summary>
        MobileRouterOperationNotPermitted = 140,

        /// <summary>
        /// Invalid prefix.
        /// </summary>
        InvalidPrefix = 141,

        /// <summary>
        /// Not authorized for prefix.
        /// </summary>
        NotAuthorizedForPrefix = 142,

        /// <summary>
        /// Forwarding setup failed.
        /// </summary>
        ForwardingSetupFailed = 143,

        /// <summary>
        /// Proxy registration not enabled.
        /// </summary>
        ProxyRegistrationNotEnabled = 152,

        /// <summary>
        /// Not Local Mobility Anchor.
        /// </summary>
        NotLocalMobilityAnchor = 153,

        /// <summary>
        /// Mobile Access Gateway not authorized for proxy registration.
        /// </summary>
        MobileAccessGatewayNotAuthorizedForProxyRegistration = 154,

        /// <summary>
        /// Not authorized for home network prefix.
        /// </summary>
        NotAuthorizedForHomeNetworkPrefix = 155,

        /// <summary>
        /// Time stamp mismatch.
        /// </summary>
        TimestampMismatch = 156,

        /// <summary>
        /// Time stamp lower than previously accepted.
        /// </summary>
        TimestampLowerThanPreviouslyAccepted = 157,

        /// <summary>
        /// Missing home network prefix option.
        /// </summary>
        MissingHomeNetworkPrefixOption = 158,

        /// <summary>
        /// BCE-PBU prefix sets do not match.
        /// </summary>
        BCE_PBU_PrefixSetsDoNotMatch = 159,

        /// <summary>
        /// Missing mobile node identifier option.
        /// </summary>
        MissingMobileNodeIdentifierOption = 160,

        /// <summary>
        /// Missing hand-off indicator option.
        /// </summary>
        MissingHandOffIndicatorOption = 161,

        /// <summary>
        /// Missing access technology type option.
        /// </summary>
        MissingAccessTechnologyTypeOption = 162,

        /// <summary>
        /// Invalid care-of address.
        /// </summary>
        InvalidCareOfAddress = 174
    }

    /// <summary>
    /// Binding Acknowledgment Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class BindingAcknowledgmentMobilityIP6Extension : AcknowledgmentMobilityIP6Extension
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public new BindingAcknowledgmentStatus Status { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingAcknowledgmentMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public BindingAcknowledgmentMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.BA)
        {
            Contract.Requires(reader != null);

            Status = (BindingAcknowledgmentStatus)base.Status;
        }
    }

    /// <summary>
    /// Constants that represent fast binding acknowledgment statuses.
    /// </summary>
    public enum FastBindingAcknowledgmentStatus
    {
        /// <summary>
        /// Fast Binding Update accepted.
        /// </summary>
        FastBindingUpdateAccepted = 0,

        /// <summary>
        /// Invalid new care-of address.
        /// </summary>
        InvalidNewCareOfAddress = 1,

        /// <summary>
        /// Reason unspecified.
        /// </summary>
        ReasonUnspecified = 128,

        /// <summary>
        /// Administratively prohibited.
        /// </summary>
        AdministrativelyProhibited = 129,

        /// <summary>
        /// Insufficient resources.
        /// </summary>
        InsufficientResources = 130,

        /// <summary>
        /// Incorrect interface identifier length.
        /// </summary>
        IncorrectInterfaceIdentifierLength = 131
    }

    /// <summary>
    /// Binding Acknowledgment Mobility IP6 extension (RFC 5568).
    /// </summary>
    public sealed class FastBindingAcknowledgmentMobilityIP6Extension : AcknowledgmentMobilityIP6Extension
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public new FastBindingAcknowledgmentStatus Status { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FastBindingAcknowledgmentMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FastBindingAcknowledgmentMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.FBack)
        {
            Contract.Requires(reader != null);

            Status = (FastBindingAcknowledgmentStatus)base.Status;
        }
    }

    /// <summary>
    /// Constants that represent binding error statuses.
    /// </summary>
    public enum BindingErrorStatus
    {
        /// <summary>
        /// Unknown binding for home address.
        /// </summary>
        UnknownBindingForHomeAddress = 1,

        /// <summary>
        /// Unrecognized mobility type.
        /// </summary>
        UnrecognizedMobilityType = 2
    }

    /// <summary>
    /// Binding Error Mobility IP6 extension (RFC 6725).
    /// </summary>
    public sealed class BindingErrorMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public BindingErrorStatus Status { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the home IP6 address.
        /// </summary>
        public IPAddress HomeAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingErrorMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public BindingErrorMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.BE)
        {
            Contract.Requires(reader != null);

            Status = (BindingErrorStatus)reader.ReadByte();
            Reserved2 = reader.ReadByte();
            HomeAddress = new IPAddress(reader.ReadBytes(16));
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 16));
        }
    }

    /// <summary>
    /// Constants that represent handover initiate codes.
    /// </summary>
    public enum HandoverInitiateCode
    {
        /// <summary>
        /// Previous care-of address.
        /// </summary>
        PreviousCareOfAddress = 0,

        /// <summary>
        /// Not previous care-of address.
        /// </summary>
        NotPreviousCareOfAddress = 1
    }

    /// <summary>
    /// Handover Initiate Mobility IP6 extension (RFC 5568).
    /// </summary>
    public sealed class HandoverInitiateMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether S flag is set.
        /// </summary>
        public bool AssignedAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether U flag is set.
        /// </summary>
        public bool Buffer { get; private set; }

        /// <summary>
        /// Gets the reserved 6 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public HandoverInitiateCode Code { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverInitiateMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoverInitiateMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.HI)
        {
            Contract.Requires(reader != null);

            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            AssignedAddressConfiguration = Convert.ToBoolean((byte)(bits >> 7));
            Buffer = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Reserved2 = (byte)((byte)(bits << 2) >> 2);
            Code = (HandoverInitiateCode)reader.ReadByte();
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 2));
        }
    }

    /// <summary>
    /// Constants that represent handover acknowledge codes.
    /// </summary>
    public enum HandoverAcknowledgeCode
    {
        /// <summary>
        /// Handover accepted.
        /// </summary>
        HandoverAccepted = 0,

        /// <summary>
        /// Invalid new care-of address.
        /// </summary>
        InvalidNewCareOfAddress = 1,

        /// <summary>
        /// New care-of address assigned.
        /// </summary>
        NewCareOfAddressAssigned = 2,

        /// <summary>
        /// Use previous care-of address.
        /// </summary>
        UsePreviousCareOfAddress = 3,

        /// <summary>
        /// Sent unsolicited.
        /// </summary>
        SentUnsolicited = 4,

        /// <summary>
        /// Reason unspecified.
        /// </summary>
        ReasonUnspecified = 128,

        /// <summary>
        /// Administratively prohibited.
        /// </summary>
        AdministrativelyProhibited = 129,

        /// <summary>
        /// Insufficient resources.
        /// </summary>
        InsufficientResources = 130
    }

    /// <summary>
    /// Handover Acknowledge Mobility IP6 extension (RFC 5568).
    /// </summary>
    public sealed class HandoverAcknowledgeMobilityIP6Extension : MobilityIP6Extension
    {
        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public HandoverAcknowledgeCode Code { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverAcknowledgeMobilityIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoverAcknowledgeMobilityIP6Extension(BinaryReader reader) : base(reader, MobilityIP6ExtensionType.HAck)
        {
            Contract.Requires(reader != null);

            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Reserved2 = reader.ReadByte();
            Code = (HandoverAcknowledgeCode)reader.ReadByte();
            SetMobilityOptions(reader, (ushort)((ExtensionLength * 8) - 2));
        }
    }
}