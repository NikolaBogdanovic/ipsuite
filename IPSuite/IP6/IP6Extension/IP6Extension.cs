﻿namespace IPSuite.IP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.IP4;

    /// <summary>
    /// Internet Protocol version 6 extension (RFC 2460). 
    /// </summary>
    public class IP6Extension
    {
        /// <summary>
        /// Gets the next header, specifying the next encapsulated IP6 extension or internet protocol.
        /// </summary>
        public InternetProtocol NextHeader { get; private set; }

        /// <summary>
        /// Gets the number of 8-byte units in the extension, excluding the first 8 bytes.
        /// </summary>
        public byte ExtensionLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public IP6Extension(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            NextHeader = (InternetProtocol)reader.ReadByte();
            ExtensionLength = reader.ReadByte();
        }
    }

    /// <summary>
    /// Hop-by-Hop and Destination Options IP6 extension (RFC 2460).
    /// </summary>
    public sealed class OptionsIP6Extension : IP6Extension
    {
        /// <summary>
        /// Gets the list of IP6 options.
        /// </summary>
        public ReadOnlyCollection<IP6Option> Options { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public OptionsIP6Extension(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            var opts = new List<IP6Option>();
            int i = 0, j = 6 + (ExtensionLength * 8);
            while(i < j)
            {
                var o = IP6Option.Create(reader);
                opts.Add(o);
                if(o.OptionType == IP6OptionType.Pad1)
                    i += 1;
                else
                    i += o.DataLength + 2;
            }
            Options = new ReadOnlyCollection<IP6Option>(opts.ToArray());
        }
    }

    /// <summary>
    /// Constants that represent routing types.
    /// </summary>
    public enum RoutingType
    {
        /// <summary>
        /// Type 0
        /// </summary>
        Type0 = 0,

        /// <summary>
        /// Nimrod
        /// </summary>
        Nimrod = 1,

        /// <summary>
        /// Type 2
        /// </summary>
        Type2 = 2,

        /// <summary>
        /// Routing Protocol for Low-power and Lossy networks
        /// </summary>
        RPL = 3
    }

    /// <summary>
    /// Routing IP6 extension (RFC 2460, RFC 5095).
    /// </summary>
    public sealed class RoutingIP6Extension : IP6Extension
    {
        /// <summary>
        /// Gets the routing type.
        /// </summary>
        public RoutingType RoutingType { get; private set; }

        /// <summary>
        /// Gets the segments left, the number of nodes this packet still has to visit before reaching its final destination.
        /// </summary>
        public byte SegmentsLeft { get; private set; }

        /// <summary>
        /// Gets the number of prefix bytes from each segment, except the last one, that are elided.
        /// </summary>
        public byte CmprI { get; private set; }

        /// <summary>
        /// Gets the number of prefix bytes from the last segment, that are elided.
        /// </summary>
        public byte CmprE { get; private set; }

        /// <summary>
        /// Gets the number of bytes that are used for padding after the last segment.
        /// </summary>
        public byte Pad { get; private set; }

        /// <summary>
        /// Gets the reserved 20 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the list of IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> Addresses { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoutingIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RoutingIP6Extension(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            RoutingType = (RoutingType)reader.ReadByte();
            SegmentsLeft = reader.ReadByte();
            var bits = reader.ReadByte();
            CmprI = (byte)(bits >> 4);
            CmprE = (byte)((byte)(bits << 4) >> 4);
            bits = reader.ReadByte();
            Pad = (byte)(bits >> 4);
            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 4) >> 4), reader.ReadByte(), reader.ReadByte() });
            switch(RoutingType)
            {
                case RoutingType.Type0:
                case RoutingType.Type2:
                case RoutingType.RPL:
                {
                    var ips = new IPAddress[(((ExtensionLength * 8) - Pad - (16 - CmprE)) / (16 - CmprI)) + 1];
                    if(ips.Length > 0)
                    {
                        for(int i = 0, j = ips.Length - 1; i < j; i++)
                            ips[i] = new IPAddress(reader.ReadBytes(16 - CmprI));
                        ips[ips.Length - 1] = new IPAddress(reader.ReadBytes(16 - CmprE));
                    }
                    Addresses = new ReadOnlyCollection<IPAddress>(ips);
                    Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(Pad));
                    break;
                }
                default:
                {
                    Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(ExtensionLength * 8));
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Fragment IP6 extension (RFC 2460).
    /// </summary>
    public sealed class FragmentIP6Extension : IP6Extension
    {
        /// <summary>
        /// Gets the fragment offset, in 8-byte units, relative to the start of the fragmentable part of the original packet.
        /// </summary>
        public ushort FragmentOffset { get; private set; }

        /// <summary>
        /// Gets the reserved 2 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool MoreFragments { get; private set; }

        /// <summary>
        /// Gets the identification.
        /// </summary>
        public uint Identification { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FragmentIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FragmentIP6Extension(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            var bytes = reader.ReadBytes(2);
            Reserved = (byte)((byte)(bytes[1] << 5) >> 6);
            MoreFragments = Convert.ToBoolean((byte)((byte)(bytes[1] << 7) >> 7));
            bytes[1] = (byte)((byte)(bytes[1] >> 3) + (byte)(bytes[0] << 5));
            bytes[0] = (byte)(bytes[0] >> 3);
            FragmentOffset = NetworkToHostOrder.ToUInt16(bytes);
            Identification = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Authentication IP6 extension (RFC 4302).
    /// </summary>
    public sealed class AuthenticationIP6Extension : IP6Extension
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets SPI, the arbitrary value which is used to identify the security association of the receiving party.
        /// </summary>
        public uint SecurityParameterIndex { get; private set; }

        /// <summary>
        /// Gets the sequence number used to prevent reply attacks.
        /// </summary>
        public uint SequenceNumber { get; private set; }

        /// <summary>
        /// Gets ICV, used for padding.
        /// </summary>
        public ReadOnlyCollection<byte> IntegrityCheckValue { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationIP6Extension"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AuthenticationIP6Extension(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            SecurityParameterIndex = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SequenceNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            IntegrityCheckValue = new ReadOnlyCollection<byte>(reader.ReadBytes((ExtensionLength * 8) - 4));
        }
    }
}