﻿namespace IPSuite.IP6
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.IP4;

    /// <summary>
    /// Internet Protocol version 6 header (RFC 2460).
    /// </summary>
    public sealed class IP6Header
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        public byte Version { get; private set; }

        /// <summary>
        /// Gets the internet traffic class priority delivery value.
        /// </summary>
        public byte TrafficClass { get; private set; }

        /// <summary>
        /// Gets DS.
        /// </summary>
        public byte DifferentiatedServices { get; private set; }

        /// <summary>
        /// Gets ECN.
        /// </summary>
        public ExplicitCongestionNotification ExplicitCongestionNotification { get; private set; }

        /// <summary>
        /// Gets the flow label used for specifying special router handling from source to destination for a sequence of packets.
        /// </summary>
        public uint FlowLabel { get; private set; }

        /// <summary>
        /// Gets the payload length of the data in the packet. When cleared to zero, use the <see cref="JumboPayloadLength"/> instead.
        /// </summary>
        public ushort PayloadLength { get; private set; }

        /// <summary>
        /// Gets the next header, specifying the next encapsulated IP6 extension.
        /// </summary>
        public InternetProtocol NextHeader { get; private set; }

        /// <summary>
        /// Gets the hop limit that replaces the TTL in the IP4 header that was originally intended to be used as a time based hop limit.
        /// </summary>
        public byte HopLimit { get; private set; }

        /// <summary>
        /// Gets the IP6 address of the sending node.
        /// </summary>
        public IPAddress SourceAddress { get; private set; }

        /// <summary>
        /// Gets the IP6 address of the receiving node.
        /// </summary>
        public IPAddress DestinationAddress { get; private set; }

        /// <summary>
        /// Gets the list of IP6 extensions.
        /// </summary>
        public ReadOnlyCollection<IP6Extension> Extensions { get; private set; }

        /// <summary>
        /// Gets the IP6 extensions length in bytes.
        /// </summary>
        public uint ExtensionsLength { get; private set; }

        /// <summary>
        /// Gets the jumbo payload length in bytes.
        /// </summary>
        public uint JumboPayloadLength { get; private set; }

        /// <summary>
        /// Gets the next encapsulated protocol that is not an IP6 extension.
        /// </summary>
        public InternetProtocol Protocol { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public IP6Header(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var bytes = reader.ReadBytes(4);
            Version = (byte)(bytes[0] >> 4);
            TrafficClass = (byte)((byte)(bytes[0] << 4) + (byte)(bytes[1] >> 4));
            DifferentiatedServices = (byte)(TrafficClass >> 2);
            ExplicitCongestionNotification = (ExplicitCongestionNotification)(byte)((byte)(TrafficClass << 6) >> 6);
            FlowLabel = NetworkToHostOrder.ToUInt32(new[] { (byte)0, (byte)((byte)(bytes[1] << 4) >> 4), bytes[2], bytes[3] });
            JumboPayloadLength = PayloadLength = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Protocol = NextHeader = (InternetProtocol)reader.ReadByte();
            HopLimit = reader.ReadByte();
            SourceAddress = new IPAddress(reader.ReadBytes(16));
            DestinationAddress = new IPAddress(reader.ReadBytes(16));
            uint i = 0;
            var exts = new List<IP6Extension>();
            while(Protocol != InternetProtocol.IP6_NoNxt)
            {
                IP6Extension e = null;
                switch(NextHeader)
                {
                    case InternetProtocol.IP6_HopOpt:
                    case InternetProtocol.IP6_Opts:
                    {
                        e = new OptionsIP6Extension(reader);
                        foreach(IP6Option opt in ((OptionsIP6Extension)e).Options)
                        {
                            var jumbo = opt as JumboPayloadIP6Option;
                            if(jumbo != null)
                                JumboPayloadLength = jumbo.JumboPayloadLength;
                        }
                        break;
                    }
                    case InternetProtocol.IP6_Route:
                    {
                        e = new RoutingIP6Extension(reader);
                        break;
                    }
                    case InternetProtocol.IP6_Frag:
                    {
                        e = new FragmentIP6Extension(reader);
                        break;
                    }
                    case InternetProtocol.IP6_AH:
                    {
                        e = new AuthenticationIP6Extension(reader);
                        break;
                    }
                    case InternetProtocol.IP6_MH:
                    {
                        e = MobilityIP6Extension.Create(reader);
                        break;
                    }
                }
                if(e == null)
                    break;
                exts.Add(e);
                Protocol = e.NextHeader;
                i += (uint)8 + e.ExtensionLength;
            }
            Extensions = new ReadOnlyCollection<IP6Extension>(exts.ToArray());
            ExtensionsLength = i;
        }
    }
}