﻿namespace IPSuite.IP6
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.IP4;

    /// <summary>
    /// Constants that represent IP6 option types.
    /// </summary>
    public enum IP6OptionType
    {
        /// <summary>
        /// Pad 1 (RFC 2460)
        /// </summary>
        Pad1 = 0,

        /// <summary>
        /// Pad N (RFC 2460)
        /// </summary>
        PadN = 1,

        /// <summary>
        /// Tunnel Encapsulation Limit (RFC 2473)
        /// </summary>
        TEL = 4,

        /// <summary>
        /// Router Alert (RFC 2711)
        /// </summary>
        RA = 5,

        /// <summary>
        /// Common Architecture Label IP6 Security Option (RFC 5570)
        /// </summary>
        CALIPSO = 7,

        /// <summary>
        /// Simplified Multicast Forwarding (RFC 6621)
        /// </summary>
        SMF = 8,

        /// <summary>
        /// Quick-Start (RFC 4782)
        /// </summary>
        QS = 38,

        /// <summary>
        /// Routing Protocol for Low-power and Lossy networks (RFC 6553)
        /// </summary>
        RPL = 99,

        /// <summary>
        /// Identifier-Locator Network Protocol Nonce (RFC 6744)
        /// </summary>
        ILNP_Nonce = 139,

        /// <summary>
        /// Line-Identification (RFC 6788)
        /// </summary>
        LI = 140,

        /// <summary>
        /// Jumbo Payload (RFC 2675)
        /// </summary>
        JP = 194,

        /// <summary>
        /// Home Address (RFC 6725)
        /// </summary>
        HoA = 201,

        /// <summary>
        /// Depth-First Forwarding (RFC 6977)
        /// </summary>
        DFF = 238
    }

    /// <summary>
    /// Constants that represent IP6 option actions.
    /// </summary>
    public enum IP6OptionAction
    {
        /// <summary>
        /// Skip option.
        /// </summary>
        SkipOption = 0,

        /// <summary>
        /// Discard packet.
        /// </summary>
        DiscardPacket = 1,

        /// <summary>
        /// Discard packet, and send ICMP Parameter Problem Code 2.
        /// </summary>
        DiscardPacketAndSendICMP = 2,

        /// <summary>
        /// Discard packet, and if destination address is not multicast, send ICMP Parameter Problem Code 2.
        /// </summary>
        DiscardPacketAndIfDestinationNotMulticastSendICMP = 3
    }

    /// <summary>
    /// Internet Protocol version 6 option.
    /// </summary>
    public abstract class IP6Option
    {
        /// <summary>
        /// Gets the option type.
        /// </summary>
        public IP6OptionType OptionType { get; private set; }

        /// <summary>
        /// Gets the action that must be taken if the processing IP6 node does not recognize the option type.
        /// </summary>
        public IP6OptionAction Action { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the option data can change en-route to the packet's final destination.
        /// </summary>
        public bool Change { get; private set; }

        /// <summary>
        /// Gets the rest subfield.
        /// </summary>
        public byte Rest { get; private set; }

        /// <summary>
        /// Gets the option data length in bytes.
        /// </summary>
        public byte DataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="optionType">The option type.</param>
        protected IP6Option(BinaryReader reader, IP6OptionType optionType)
        {
            Contract.Requires(reader != null);

            OptionType = optionType;
            Action = (IP6OptionAction)(byte)((byte)OptionType >> 6);
            Change = Convert.ToBoolean((byte)((byte)((byte)OptionType << 2) >> 6));
            Rest = (byte)((byte)((byte)OptionType << 3) >> 3);
            if(OptionType != IP6OptionType.Pad1)
                DataLength = reader.ReadByte();
        }

        /// <summary>
        /// Creates an IP6 option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// An IP6 option.
        /// </returns>
        public static IP6Option Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (IP6OptionType)reader.ReadByte();
            switch(type)
            {
                case IP6OptionType.TEL:
                    return new TunnelEncapsulationLimitIP6Option(reader);
                case IP6OptionType.RA:
                    return new RouterAlertIP6Option(reader);
                case IP6OptionType.CALIPSO:
                    return new CalipsoIP6Option(reader);
                case IP6OptionType.SMF:
                    return new SimplifiedMulticastForwardingIP6Option(reader);
                case IP6OptionType.QS:
                    return new QuickStartIP6Option(reader);
                case IP6OptionType.RPL:
                    return new RplIP6Option(reader);
                case IP6OptionType.ILNP_Nonce:
                    return new IdentifierLocatorNetworkNonceIP6Option(reader);
                case IP6OptionType.LI:
                    return new LineIdentificationIP6Option(reader);
                case IP6OptionType.JP:
                    return new JumboPayloadIP6Option(reader);
                case IP6OptionType.HoA:
                    return new HomeAddressIP6Option(reader);
                case IP6OptionType.DFF:
                    return new DepthFirstForwardingIP6Option(reader);
                default:
                    return new GenericIP6Option(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic IP6 option.
    /// </summary>
    public sealed class GenericIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="optionType">The option type.</param>
        public GenericIP6Option(BinaryReader reader, IP6OptionType optionType) : base(reader, optionType)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Tunnel Encapsulation Limit IP6 option.
    /// </summary>
    public sealed class TunnelEncapsulationLimitIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the tunnel encapsulation limit specifying how many further levels of encapsulation are permitted.
        /// </summary>
        public byte TunnelEncapsulationLimit { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TunnelEncapsulationLimitIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TunnelEncapsulationLimitIP6Option(BinaryReader reader) : base(reader, IP6OptionType.TEL)
        {
            Contract.Requires(reader != null);

            TunnelEncapsulationLimit = reader.ReadByte();
        }
    }

    /// <summary>
    /// Constants that represent router alert codes.
    /// </summary>
    public enum RouterAlertCode
    {
        /// <summary>
        /// Datagram contains Multicast Listener Discovery message.
        /// </summary>
        MulticastListenerDiscovery = 0,

        /// <summary>
        /// Datagram contains RSVP message.
        /// </summary>
        RSV = 1,

        /// <summary>
        /// Datagram contains Active Networks message.
        /// </summary>
        ActiveNetworks = 2
    }

    /// <summary>
    /// Router Alert IP6 option.
    /// </summary>
    public sealed class RouterAlertIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        public RouterAlertCode Value { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterAlertIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterAlertIP6Option(BinaryReader reader) : base(reader, IP6OptionType.RA)
        {
            Contract.Requires(reader != null);

            Value = (RouterAlertCode)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// CALIPSO IP6 option.
    /// </summary>
    public sealed class CalipsoIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the domain of interpretation.
        /// </summary>
        public uint DomainOfInterpretation { get; private set; }

        /// <summary>
        /// Gets the compartment length in 4-byte units, specifying the size of the <see cref="CompartmentBitmap"/>.
        /// </summary>
        public byte CompartmentLength { get; private set; }

        /// <summary>
        /// Gets the sensitivity level, indicating the relative sensitivity of the data contained in this datagram in the context of the <see cref="DomainOfInterpretation"/>.
        /// </summary>
        public byte SensitivityLevel { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the list of compartment bitmaps.
        /// </summary>
        public ReadOnlyCollection<ulong> CompartmentBitmap { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CalipsoIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CalipsoIP6Option(BinaryReader reader) : base(reader, IP6OptionType.CALIPSO)
        {
            Contract.Requires(reader != null);

            DomainOfInterpretation = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            CompartmentLength = reader.ReadByte();
            SensitivityLevel = reader.ReadByte();
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var words = new ulong[CompartmentLength / 2];
            for(int i = 0, j = words.Length; i < j; i++)
                words[i] = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
            CompartmentBitmap = new ReadOnlyCollection<ulong>(words);
        }
    }

    /// <summary>
    /// Constants that represent tagger types.
    /// </summary>
    public enum TaggerType
    {
        /// <summary>
        /// A TaggerId field is not present.
        /// </summary>
        NULL = 0,

        /// <summary>
        /// A TaggerId of non-specific context is present.
        /// </summary>
        DEFAULT = 1,

        /// <summary>
        /// A TaggerId representing an IP4 address is present.
        /// </summary>
        IP4 = 2,

        /// <summary>
        /// A TaggerId representing an IP6 address is present.
        /// </summary>
        IP6 = 3
    }

    /// <summary>
    /// Simplified Multicast Forwarding IP6 option.
    /// </summary>
    public sealed class SimplifiedMulticastForwardingIP6Option : IP6Option
    {
        /// <summary>
        /// Gets a value indicating whether H flag is set.
        /// </summary>
        public bool Hash { get; private set; }

        /// <summary>
        /// Gets the tagger type.
        /// </summary>
        public TaggerType TaggerType { get; private set; }

        /// <summary>
        /// Gets the tagger length in bytes.
        /// </summary>
        public byte TaggerLength { get; private set; }

        /// <summary>
        /// Gets the tagger identifier.
        /// </summary>
        public ReadOnlyCollection<byte> TaggerId { get; private set; }

        /// <summary>
        /// Gets the Duplicate Packet Detection (DPD) identifier.
        /// </summary>
        public ReadOnlyCollection<byte> Identifier { get; private set; }

        /// <summary>
        /// Gets HAV.
        /// </summary>
        public ReadOnlyCollection<byte> HashAssistValue { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimplifiedMulticastForwardingIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SimplifiedMulticastForwardingIP6Option(BinaryReader reader) : base(reader, IP6OptionType.SMF)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Hash = Convert.ToBoolean((byte)(bits >> 7));
            if(!Hash)
            {
                TaggerType = (TaggerType)(byte)((byte)(bits << 1) >> 5);
                TaggerLength = (byte)((byte)(bits << 4) >> 4);
                if(TaggerType != TaggerType.NULL)
                {
                    TaggerId = new ReadOnlyCollection<byte>(reader.ReadBytes(TaggerLength + 1));
                    Identifier = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - (TaggerLength + 1 + 1)));
                }
                else
                    Identifier = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 1));
            }
            else
            {
                var hav = new byte[DataLength];
                hav[0] = (byte)((byte)(bits << 1) >> 1);
                for(int i = 1, j = hav.Length; i < j; i++)
                    hav[i] = reader.ReadByte();
                HashAssistValue = new ReadOnlyCollection<byte>(hav);
            }
        }
    }

    /// <summary>
    /// Quick-Start IP6 option.
    /// </summary>
    public sealed class QuickStartIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the function.
        /// </summary>
        public RateType Function { get; private set; }

        /// <summary>
        /// Gets the rate.
        /// </summary>
        public byte Rate { get; private set; }

        /// <summary>
        /// Gets TTL, the time to live difference.
        /// </summary>
        public byte TimeToLive { get; private set; }

        /// <summary>
        /// Gets the nonce.
        /// </summary>
        public uint Nonce { get; private set; }

        /// <summary>
        /// Gets the reserved 2 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickStartIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public QuickStartIP6Option(BinaryReader reader) : base(reader, IP6OptionType.QS)
        {
            Contract.Requires(reader != null);

            byte bits = reader.ReadByte();
            Function = (RateType)(byte)(bits >> 4);
            Rate = (byte)((byte)(bits << 4) >> 4);
            TimeToLive = reader.ReadByte();
            var bytes = reader.ReadBytes(4);
            Reserved = (byte)((byte)(bytes[3] << 6) >> 6);
            bytes[3] = (byte)((byte)(bytes[3] >> 2) + (byte)(bytes[2] << 6));
            bytes[2] = (byte)((byte)(bytes[2] >> 2) + (byte)(bytes[1] << 6));
            bytes[1] = (byte)((byte)(bytes[1] >> 2) + (byte)(bytes[0] << 6));
            bytes[0] = (byte)(bytes[0] >> 2);
            Nonce = NetworkToHostOrder.ToUInt32(bytes);
        }
    }

    /// <summary>
    /// RPL IP6 option.
    /// </summary>
    public sealed class RplIP6Option : IP6Option
    {
        /// <summary>
        /// Gets a value indicating whether O flag is set.
        /// </summary>
        public bool Down { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool RankError { get; private set; }

        /// <summary>
        /// Gets a value indicating whether F flag is set.
        /// </summary>
        public bool ForwardingError { get; private set; }

        /// <summary>
        /// Gets the reserved 5 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the instance identifier.
        /// </summary>
        public byte InstanceId { get; private set; }

        /// <summary>
        /// Gets the sender rank.
        /// </summary>
        public ushort SenderRank { get; private set; }

        /// <summary>
        /// Gets the list of RPL sub-TLVs.
        /// </summary>
        public ReadOnlyCollection<byte> SubTypeLengthValues { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RplIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RplIP6Option(BinaryReader reader) : base(reader, IP6OptionType.RPL)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Down = Convert.ToBoolean((byte)(bits >> 7));
            RankError = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            ForwardingError = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Reserved = (byte)((byte)(bits << 3) >> 3);
            InstanceId = reader.ReadByte();
            SenderRank = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SubTypeLengthValues = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4));
        }
    }

    /// <summary>
    /// Identifier-Locator Network Nonce IP6 option.
    /// </summary>
    public sealed class IdentifierLocatorNetworkNonceIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the nonce.
        /// </summary>
        public ReadOnlyCollection<byte> Nonce { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifierLocatorNetworkNonceIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public IdentifierLocatorNetworkNonceIP6Option(BinaryReader reader) : base(reader, IP6OptionType.ILNP_Nonce)
        {
            Contract.Requires(reader != null);

            Nonce = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Line-Identification IP6 option.
    /// </summary>
    public sealed class LineIdentificationIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the line identifier length in bytes.
        /// </summary>
        public byte LineIdLength { get; private set; }

        /// <summary>
        /// Gets the line identifier.
        /// </summary>
        public ReadOnlyCollection<byte> LineId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineIdentificationIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public LineIdentificationIP6Option(BinaryReader reader) : base(reader, IP6OptionType.LI)
        {
            Contract.Requires(reader != null);

            LineIdLength = reader.ReadByte();
            LineId = new ReadOnlyCollection<byte>(reader.ReadBytes(LineIdLength));
        }
    }

    /// <summary>
    /// Jumbo Payload IP6 option.
    /// </summary>
    public sealed class JumboPayloadIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the jumbo payload length in bytes.
        /// </summary>
        public uint JumboPayloadLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JumboPayloadIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public JumboPayloadIP6Option(BinaryReader reader) : base(reader, IP6OptionType.JP)
        {
            Contract.Requires(reader != null);

            JumboPayloadLength = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Home Address IP6 option.
    /// </summary>
    public sealed class HomeAddressIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the home IP6 address of the mobile node sending the packet.
        /// </summary>
        public IPAddress HomeAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeAddressIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HomeAddressIP6Option(BinaryReader reader) : base(reader, IP6OptionType.HoA)
        {
            Contract.Requires(reader != null);

            HomeAddress = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Depth-First Forwarding IP6 option.
    /// </summary>
    public sealed class DepthFirstForwardingIP6Option : IP6Option
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        public byte Version { get; private set; }

        /// <summary>
        /// Gets a value indicating whether DUP flag is set.
        /// </summary>
        public bool DuplicatePacket { get; private set; }

        /// <summary>
        /// Gets a value indicating whether RET flag is set.
        /// </summary>
        public bool ReturnPacket { get; private set; }

        /// <summary>
        /// Gets the reserved 4 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DepthFirstForwardingIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DepthFirstForwardingIP6Option(BinaryReader reader) : base(reader, IP6OptionType.DFF)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Version = (byte)(bits >> 6);
            DuplicatePacket = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            ReturnPacket = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            Reserved = (byte)((byte)(bits << 4) >> 4);
            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }
}