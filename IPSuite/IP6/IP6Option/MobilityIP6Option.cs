﻿namespace IPSuite.IP6
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Constants that represent Mobility IP6 option types.
    /// </summary>
    public enum MobilityIP6OptionType
    {
        /// <summary>
        /// Pad 1 (RFC 6275)
        /// </summary>
        Pad1 = 0,

        /// <summary>
        /// Pad N (RFC 6275)
        /// </summary>
        PadN = 1,

        /// <summary>
        /// Binding Refresh Advice (RFC 6275)
        /// </summary>
        BRA = 2,

        /// <summary>
        /// Alternate Care-of Address (RFC 6275)
        /// </summary>
        ACoA = 3,

        /// <summary>
        /// Nonce Indexes (RFC 6275)
        /// </summary>
        NI = 4,

        /// <summary>
        /// Binding Authorization Data (RFC 6275)
        /// </summary>
        BAD = 5,

        /// <summary>
        /// Mobile Network Prefix (RFC 3963)
        /// </summary>
        MNP = 6,

        /// <summary>
        /// Link-Layer Address (RFC 5568)
        /// </summary>
        MH_LLA = 7,

        /// <summary>
        /// Mobility Node Identifier (RFC 4283)
        /// </summary>
        MN_ID = 8,

        /// <summary>
        /// Authentication (RFC 4285)
        /// </summary>
        Auth = 9,

        /// <summary>
        /// Replay Protection (RFC 4285)
        /// </summary>
        Mesg_ID = 10,

        /// <summary>
        /// Cryptographically Generated Address Parameters Request (RFC 486)
        /// </summary>
        CGA_Parameters_Request = 11,

        /// <summary>
        /// Cryptographically Generated Address Parameters Reply (RFC 486)
        /// </summary>
        CGA_Parameters_Reply = 12,

        /// <summary>
        /// Signature (RFC 4866)
        /// </summary>
        Sig = 13,

        /// <summary>
        /// Permanent Home Keygen Token (RFC 4866)
        /// </summary>
        PHKT = 14,

        /// <summary>
        /// Care-of Test Initiate (RFC 4866)
        /// </summary>
        CoTI = 15,

        /// <summary>
        /// Care-of Test (RFC 4866)
        /// </summary>
        CoT = 16,

        /// <summary>
        /// DNS Update (RFC 5026)
        /// </summary>
        DNS_Update = 17,

        /// <summary>
        /// Experimental (RFC 5096)
        /// </summary>
        Experimental = 18,

        /// <summary>
        /// Vendor Specific (RFC 5094)
        /// </summary>
        VS = 19,

        /// <summary>
        /// Service Selection (RFC 5149)
        /// </summary>
        SS = 20,

        /// <summary>
        /// Fast Binding Authorization Data (RFC 5568)
        /// </summary>
        BADF = 21,

        /// <summary>
        /// Home Network Prefix (RFC 5213)
        /// </summary>
        HNP = 22,

        /// <summary>
        /// Hand-off Indicator (RFC 5213)
        /// </summary>
        HI = 23,

        /// <summary>
        /// Access Technology Type (RFC 5213)
        /// </summary>
        ATT = 24,

        /// <summary>
        /// Mobile Node Link-Layer Identifier (RFC 5213)
        /// </summary>
        MN_LLI = 25,

        /// <summary>
        /// Link-Local Address (RFC 5213)
        /// </summary>
        LLA = 26,

        /// <summary>
        /// Time stamp (RFC 5213)
        /// </summary>
        TS = 27,

        /// <summary>
        /// Restart Counter (RFC 5847)
        /// </summary>
        RC = 28,

        /// <summary>
        /// IP4 Home Address (RFC 5555)
        /// </summary>
        IP4_HoA = 29,

        /// <summary>
        /// IP4 Address Acknowledgment (RFC 5555)
        /// </summary>
        IP4_AA = 30,

        /// <summary>
        /// NAT Detection (RFC 5555)
        /// </summary>
        NAT_Detection = 31,

        /// <summary>
        /// IP4 Care-of Address (RFC 5555)
        /// </summary>
        IP4_CoA = 32,

        /// <summary>
        /// Generic Routing Encapsulation Key (RFC 5845)
        /// </summary>
        GRE_Key = 33,

        /// <summary>
        /// Address or Prefix (RFC 5568)
        /// </summary>
        MH_AP = 34,

        /// <summary>
        /// Binding Identifier (RFC 5648)
        /// </summary>
        BID = 35,

        /// <summary>
        /// IP4 Home Address Request (RFC 5844)
        /// </summary>
        IP4_HoA_Request = 36,

        /// <summary>
        /// IP4 Home Address Reply (RFC 5844)
        /// </summary>
        IP4_HoA_Reply = 37,

        /// <summary>
        /// IP4 Default-Router Address (RFC 5844)
        /// </summary>
        IP4_DRA = 38,

        /// <summary>
        /// IP4 DHCP Support Mode (RFC 5844)
        /// </summary>
        IP4_DHCP_SM = 39,

        /// <summary>
        /// Context Request (RFC 5949)
        /// </summary>
        CR = 40,

        /// <summary>
        /// Local Mobility Anchor Address (RFC 5949)
        /// </summary>
        LMAA = 41,

        /// <summary>
        /// Mobile Node Link-Local Address Interface Identifier (RFC 5949)
        /// </summary>
        MN_LLA_IID = 42,

        /// <summary>
        /// Transient Binding (RFC 6058)
        /// </summary>
        TB = 43,

        /// <summary>
        /// Flow Summary (RFC 6089)
        /// </summary>
        FS = 44,

        /// <summary>
        /// Flow Identification (RFC 6089)
        /// </summary>
        FID = 45,

        /// <summary>
        /// Redirect-Capability (RFC 6463)
        /// </summary>
        RdC = 46,

        /// <summary>
        /// Redirect (RFC 6463)
        /// </summary>
        Rd = 47,

        /// <summary>
        /// Load Information (RFC 6463)
        /// </summary>
        LI = 49,

        /// <summary>
        /// IP4 Alternate Care-of Address (RFC 6463)
        /// </summary>
        IP4_ACoA = 48,

        /// <summary>
        /// Mobile Node Group Identifier (RFC 6602)
        /// </summary>
        MN_GID = 49,

        /// <summary>
        /// Mobile Access Gateway Address (RFC 6705)
        /// </summary>
        MAGA = 51,

        /// <summary>
        /// Access Network Identifier (RFC 6757)
        /// </summary>
        ANI = 52,

        /// <summary>
        /// IP4 Traffic Offload Selector (RFC 6909)
        /// </summary>
        IP4_TOS = 53,

        /// <summary>
        /// Dynamic IP Multicast Selector (RFC 7028)
        /// </summary>
        DIPMS = 54
    }

    /// <summary>
    /// Mobility IP6 option (RFC 6275).
    /// </summary>
    public abstract class MobilityIP6Option
    {
        /// <summary>
        /// Gets the mobility type.
        /// </summary>
        public MobilityIP6OptionType MobilityType { get; private set; }

        /// <summary>
        /// Gets the data length in bytes.
        /// </summary>
        public byte DataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The mobility type.</param>
        protected MobilityIP6Option(BinaryReader reader, MobilityIP6OptionType type)
        {
            Contract.Requires(reader != null);

            MobilityType = type;
            if(MobilityType == MobilityIP6OptionType.Pad1)
                return;
            DataLength = reader.ReadByte();
        }

        /// <summary>
        /// Creates a Mobility IP6 option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A Mobility IP6 option.
        /// </returns>
        public static MobilityIP6Option Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (MobilityIP6OptionType)reader.ReadByte();
            switch(type)
            {
                case MobilityIP6OptionType.BRA:
                    return new BindingRefreshAdviceMobilityIP6Option(reader);
                case MobilityIP6OptionType.ACoA:
                case MobilityIP6OptionType.LLA:
                    return new AddressMobilityIP6Option(reader, type);
                case MobilityIP6OptionType.NI:
                    return new NonceIndexesMobilityIP6Option(reader);
                case MobilityIP6OptionType.BAD:
                    return new BindingAuthorizationDataMobilityIP6Option(reader);
                case MobilityIP6OptionType.MNP:
                case MobilityIP6OptionType.HNP:
                    return new NetworkPrefixMobilityIP6Option(reader, type);
                case MobilityIP6OptionType.MH_LLA:
                    return new LinkLayerAddressMobilityIP6Option(reader);
                case MobilityIP6OptionType.BADF:
                    return new FastBindingAuthorizationDataMobilityIP6Option(reader);
                case MobilityIP6OptionType.HI:
                    return new HandoffIndicatorMobilityIP6Option(reader);
                case MobilityIP6OptionType.ATT:
                    return new AccessTechnologyTypeMobilityIP6Option(reader);
                case MobilityIP6OptionType.MN_LLI:
                    return new MobileNodeLinkLayerIdentifierMobilityIP6Option(reader);
                case MobilityIP6OptionType.TS:
                    return new TimeStampMobilityIP6Option(reader);
                case MobilityIP6OptionType.MH_AP:
                    return new AddressOrPrefixMobilityIP6Option(reader);
                default:
                    return new GenericMobilityIP6Option(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic Mobility IP6 option (RFC 6275).
    /// </summary>
    public sealed class GenericMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The mobility type.</param>
        public GenericMobilityIP6Option(BinaryReader reader, MobilityIP6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Binding Refresh Advice Mobility IP6 option (RFC 6275).
    /// </summary>
    public sealed class BindingRefreshAdviceMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the number of 4-second units.
        /// </summary>
        public ushort RefreshInterval { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingRefreshAdviceMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public BindingRefreshAdviceMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.BRA)
        {
            Contract.Requires(reader != null);

            RefreshInterval = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Address Mobility IP6 option (RFC 5213, RFC 6275).
    /// </summary>
    public sealed class AddressMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the IP6 address.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of address mobility types.</param>
        public AddressMobilityIP6Option(BinaryReader reader, MobilityIP6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == MobilityIP6OptionType.ACoA || type == MobilityIP6OptionType.LLA);

            Address = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Nonce Indexes Mobility IP6 option (RFC 6275).
    /// </summary>
    public sealed class NonceIndexesMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the the home nonce index telling the correspondent node which nonce value to use when producing the home keygen token.
        /// </summary>
        public ushort HomeNonceIndex { get; private set; }

        /// <summary>
        /// Gets the care-of nonce index telling the correspondent node which nonce value to use when producing the care-of keygen token.
        /// </summary>
        public ushort CareOfNonceIndex { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonceIndexesMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NonceIndexesMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.NI)
        {
            Contract.Requires(reader != null);

            HomeNonceIndex = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            CareOfNonceIndex = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Binding Authorization Data Mobility IP6 option (RFC 6275).
    /// </summary>
    public sealed class BindingAuthorizationDataMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the authenticator, a cryptographic value that can be used to determine if the message in question comes from the right authority.
        /// </summary>
        public ReadOnlyCollection<byte> Authenticator { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingAuthorizationDataMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public BindingAuthorizationDataMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.BAD)
        {
            Contract.Requires(reader != null);

            Authenticator = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Network Prefix Mobility IP6 option (RFC 3963,  RFC 5213).
    /// </summary>
    public sealed class NetworkPrefixMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the <see cref="NetworkPrefix"/> length in bits.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the network IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> NetworkPrefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkPrefixMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of network prefix mobility types.</param>
        public NetworkPrefixMobilityIP6Option(BinaryReader reader, MobilityIP6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == MobilityIP6OptionType.MNP || type == MobilityIP6OptionType.HNP);

            Reserved = reader.ReadByte();
            PrefixLength = reader.ReadByte();
            NetworkPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 2));
        }
    }

    /// <summary>
    /// Constants that represent link-layer address codes.
    /// </summary>
    public enum LinkLayerAddressCode
    {
        /// <summary>
        /// Mobile Node
        /// </summary>
        MN = 2
    }

    /// <summary>
    /// Link-Layer Address Mobility IP6 option (RFC 5213).
    /// </summary>
    public sealed class LinkLayerAddressMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public LinkLayerAddressCode Code { get; private set; }

        /// <summary>
        /// Gets LLA.
        /// </summary>
        public ReadOnlyCollection<byte> LinkLayerAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkLayerAddressMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public LinkLayerAddressMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.MH_LLA)
        {
            Contract.Requires(reader != null);

            Code = (LinkLayerAddressCode)reader.ReadByte();
            LinkLayerAddress = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 1));
        }
    }

    /// <summary>
    /// Fast Binding Authorization Data Mobility IP6 option (RFC 5568).
    /// </summary>
    public sealed class FastBindingAuthorizationDataMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets SPI.
        /// </summary>
        public uint SecurityParameterIndex { get; private set; }

        /// <summary>
        /// Gets the authenticator, a cryptographic value that can be used to determine if the message in question comes from the right authority.
        /// </summary>
        public ReadOnlyCollection<byte> Authenticator { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FastBindingAuthorizationDataMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FastBindingAuthorizationDataMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.BADF)
        {
            Contract.Requires(reader != null);

            SecurityParameterIndex = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Authenticator = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4));
        }
    }

    /// <summary>
    /// Constants that represent hand-off types.
    /// </summary>
    public enum HandoffType
    {
        /// <summary>
        /// Attachment over new interface.
        /// </summary>
        AttachmentOverNewInterface = 1,

        /// <summary>
        /// Hand-off between two different interfaces of mobile node.
        /// </summary>
        HandoffBetweenTwoDifferentInterfacesOfMobileNode = 2,

        /// <summary>
        /// Hand-off between mobile access gateways for same interface.
        /// </summary>
        HandoffBetweenMobileAccessGatewaysForSameInterface = 3,

        /// <summary>
        /// Hand-off state unknown.
        /// </summary>
        HandoffStateUnknown = 4,

        /// <summary>
        /// Hand-off state not changed.
        /// </summary>
        HandoffStateNotChanged = 5
    }

    /// <summary>
    /// Hand-off Indicator Mobility IP6 option (RFC 5213).
    /// </summary>
    public sealed class HandoffIndicatorMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets HI.
        /// </summary>
        public HandoffType HandoffIndicator { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoffIndicatorMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoffIndicatorMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.HI)
        {
            Contract.Requires(reader != null);

            Reserved = reader.ReadByte();
            HandoffIndicator = (HandoffType)reader.ReadByte();
        }
    }

    /// <summary>
    /// Constants that represent access technology types.
    /// </summary>
    public enum AccessTechnologyType
    {
        /// <summary>
        /// Virtual
        /// </summary>
        LogicalNetworkInterface = 1,

        /// <summary>
        /// PPP
        /// </summary>
        PointToPointProtocol = 2,

        /// <summary>
        /// IEEE 802.3
        /// </summary>
        Ethernet = 3,

        /// <summary>
        /// IEEE 802.11a/b/g
        /// </summary>
        WirelessLAN = 4,

        /// <summary>
        /// IEEE 802.16e 
        /// </summary>
        WIMAX = 5
    }

    /// <summary>
    /// Access Technology Type Mobility IP6 option (RFC 5213).
    /// </summary>
    public sealed class AccessTechnologyTypeMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets ATT, the access technology type through which the mobile node is connected to the access link on the mobile access gateway.
        /// </summary>
        public AccessTechnologyType AccessTechnologyType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessTechnologyTypeMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AccessTechnologyTypeMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.ATT)
        {
            Contract.Requires(reader != null);

            Reserved = reader.ReadByte();
            AccessTechnologyType = (AccessTechnologyType)reader.ReadByte();
        }
    }

    /// <summary>
    /// Mobile Node Link-Layer Identifier Mobility IP6 option (RFC 5213).
    /// </summary>
    public sealed class MobileNodeLinkLayerIdentifierMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets LLI.
        /// </summary>
        public ReadOnlyCollection<byte> LinkLayerIdentifier { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobileNodeLinkLayerIdentifierMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MobileNodeLinkLayerIdentifierMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.MN_LLI)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            LinkLayerIdentifier = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 2));
        }
    }

    /// <summary>
    /// Time Stamp Mobility IP6 option (RFC 5213).
    /// </summary>
    public sealed class TimeStampMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        public ulong TimeStamp { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeStampMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeStampMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.TS)
        {
            Contract.Requires(reader != null);

            TimeStamp = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// Constants that represent IP6 address or prefix codes.
    /// </summary>
    public enum AddressOrPrefixCode
    {
        /// <summary>
        /// Old Care-of Address
        /// </summary>
        OldCareOfAddress = 1,

        /// <summary>
        /// New Care-of Address
        /// </summary>
        NewCareOfAddress = 2,

        /// <summary>
        /// New Access Router Address
        /// </summary>
        NewAccessRouterAddress = 3,

        /// <summary>
        /// New Access Router Prefix
        /// </summary>
        NewAccessRouterPrefix = 4
    }

    /// <summary>
    /// Address Or Prefix Mobility IP6 option (RFC 5568).
    /// </summary>
    public sealed class AddressOrPrefixMobilityIP6Option : MobilityIP6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public AddressOrPrefixCode Code { get; private set; }

        /// <summary>
        /// Gets the <see cref="AddressOrPrefix"/> length in bits.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the IP6 address or prefix.
        /// </summary>
        public ReadOnlyCollection<byte> AddressOrPrefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressOrPrefixMobilityIP6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddressOrPrefixMobilityIP6Option(BinaryReader reader) : base(reader, MobilityIP6OptionType.MH_AP)
        {
            Contract.Requires(reader != null);

            Code = (AddressOrPrefixCode)reader.ReadByte();
            PrefixLength = reader.ReadByte();
            AddressOrPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 2));
        }
    }
}