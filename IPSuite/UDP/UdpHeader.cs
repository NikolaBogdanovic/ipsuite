﻿namespace IPSuite.UDP
{
    using System.Diagnostics.Contracts;
    using System.IO;

    /// <summary>
    /// User Datagram Protocol header (RFC 768).
    /// </summary>
    public sealed class UdpHeader
    {
        /// <summary>
        /// Gets the port number of the sender.
        /// </summary>
        public ushort SourcePort { get; private set; }

        /// <summary>
        /// Gets the port this packet is addressed to.
        /// </summary>
        public ushort DestinationPort { get; private set; }

        /// <summary>
        /// Gets the UDP header and encapsulated data length in bytes.
        /// </summary>
        public ushort Length { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the jumbo payload length in bytes.
        /// </summary>
        public uint JumboPayloadLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UdpHeader"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="length">The length in bytes.</param>
        public UdpHeader(BinaryReader reader, uint length)
        {
            Contract.Requires(reader != null);
            Contract.Requires(length >= 8);

            SourcePort = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DestinationPort = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Length = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            JumboPayloadLength = length - 8;
        }
    }
}