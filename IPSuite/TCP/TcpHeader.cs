﻿namespace IPSuite.TCP
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;

    /// <summary>
    /// Transmission Control Protocol header (RFC 793).
    /// </summary>
    public sealed class TcpHeader
    {
        /// <summary>
        /// Gets the port number of the sender.
        /// </summary>
        public ushort SourcePort { get; private set; }

        /// <summary>
        /// Gets the port this packet is addressed to.
        /// </summary>
        public ushort DestinationPort { get; private set; }

        /// <summary>
        /// Gets the sequence number of the first data byte in this segment. If the <see cref="Synchronise"/> flag is set, the sequence number is the initial sequence number and the first data byte is initial sequence number + 1.
        /// </summary>
        public uint SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the acknowledgment number. If the <see cref="Acknowledgment"/> flag is set, this field contains the value of the next <see cref="SequenceNumber"/> the sender of the segment is expecting to receive.
        /// </summary>
        public uint AcknowledgmentNumber { get; private set; }

        /// <summary>
        /// Gets the number of 4-byte units in the TCP header. This indicates where the data begins.
        /// </summary>
        public byte DataOffset { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether NS flag is set.
        /// </summary>
        public bool NonceSum { get; private set; }

        /// <summary>
        /// Gets a value indicating whether CWR flag is set.
        /// </summary>
        public bool CongestionWindowReduced { get; private set; }

        /// <summary>
        /// Gets a value indicating whether ECE flag is set.
        /// </summary>
        public bool ExplicitCongestionNotificationEcho { get; private set; }

        /// <summary>
        /// Gets a value indicating whether URG flag is set.
        /// </summary>
        public bool Urgent { get; private set; }

        /// <summary>
        /// Gets a value indicating whether ACK flag is set.
        /// </summary>
        public bool Acknowledgment { get; private set; }

        /// <summary>
        /// Gets a value indicating whether PSH flag is set.
        /// </summary>
        public bool Push { get; private set; }

        /// <summary>
        /// Gets a value indicating whether RST flag is set.
        /// </summary>
        public bool Reset { get; private set; }

        /// <summary>
        /// Gets a value indicating whether SYN flag is set.
        /// </summary>
        public bool Synchronise { get; private set; }

        /// <summary>
        /// Gets a value indicating whether FIN flag is set.
        /// </summary>
        public bool Finish { get; private set; }

        /// <summary>
        /// Gets the number of data bytes, beginning with the one indicated in the <see cref="AcknowledgmentNumber"/> field, which the sender of this segment is willing to accept.
        /// </summary>
        public ushort WindowSize { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the urgent pointer. If the <see cref="Urgent"/> flag is set, this field points to the <see cref="SequenceNumber"/> of the last byte in a segment of urgent data.
        /// </summary>
        public ushort UrgentPointer { get; private set; }

        /// <summary>
        /// Gets the list of TCP options.
        /// </summary>
        public ReadOnlyCollection<TcpOption> Options { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Gets the jumbo payload length in bytes.
        /// </summary>
        public uint JumboPayloadLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpHeader"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="length">The length in bytes.</param>
        public TcpHeader(BinaryReader reader, uint length)
        {
            Contract.Requires(reader != null);
            Contract.Requires(length >= 20);

            SourcePort = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DestinationPort = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SequenceNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            AcknowledgmentNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            byte bits = reader.ReadByte();
            DataOffset = (byte)(bits >> 4);
            Reserved = (byte)((byte)(bits << 4) >> 5);
            NonceSum = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            bits = reader.ReadByte();
            CongestionWindowReduced = Convert.ToBoolean((byte)(bits >> 7));
            ExplicitCongestionNotificationEcho = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Urgent = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Acknowledgment = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            Push = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            Reset = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            Synchronise = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
            Finish = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            WindowSize = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            UrgentPointer = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var opts = new List<TcpOption>();
            int i = 0, j = (DataOffset * 4) - 20;
            while(i < j)
            {
                var o = TcpOption.Create(reader);
                opts.Add(o);
                if(o.OptionKind == TcpOptionKind.EOOL)
                {
                    i += 1;
                    break;
                }
                if(o.OptionKind == TcpOptionKind.NOOP)
                    i += 1;
                else
                    i += o.Length;
            }
            Options = new ReadOnlyCollection<TcpOption>(opts.ToArray());
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(j - i));
            JumboPayloadLength = (uint)(length - (DataOffset * 4));
        }
    }
}