﻿namespace IPSuite.TCP
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Constants that represent MultiPath TCP option subtypes.
    /// </summary>
    public enum MultiPathTcpOptionSubType
    {
        /// <summary>
        /// MultiPath Capable (RFC 6824)
        /// </summary>
        MP_CAPABLE = 0,

        /// <summary>
        /// Join Connection (RFC 6824)
        /// </summary>
        MP_JOIN = 1,

        /// <summary>
        /// Data Sequence Signal (RFC 6824)
        /// </summary>
        DSS = 2,

        /// <summary>
        /// Add Address (RFC 6824)
        /// </summary>
        ADD_ADDR = 3,

        /// <summary>
        /// Remove Address (RFC 6824)
        /// </summary>
        REMOVE_ADDR = 4,

        /// <summary>
        /// Change Subflow Priority (RFC 6824)
        /// </summary>
        MP_PRIO = 5,

        /// <summary>
        /// Fail-over (RFC 6824)
        /// </summary>
        MP_FAIL = 6,

        /// <summary>
        /// Fast Close (RFC 6824)
        /// </summary>
        MP_FASTCLOSE = 7
    }

    /// <summary>
    /// MultiPath TCP option (RFC 6824).
    /// </summary>
    public abstract class MultiPathTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the multipath subtype.
        /// </summary>
        public MultiPathTcpOptionSubType SubType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="subtype">The multipath subtype.</param>
        protected MultiPathTcpOption(BinaryReader reader, MultiPathTcpOptionSubType subtype) : base(reader, TcpOptionKind.MPTCP)
        {
            Contract.Requires(reader != null);

            SubType = subtype;
        }

        /// <summary>
        /// Creates a MultiPath TCP option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A MultiPath TCP option.
        /// </returns>
        public static new MultiPathTcpOption Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var length = reader.ReadByte();
            var subtype = (MultiPathTcpOptionSubType)(byte)(reader.ReadByte() >> 4);
            reader.BaseStream.Position -= 2;
            switch(subtype)
            {
                case MultiPathTcpOptionSubType.MP_CAPABLE:
                    return new CapableMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.MP_JOIN:
                {
                    switch(length)
                    {
                        case 12:
                            return new JoinConnectionInitialHandshakeMultiPathTcpOption(reader);
                        case 16:
                            return new JoinConnectionRespondingHandshakeMultiPathTcpOption(reader);
                        case 24:
                            return new JoinConnectionFinalHandshakeMultiPathTcpOption(reader);
                    }
                    goto default;
                }
                case MultiPathTcpOptionSubType.DSS:
                    return new DataSequenceSignalMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.ADD_ADDR:
                    return new AddAddressMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.REMOVE_ADDR:
                    return new RemoveAddressMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.MP_PRIO:
                    return new ChangeSubflowPriorityMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.MP_FAIL:
                    return new FallbackMultiPathTcpOption(reader);
                case MultiPathTcpOptionSubType.MP_FASTCLOSE:
                    return new FastCloseMultiPathTcpOption(reader);
                default:
                    return new GenericMultiPathTcpOption(reader, subtype);
            }
        }
    }

    /// <summary>
    /// Generic MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class GenericMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the MultiPath data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="subtype">The multipath subtype.</param>
        public GenericMultiPathTcpOption(BinaryReader reader, MultiPathTcpOptionSubType subtype) : base(reader, subtype)
        {
            Contract.Requires(reader != null);

            var data = new byte[Length - 2];
            data[0] = (byte)((byte)(reader.ReadByte() << 4) >> 4);
            for(int i = 1, j = data.Length; i < j; i++)
                data[i] = reader.ReadByte();
            Data = new ReadOnlyCollection<byte>(data);
        }
    }

    /// <summary>
    /// Capable MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class CapableMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the MultiPath version.
        /// </summary>
        public byte Version { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool ChecksumRequired { get; private set; }

        /// <summary>
        /// Gets a value indicating whether B flag is set.
        /// </summary>
        public bool Extensibility { get; private set; }

        /// <summary>
        /// Gets a value indicating whether C flag is set.
        /// </summary>
        public bool C { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool D { get; private set; }

        /// <summary>
        /// Gets a value indicating whether E flag is set.
        /// </summary>
        public bool E { get; private set; }

        /// <summary>
        /// Gets a value indicating whether F flag is set.
        /// </summary>
        public bool F { get; private set; }

        /// <summary>
        /// Gets a value indicating whether G flag is set.
        /// </summary>
        public bool G { get; private set; }

        /// <summary>
        /// Gets a value indicating whether H flag is set.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public bool HMAC_SHA1 { get; private set; }

        /// <summary>
        /// Gets the sender's key.
        /// </summary>
        public ReadOnlyCollection<byte> SendersKey { get; private set; }

        /// <summary>
        /// Gets the receiver's key.
        /// </summary>
        public ReadOnlyCollection<byte> ReceiversKey { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CapableMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CapableMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_CAPABLE)
        {
            Contract.Requires(reader != null);

            Version = (byte)((byte)(reader.ReadByte() << 4) >> 4);
            var bits = reader.ReadByte();
            ChecksumRequired = Convert.ToBoolean((byte)(bits >> 7));
            Extensibility = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            C = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            D = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            E = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            F = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            G = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
            HMAC_SHA1 = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            SendersKey = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
            if(Length == 20)
                ReceiversKey = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// Join Connection Initial Handshake MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class JoinConnectionInitialHandshakeMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether B flag is set.
        /// </summary>
        public bool BackupPath { get; private set; }

        /// <summary>
        /// Gets the address identifier.
        /// </summary>
        public byte AddressId { get; private set; }

        /// <summary>
        /// Gets the receiver's token.
        /// </summary>
        public ReadOnlyCollection<byte> ReceiversToken { get; private set; }

        /// <summary>
        /// Gets the sender's random number.
        /// </summary>
        public uint SendersRandomNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinConnectionInitialHandshakeMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public JoinConnectionInitialHandshakeMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_JOIN)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Reserved = (byte)((byte)(bits << 4) >> 5);
            BackupPath = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            AddressId = reader.ReadByte();
            ReceiversToken = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            SendersRandomNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Join Connection Responding Handshake MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class JoinConnectionRespondingHandshakeMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether B flag is set.
        /// </summary>
        public bool BackupPath { get; private set; }

        /// <summary>
        /// Gets the address identifier.
        /// </summary>
        public byte AddressId { get; private set; }

        /// <summary>
        /// Gets the sender's truncated HMAC.
        /// </summary>
        public ReadOnlyCollection<byte> SendersTruncatedHmac { get; private set; }

        /// <summary>
        /// Gets the sender's random number.
        /// </summary>
        public uint SendersRandomNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinConnectionRespondingHandshakeMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public JoinConnectionRespondingHandshakeMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_JOIN)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Reserved = (byte)((byte)(bits << 4) >> 5);
            BackupPath = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            AddressId = reader.ReadByte();
            SendersTruncatedHmac = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
            SendersRandomNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// JoinConnection Final Handshake MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class JoinConnectionFinalHandshakeMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 12 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the sender's HMAC.
        /// </summary>
        public ReadOnlyCollection<byte> SendersHmac { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinConnectionFinalHandshakeMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public JoinConnectionFinalHandshakeMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_JOIN)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(reader.ReadByte() << 4) >> 4), reader.ReadByte() });
            SendersHmac = new ReadOnlyCollection<byte>(reader.ReadBytes(20));
        }
    }

    /// <summary>
    /// Data Sequence Signal MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class DataSequenceSignalMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 7 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether F flag is set.
        /// </summary>
        public bool DataFinal { get; private set; }

        /// <summary>
        /// Gets a value indicating whether "m" flag is set.
        /// </summary>
        public bool DataSequenceNumberLength { get; private set; }

        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool DataSequenceNumberPresent { get; private set; }

        /// <summary>
        /// Gets a value indicating whether "a" flag is set.
        /// </summary>
        public bool DataAcknowledgmentLength { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool DataAcknowledgmentPresent { get; private set; }

        /// <summary>
        /// Gets the data acknowledgment.
        /// </summary>
        public ReadOnlyCollection<byte> DataAcknowledgment { get; private set; }

        /// <summary>
        /// Gets the data sequence number.
        /// </summary>
        public ReadOnlyCollection<byte> DataSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the subflow sequence number.
        /// </summary>
        public uint SubflowSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the length of the data level.
        /// </summary>
        public ushort DataLevelLength { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSequenceSignalMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DataSequenceSignalMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.DSS)
        {
            Contract.Requires(reader != null);

            Reserved = (byte)((byte)(reader.ReadByte() << 4) >> 1);
            var bits = reader.ReadByte();
            Reserved += (byte)(bits >> 5);
            DataFinal = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            DataSequenceNumberLength = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            DataSequenceNumberPresent = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            DataAcknowledgmentLength = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
            DataAcknowledgmentPresent = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            if(DataAcknowledgmentPresent)
                DataAcknowledgment = new ReadOnlyCollection<byte>(reader.ReadBytes(DataAcknowledgmentLength ? 8 : 4));
            if(DataSequenceNumberPresent)
            {
                DataSequenceNumber = new ReadOnlyCollection<byte>(reader.ReadBytes(DataSequenceNumberLength ? 8 : 4));
                SubflowSequenceNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                DataLevelLength = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            }
            if(Length > 4 + (DataAcknowledgmentPresent ? (DataAcknowledgmentLength ? 8 : 4) : 0) + (DataSequenceNumberPresent ? (DataSequenceNumberLength ? 8 : 4) + 6 : 0))
                Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Change Subflow Priority MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class ChangeSubflowPriorityMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether B flag is set.
        /// </summary>
        public bool BackupPath { get; private set; }

        /// <summary>
        /// Gets the address identifier.
        /// </summary>
        public byte AddressId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeSubflowPriorityMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ChangeSubflowPriorityMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_PRIO)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Reserved = (byte)((byte)(bits << 4) >> 5);
            BackupPath = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            if(Length == 4)
                AddressId = reader.ReadByte();
        }
    }

    /// <summary>
    /// Add Address MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class AddAddressMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the IP version (4 or 6).
        /// </summary>
        public byte IPVersion { get; private set; }

        /// <summary>
        /// Gets the address identifier.
        /// </summary>
        public byte AddressId { get; private set; }

        /// <summary>
        /// Gets the IP4 or IP6 address.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Gets the TCP port.
        /// </summary>
        public ushort Port { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddAddressMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddAddressMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.ADD_ADDR)
        {
            Contract.Requires(reader != null);

            IPVersion = (byte)((byte)(reader.ReadByte() << 4) >> 4);
            if(IPVersion != 4 && IPVersion != 6)
                throw new ArgumentOutOfRangeException("reader", IPVersion, @"Argument Out Of Range Exception");
            AddressId = reader.ReadByte();
            Address = new IPAddress(reader.ReadBytes(IPVersion == 4 ? 4 : 16));
            if(Length > 4 + (IPVersion == 4 ? 4 : 16))
                Port = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Remove Address MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class RemoveAddressMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserve 4 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the list of address identifiers.
        /// </summary>
        public ReadOnlyCollection<byte> AddressIds { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoveAddressMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RemoveAddressMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.REMOVE_ADDR)
        {
            Contract.Requires(reader != null);

            Reserved = (byte)((byte)(reader.ReadByte() << 4) >> 4);
            AddressIds = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - 3));
        }
    }

    /// <summary>
    /// Fast Close MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class FastCloseMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 12 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the receiver's key.
        /// </summary>
        public ReadOnlyCollection<byte> ReceiversKey { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FastCloseMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FastCloseMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_FASTCLOSE)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(reader.ReadByte() << 4) >> 4), reader.ReadByte() });
            ReceiversKey = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// Fall-back MultiPath TCP option (RFC 6824).
    /// </summary>
    public sealed class FallbackMultiPathTcpOption : MultiPathTcpOption
    {
        /// <summary>
        /// Gets the reserved 12 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the data sequence number.
        /// </summary>
        public ulong DataSequenceNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackMultiPathTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FallbackMultiPathTcpOption(BinaryReader reader) : base(reader, MultiPathTcpOptionSubType.MP_FAIL)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(reader.ReadByte() << 4) >> 4), reader.ReadByte() });
            DataSequenceNumber = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
        }
    }
}