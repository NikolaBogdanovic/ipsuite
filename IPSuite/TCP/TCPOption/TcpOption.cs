﻿namespace IPSuite.TCP
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;

    /// <summary>
    /// Constants that represent TCP option kinds.
    /// </summary>
    public enum TcpOptionKind
    {
        /// <summary>
        /// End Of Option List (RFC 793)
        /// </summary>
        EOOL = 0,

        /// <summary>
        /// No Operation (RFC 793)
        /// </summary>
        NOOP = 1,

        /// <summary>
        /// Maximum Segment Size (RFC 793)
        /// </summary>
        MSS = 2,

        /// <summary>
        /// Window Scale (RFC 1323)
        /// </summary>
        WS = 3,

        /// <summary>
        /// Selective Acknowledgment Permitted (RFC 2018)
        /// </summary>
        SACK_Permitted = 4,

        /// <summary>
        /// Selective Acknowledgment (RFC 2018)
        /// </summary>
        SACK = 5,

        /// <summary>
        /// Echo Request (RFC 1072, RFC 6247)
        /// </summary>
        EchoRequest = 6,

        /// <summary>
        /// Echo Reply (RFC 1072, RFC 6247)
        /// </summary>
        EchoReply = 7,

        /// <summary>
        /// Time Stamp (RFC 1323)
        /// </summary>
        TS = 8,

        /// <summary>
        /// Partial Order Connection Permitted (RFC 1693, RFC 6247)
        /// </summary>
        POC_Permitted = 9,

        /// <summary>
        /// Partial Order Connection Service Profile (RFC 1693, RFC 6247)
        /// </summary>
        POC_Service_Profile = 10,

        /// <summary>
        /// Connection Count (RFC 1644, RFC 6247)
        /// </summary>
        CC = 11,

        /// <summary>
        /// Connection Count New (RFC 1644, RFC 6247)
        /// </summary>
        CC_New = 12,

        /// <summary>
        /// Connection Count Echo (RFC 1644, RFC 6247)
        /// </summary>
        CC_Echo = 13,

        /// <summary>
        /// Alternate Checksum Request (RFC 1146, RFC 6247)
        /// </summary>
        ACR = 14,

        /// <summary>
        /// Alternate Checksum Data (RFC 1146, RFC 6247)
        /// </summary>
        ACD = 15,

        /// <summary>
        /// MD5 Signature (RFC 2385)
        /// </summary>
        MD5 = 19,

        /// <summary>
        /// Packet Mood (RFC 5841)
        /// </summary>
        PM = 25,

        /// <summary>
        /// Quick-Start Response (RFC 4782)
        /// </summary>
        QSR = 27,

        /// <summary>
        /// User Timeout (RFC 5482)
        /// </summary>
        UT = 28,

        /// <summary>
        /// Authentication Option (RFC 5925)
        /// </summary>
        AO = 29,

        /// <summary>
        /// MultiPath TCP (RFC 6824)
        /// </summary>
        MPTCP = 30
    }

    /// <summary>
    /// Transmission Control Protocol option (RFC 793).
    /// </summary>
    public abstract class TcpOption
    {
        /// <summary>
        /// Gets the option kind.
        /// </summary>
        public TcpOptionKind OptionKind { get; private set; }

        /// <summary>
        /// Gets the length in bytes.
        /// </summary>
        public byte Length { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="kind">The option kind.</param>
        protected TcpOption(BinaryReader reader, TcpOptionKind kind)
        {
            Contract.Requires(reader != null);

            OptionKind = kind;
            if(OptionKind != TcpOptionKind.EOOL && OptionKind != TcpOptionKind.NOOP)
                Length = reader.ReadByte();
        }

        /// <summary>
        /// Creates a TCP option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A TCP option.
        /// </returns>
        public static TcpOption Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var kind = (TcpOptionKind)reader.ReadByte();
            switch(kind)
            {
                case TcpOptionKind.MSS:
                    return new MaximumSegmentSizeTcpOption(reader);
                case TcpOptionKind.WS:
                    return new WindowScaleTcpOption(reader);
                case TcpOptionKind.SACK:
                    return new SelectiveAcknowledgmentTcpOption(reader);
                case TcpOptionKind.TS:
                    return new TimeStampTcpOption(reader);
                case TcpOptionKind.POC_Service_Profile:
                    return new PartialOrderConnectionServiceProfileTcpOption(reader);
                case TcpOptionKind.CC:
                case TcpOptionKind.CC_New:
                case TcpOptionKind.CC_Echo:
                    return new ConnectionCountTcpOption(reader, kind);
                case TcpOptionKind.ACR:
                    return new AlternateChecksumRequestTcpOption(reader);
                case TcpOptionKind.QSR:
                    return new QuickStartResponseTcpOption(reader);
                case TcpOptionKind.UT:
                    return new UserTimeoutTcpOption(reader);
                case TcpOptionKind.AO:
                    return new AuthenticationTcpOption(reader);
                case TcpOptionKind.MPTCP:
                    return MultiPathTcpOption.Create(reader);
                default:
                    return new GenericTcpOption(reader, kind);
            }
        }
    }

    /// <summary>
    /// Generic TCP option (RFC 793).
    /// </summary>
    public sealed class GenericTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="kind">The option kind.</param>
        public GenericTcpOption(BinaryReader reader, TcpOptionKind kind) : base(reader, kind)
        {
            Contract.Requires(reader != null);

            if(Length > 0)
                Data = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - 2));
        }
    }

    /// <summary>
    /// Maximum Segment Size TCP option (RFC 793).
    /// </summary>
    public sealed class MaximumSegmentSizeTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the maximum segment size in bytes.
        /// </summary>
        public ushort MaximumSegmentSize { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaximumSegmentSizeTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MaximumSegmentSizeTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.MSS)
        {
            Contract.Requires(reader != null);

            MaximumSegmentSize = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Window Scale TCP option (RFC 1323).
    /// </summary>
    public sealed class WindowScaleTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the shift count.
        /// </summary>
        public byte ShiftCount { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowScaleTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public WindowScaleTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.WS)
        {
            Contract.Requires(reader != null);

            ShiftCount = reader.ReadByte();
        }
    }

    /// <summary>
    /// Selective Acknowledgment TCP option (RFC 2018).
    /// </summary>
    public sealed class SelectiveAcknowledgmentTcpOption : TcpOption
    {
        /// <summary>
        /// Block class.
        /// </summary>
        public sealed class Block
        {
            /// <summary>
            /// Gets the first sequence number of this block.
            /// </summary>
            public uint LeftEdge { get; private set; }

            /// <summary>
            /// Gets the sequence number immediately following the last sequence number of this block.
            /// </summary>
            public uint RightEdge { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="Block"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            public Block(BinaryReader reader)
            {
                Contract.Requires(reader != null);

                LeftEdge = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                RightEdge = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            }
        }

        /// <summary>
        /// Gets the list of blocks.
        /// </summary>
        public ReadOnlyCollection<Block> Blocks { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectiveAcknowledgmentTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SelectiveAcknowledgmentTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.SACK)
        {
            Contract.Requires(reader != null);

            var blocks = new Block[(Length - 2) / 8];
            for(int i = 0, j = blocks.Length; i < j; i++)
                blocks[i] = new Block(reader);
            Blocks = new ReadOnlyCollection<Block>(blocks);
        }
    }

    /// <summary>
    /// Time Stamp TCP option (RFC 1323).
    /// </summary>
    public sealed class TimeStampTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the current value of the time stamp clock of the TCP sending the option.
        /// </summary>
        public uint Value { get; private set; }

        /// <summary>
        /// Gets the echo reply of a time stamp value that was sent by the remote TCP.
        /// </summary>
        public uint EchoReply { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeStampTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeStampTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.TS)
        {
            Contract.Requires(reader != null);

            Value = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            EchoReply = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Partial Order Connection Service Profile TCP option (RFC 1693, RFC 6247).
    /// </summary>
    public sealed class PartialOrderConnectionServiceProfileTcpOption : TcpOption
    {
        /// <summary>
        /// Gets a value indicating whether SF flag is set.
        /// </summary>
        public bool Start { get; private set; }

        /// <summary>
        /// Gets a value indicating whether EF flag is set.
        /// </summary>
        public bool End { get; private set; }

        /// <summary>
        /// Gets the filler 6 bits.
        /// </summary>
        public byte Filler { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PartialOrderConnectionServiceProfileTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public PartialOrderConnectionServiceProfileTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.POC_Service_Profile)
        {
            Contract.Requires(reader != null);

            byte bits = reader.ReadByte();
            Start = Convert.ToBoolean((byte)(bits >> 7));
            End = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Filler = (byte)((byte)(bits << 2) >> 2);
        }
    }

    /// <summary>
    /// Connection Count TCP option (RFC 1644, RFC 6247).
    /// </summary>
    public sealed class ConnectionCountTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the connection count.
        /// </summary>
        public uint ConnectionCount { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionCountTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="kind">One of connection count option kinds.</param>
        public ConnectionCountTcpOption(BinaryReader reader, TcpOptionKind kind) : base(reader, kind)
        {
            Contract.Requires(reader != null);
            Contract.Requires(kind == TcpOptionKind.CC || kind == TcpOptionKind.CC_New || kind == TcpOptionKind.CC_Echo);

            ConnectionCount = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Constants representing checksum types.
    /// </summary>
    public enum ChecksumType
    {
        /// <summary>
        /// Transmission Control Protocol
        /// </summary>
        TCP = 0,

        /// <summary>
        /// Fletcher 8-bit
        /// </summary>
        Fletcher8 = 1,

        /// <summary>
        /// Fletcher 16-bit
        /// </summary>
        Fletcher16 = 2,

        /// <summary>
        /// Redundant Checksum Avoidance
        /// </summary>
        RCA = 3
    }

    /// <summary>
    /// Alternate Checksum Request TCP option (RFC 1146, RFC 6247).
    /// </summary>
    public sealed class AlternateChecksumRequestTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the checksum type.
        /// </summary>
        public ChecksumType ChecksumType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AlternateChecksumRequestTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AlternateChecksumRequestTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.ACR)
        {
            Contract.Requires(reader != null);

            ChecksumType = (ChecksumType)reader.ReadByte();
        }
    }

    /// <summary>
    /// Quick-Start Response TCP option (RFC 4782).
    /// </summary>
    public sealed class QuickStartResponseTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the reserved 4 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the rate request.
        /// </summary>
        public byte RateRequest { get; private set; }

        /// <summary>
        /// Gets the time to live difference.
        /// </summary>
        public byte TimeToLive { get; private set; }

        /// <summary>
        /// Gets the nonce.
        /// </summary>
        public uint Nonce { get; private set; }

        /// <summary>
        /// Gets the reserved 2 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickStartResponseTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public QuickStartResponseTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.QSR)
        {
            Contract.Requires(reader != null);

            Reserved = (byte)(reader.ReadByte() >> 4);
            RateRequest = (byte)((byte)(reader.ReadByte() << 4) >> 4);
            TimeToLive = reader.ReadByte();
            var bytes = reader.ReadBytes(4);
            Reserved2 = (byte)((byte)(bytes[3] << 6) >> 6);
            bytes[3] = (byte)((byte)(bytes[3] >> 2) + (byte)(bytes[2] << 6));
            bytes[2] = (byte)((byte)(bytes[2] >> 2) + (byte)(bytes[1] << 6));
            bytes[1] = (byte)((byte)(bytes[1] >> 2) + (byte)(bytes[0] << 6));
            bytes[0] = (byte)(bytes[0] >> 2);
            Nonce = NetworkToHostOrder.ToUInt32(bytes);
        }
    }

    /// <summary>
    /// Constants that represent time intervals.
    /// </summary>
    public enum TimeInterval
    {
        /// <summary>
        /// Seconds
        /// </summary>
        Seconds = 0,

        /// <summary>
        /// Minutes
        /// </summary>
        Minutes = 1
    }

    /// <summary>
    /// User Timeout TCP option (RFC 5482).
    /// </summary>
    public sealed class UserTimeoutTcpOption : TcpOption
    {
        /// <summary>
        /// Gets the time interval granularity.
        /// </summary>
        public TimeInterval Granularity { get; private set; }

        /// <summary>
        /// Gets the user timeout suggestion for this connection.
        /// </summary>
        public ushort UserTimeout { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserTimeoutTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public UserTimeoutTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.UT)
        {
            Contract.Requires(reader != null);

            byte bits = reader.ReadByte();
            Granularity = (TimeInterval)(byte)(bits >> 7);
            UserTimeout = NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(bits << 1) >> 1), reader.ReadByte() });
        }
    }

    /// <summary>
    /// Constants that represent MAC algorithms.
    /// </summary>
    public enum MacAlgorithm
    {
        /// <summary>
        /// HMAC-SHA-1-96
        /// </summary>
        SHA1 = 0,

        /// <summary>
        /// AES-128-CMAC-96
        /// </summary>
        AES128 = 1
    }

    /// <summary>
    /// Authentication TCP option (RFC 5925).
    /// </summary>
    public sealed class AuthenticationTcpOption : TcpOption
    {
        /// <summary>
        /// Gets KeyID.
        /// </summary>
        public byte MasterKeyTuple { get; private set; }

        /// <summary>
        /// Gets RNextKeyID.
        /// </summary>
        public byte ReceiveNext { get; private set; }

        /// <summary>
        /// Gets MAC.
        /// </summary>
        public ReadOnlyCollection<byte> MessageAuthenticationCode { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationTcpOption"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AuthenticationTcpOption(BinaryReader reader) : base(reader, TcpOptionKind.AO)
        {
            Contract.Requires(reader != null);

            MasterKeyTuple = reader.ReadByte();
            ReceiveNext = reader.ReadByte();
            MessageAuthenticationCode = new ReadOnlyCollection<byte>(reader.ReadBytes(Length - 4));
        }
    }
}