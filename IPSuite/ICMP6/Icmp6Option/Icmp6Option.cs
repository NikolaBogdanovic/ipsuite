﻿namespace IPSuite.ICMP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.DNS;
    using IPSuite.IP6;

    /// <summary>
    /// Constants that represent ICMP6 option types.
    /// </summary>
    public enum Icmp6OptionType
    {
        /// <summary>
        /// Source Link-Layer Address (RFC 4861)
        /// </summary>
        SourceLinkLayerAddress = 1,

        /// <summary>
        /// Target Link-Layer Address (RFC 4861)
        /// </summary>
        TargetLinkLayerAddress = 2,

        /// <summary>
        /// Prefix Information (RFC 4861)
        /// </summary>
        PrefixInformation = 3,

        /// <summary>
        /// Redirected Header (RFC 4861)
        /// </summary>
        RedirectedHeader = 4,

        /// <summary>
        /// Maximum Transmission Unit (RFC 4861)
        /// </summary>
        MaximumTransmissionUnit = 5,

        /// <summary>
        /// Shortcut Limit (RFC 2491)
        /// </summary>
        ShortcutLimit = 6,

        /// <summary>
        /// Advertisement Interval (RFC 6275)
        /// </summary>
        AdvertisementInterval = 7,

        /// <summary>
        /// Home Agent Information (RFC 6275)
        /// </summary>
        HomeAgentInformation = 8,

        /// <summary>
        /// Source Address List (RFC 3122)
        /// </summary>
        SourceAddressList = 9,

        /// <summary>
        /// Target Address List (RFC 3122)
        /// </summary>
        TargetAddressList = 10,

        /// <summary>
        /// Cryptographically Generated Address (RFC 3971)
        /// </summary>
        CryptographicallyGeneratedAddress = 11,

        /// <summary>
        /// RSA Signature (RFC 3971)
        /// </summary>
        RSASignature = 12,

        /// <summary>
        /// Time Stamp (RFC 3971)
        /// </summary>
        TimeStamp = 13,

        /// <summary>
        /// Nonce (RFC 3971)
        /// </summary>
        Nonce = 14,

        /// <summary>
        /// Trust Anchor (RFC 3971)
        /// </summary>
        TrustAnchor = 15,

        /// <summary>
        /// Certificate (RFC 3971)
        /// </summary>
        Certificate = 16,

        /// <summary>
        /// Address Or Prefix (RFC 5568)
        /// </summary>
        AddressOrPrefix = 17,

        /// <summary>
        /// New Router Prefix Information (RFC 4068)
        /// </summary>
        NewRouterPrefixInformation = 18,

        /// <summary>
        /// Link-Layer Address (RFC 5568)
        /// </summary>
        LinkLayerAddress = 19,

        /// <summary>
        /// Neighbor Advertisement Acknowledgment (RFC 5568)
        /// </summary>
        NeighborAdvertisementAcknowledgment = 20,

        /// <summary>
        /// Mobility Anchor Point (RFC 4140)
        /// </summary>
        MobilityAnchorPoint = 23,

        /// <summary>
        /// Route Information (RFC 4191)
        /// </summary>
        RouteInformation = 24,

        /// <summary>
        /// Recursive DNS Server (RFC 5006, RFC 6106)
        /// </summary>
        RecursiveDNSServer = 25,
        
        /// <summary>
        /// Expanded Flags (RFC 5175)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flags")]
        ExpandedFlags = 26,

        /// <summary>
        /// Handover Key Request (RFC 5269)
        /// </summary>
        HandoverKeyRequest = 27,

        /// <summary>
        /// Handover Key Reply (RFC 5269)
        /// </summary>
        HandoverKeyReply = 28,

        /// <summary>
        /// Handover Assist Information (RFC 5271)
        /// </summary>
        HandoverAssistInformation = 29,

        /// <summary>
        /// Mobile Node Identifier (RFC 5271)
        /// </summary>
        MobileNodeIdentifier = 30,

        /// <summary>
        /// DNS Search List (RFC 6106)
        /// </summary>
        DNSSearchList = 31,

        /// <summary>
        /// Proxy Signature (RFC 6496)
        /// </summary>
        ProxySignature = 32,

        /// <summary>
        /// Address Registration (RFC 6775)
        /// </summary>
        AddressRegistration = 33,

        /// <summary>
        /// Low-rate Wireless Personal Area Network Context (RFC 6775)
        /// </summary>
        LoWPANContext = 34,

        /// <summary>
        /// Authoritative Border Router (RFC 6775)
        /// </summary>
        AuthoritativeBorderRouter = 35,

        /// <summary>
        /// Candidate Access Router Discovery Request (RFC 4065)
        /// </summary>
        CandidateAccessRouterDiscoveryRequest = 138,

        /// <summary>
        /// Candidate Access Router Discovery Reply (RFC 4065)
        /// </summary>
        CandidateAccessRouterDiscoveryReply = 139
    }

    /// <summary>
    /// Internet Control Message Protocol version 6 option (RFC 4861).
    /// </summary>
    public abstract class Icmp6Option
    {
        /// <summary>
        /// Gets the option type.
        /// </summary>
        public Icmp6OptionType OptionType { get; private set; }

        /// <summary>
        /// Gets the option length in 8-byte units.
        /// </summary>
        public byte Length { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Icmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The option type.</param>
        protected Icmp6Option(BinaryReader reader, Icmp6OptionType type)
        {
            Contract.Requires(reader != null);

            OptionType = type;
            Length = reader.ReadByte();
        }

        /// <summary>
        /// Creates an ICMP6 option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// An ICMP6 option.
        /// </returns>
        public static Icmp6Option Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (Icmp6OptionType)reader.ReadByte();
            switch(type)
            {
                case Icmp6OptionType.PrefixInformation:
                    return new PrefixInformationIcmp6Option(reader);
                case Icmp6OptionType.RedirectedHeader:
                    return new RedirectedHeaderIcmp6Option(reader);
                case Icmp6OptionType.MaximumTransmissionUnit:
                    return new MaximumTransmissionUnitIcmp6Option(reader);
                case Icmp6OptionType.ShortcutLimit:
                    return new ShortcutLimitIcmp6Option(reader);
                case Icmp6OptionType.AdvertisementInterval:
                    return new AdvertisementIntervalIcmp6Option(reader);
                case Icmp6OptionType.HomeAgentInformation:
                    return new HomeAgentInformationIcmp6Option(reader);
                case Icmp6OptionType.SourceAddressList:
                case Icmp6OptionType.TargetAddressList:
                    return new AddressListIcmp6Option(reader, type);
                case Icmp6OptionType.CryptographicallyGeneratedAddress:
                    return new CryptographicallyGeneratedAddressIcmp6Option(reader);
                case Icmp6OptionType.RSASignature:
                    return new RsaSignatureIcmp6Option(reader);
                case Icmp6OptionType.TimeStamp:
                    return new TimeStampIcmp6Option(reader);
                case Icmp6OptionType.TrustAnchor:
                    return new TrustAnchorIcmp6Option(reader);
                case Icmp6OptionType.Certificate:
                    return new CertificateIcmp6Option(reader);
                case Icmp6OptionType.AddressOrPrefix:
                    return new AddressOrPrefixIcmp6Option(reader);
                case Icmp6OptionType.NewRouterPrefixInformation:
                    return new NewRouterPrefixInformationIcmp6Option(reader);
                case Icmp6OptionType.LinkLayerAddress:
                    return new LinkLayerAddressIcmp6Option(reader);
                case Icmp6OptionType.NeighborAdvertisementAcknowledgment:
                    return new NeighborAdvertisementAcknowledgmentIcmp6Option(reader);
                case Icmp6OptionType.MobilityAnchorPoint:
                    return new MobilityAnchorPointIcmp6Option(reader);
                case Icmp6OptionType.RouteInformation:
                    return new RouteInformationIcmp6Option(reader);
                case Icmp6OptionType.RecursiveDNSServer:
                    return new RecursiveDnsServerIcmp6Option(reader);
                case Icmp6OptionType.ExpandedFlags:
                    return new ExpandedFlagsIcmp6Option(reader);
                case Icmp6OptionType.HandoverKeyRequest:
                    return new HandoverKeyRequestIcmp6Option(reader);
                case Icmp6OptionType.HandoverKeyReply:
                    return new HandoverKeyReplyIcmp6Option(reader);
                case Icmp6OptionType.HandoverAssistInformation:
                    return new HandoverAssistInformationIcmp6Option(reader);
                case Icmp6OptionType.MobileNodeIdentifier:
                    return new MobileNodeIdentifierIcmp6Option(reader);
                case Icmp6OptionType.DNSSearchList:
                    return new DnsSearchListIcmp6Option(reader);
                case Icmp6OptionType.ProxySignature:
                    return new ProxySignatureIcmp6Option(reader);
                case Icmp6OptionType.AddressRegistration:
                    return new AddressRegistrationIcmp6Option(reader);
                case Icmp6OptionType.LoWPANContext:
                    return new LowpanContextIcmp6Option(reader);
                case Icmp6OptionType.AuthoritativeBorderRouter:
                    return new AuthoritativeBorderRouterIcmp6Option(reader);
                default:
                    return new GenericIcmp6Option(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic ICMP6 option (RFC 4861).
    /// </summary>
    public sealed class GenericIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The option type.</param>
        public GenericIcmp6Option(BinaryReader reader, Icmp6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2));
        }
    }

    /// <summary>
    /// Prefix Information ICMP6 option (RFC 4861).
    /// </summary>
    public sealed class PrefixInformationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the number of leading bits in the <see cref="Prefix"/> that are valid.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets a value indicating whether L flag is set.
        /// </summary>
        public bool OnLink { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool AutonomousAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool RouterAddress { get; private set; }

        /// <summary>
        /// Gets the reserved 5 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the valid lifetime in seconds.
        /// </summary>
        public uint ValidLifetime { get; private set; }

        /// <summary>
        /// Gets the preferred lifetime in seconds.
        /// </summary>
        public uint PreferredLifetime { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Gets the IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> Prefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixInformationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public PrefixInformationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.PrefixInformation)
        {
            Contract.Requires(reader != null);

            PrefixLength = reader.ReadByte();
            var bits = reader.ReadByte();
            OnLink = Convert.ToBoolean((byte)(bits >> 7));
            AutonomousAddressConfiguration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            RouterAddress = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Reserved = (byte)((byte)(bits << 3) >> 3);
            ValidLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            PreferredLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            Prefix = new ReadOnlyCollection<byte>(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Redirected Header ICMP6 option (RFC 4861).
    /// </summary>
    public sealed class RedirectedHeaderIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 48 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the IP6 header.
        /// </summary>
        public IP6Header IP6Header { get; private set; }

        /// <summary>
        /// Gets the redirect data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedirectedHeaderIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RedirectedHeaderIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.RedirectedHeader)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(6));
            IP6Header = new IP6Header(reader);
            Data = new ReadOnlyCollection<byte>(reader.ReadBytes((int)((Length * 8) - 2 - 6 - 40 - IP6Header.ExtensionsLength)));
        }
    }

    /// <summary>
    /// Maximum Transmission Unit ICMP6 option (RFC 4861).
    /// </summary>
    public sealed class MaximumTransmissionUnitIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the maximum transmission unit.
        /// </summary>
        public uint MaximumTransmissionUnit { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaximumTransmissionUnitIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MaximumTransmissionUnitIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.MaximumTransmissionUnit)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            MaximumTransmissionUnit = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Shortcut Limit ICMP6 option (RFC 2491).
    /// </summary>
    public sealed class ShortcutLimitIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the shortcut limit.
        /// </summary>
        public byte ShortcutLimit { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShortcutLimitIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ShortcutLimitIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.ShortcutLimit)
        {
            Contract.Requires(reader != null);

            ShortcutLimit = reader.ReadByte();
            Reserved = reader.ReadByte();
            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Advertisement Interval ICMP6 option (RFC 6275).
    /// </summary>
    public sealed class AdvertisementIntervalIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the advertisement interval in milliseconds.
        /// </summary>
        public uint AdvertisementInterval { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvertisementIntervalIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AdvertisementIntervalIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.AdvertisementInterval)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            AdvertisementInterval = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Home Agent Information ICMP6 option (RFC 6275).
    /// </summary>
    public sealed class HomeAgentInformationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool MobileRouterSupport { get; private set; }

        /// <summary>
        /// Gets the reserved 15 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the home agent preference.
        /// </summary>
        public ushort HomeAgentPreference { get; private set; }

        /// <summary>
        /// Gets the home agent lifetime in seconds.
        /// </summary>
        public ushort HomeAgentLifetime { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeAgentInformationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HomeAgentInformationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.HomeAgentInformation)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            MobileRouterSupport = Convert.ToBoolean((byte)(bits >> 7));
            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 1) >> 1), reader.ReadByte() });
            HomeAgentPreference = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            HomeAgentLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Address List ICMP6 option (RFC 3122).
    /// </summary>
    public sealed class AddressListIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 48 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the list of IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> Addresses { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressListIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The option type.</param>
        public AddressListIcmp6Option(BinaryReader reader, Icmp6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp6OptionType.SourceAddressList || type == Icmp6OptionType.TargetAddressList);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(6));
            var ips = new IPAddress[(Length - 8) / 16];
            for(int i = 0, j = ips.Length; i < j; i++)
                ips[i] = new IPAddress(reader.ReadBytes(16));
            Addresses = new ReadOnlyCollection<IPAddress>(ips);
        }
    }

    /// <summary>
    /// Cryptographically Generated Address ICMP6 option (RFC 3971).
    /// </summary>
    public sealed class CryptographicallyGeneratedAddressIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the <see cref="Padding"/> length in bytes.
        /// </summary>
        public byte PadLength { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the list of CGA parameters data structure.
        /// </summary>
        public ReadOnlyCollection<byte> Parameters { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CryptographicallyGeneratedAddressIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CryptographicallyGeneratedAddressIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.CryptographicallyGeneratedAddress)
        {
            Contract.Requires(reader != null);

            PadLength = reader.ReadByte();
            Reserved = reader.ReadByte();

            // data structure
            Parameters = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2 - PadLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(PadLength));
        }
    }

    /// <summary>
    /// RSA Signature ICMP6 option (RFC 3971).
    /// </summary>
    public sealed class RsaSignatureIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the key hash.
        /// </summary>
        public ReadOnlyCollection<byte> KeyHash { get; private set; }

        /// <summary>
        /// Gets the digital signature.
        /// </summary>
        public ReadOnlyCollection<byte> DigitalSignature { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RsaSignatureIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RsaSignatureIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.RSASignature)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            KeyHash = new ReadOnlyCollection<byte>(reader.ReadBytes(16));

            // unknown length
            DigitalSignature = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 18));

            // unknown length
            Padding = new ReadOnlyCollection<byte>(new byte[0]);
        }
    }

    /// <summary>
    /// Time Stamp ICMP6 option (RFC 3971).
    /// </summary>
    public sealed class TimeStampIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 48 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        public ulong TimeStamp { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeStampIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeStampIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.TimeStamp)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(6));
            TimeStamp = NetworkToHostOrder.ToUInt64(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// Constants that represent name types.
    /// </summary>
    public enum NameType
    {
        /// <summary>
        /// Distinguished Encoding Rules X.501 Name
        /// </summary>
        DER = 1,

        /// <summary>
        /// Fully Qualified Domain Name
        /// </summary>
        FQDN = 2,

        /// <summary>
        /// SHA-1 Subject Key Identifier
        /// </summary>
        SHA1SKI = 3,

        /// <summary>
        /// SHA-224 Subject Key Identifier
        /// </summary>
        SHA224SKI = 4,

        /// <summary>
        /// SHA-256 Subject Key Identifier
        /// </summary>
        SHA256SKI = 5,

        /// <summary>
        /// SHA-384 Subject Key Identifier
        /// </summary>
        SHA384SKI = 6,

        /// <summary>
        /// SHA-512 Subject Key Identifier
        /// </summary>
        SHA512SKI = 7
    }

    /// <summary>
    /// Trust Anchor ICMP6 option (RFC 3971).
    /// </summary>
    public sealed class TrustAnchorIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the name type.
        /// </summary>
        public NameType NameType { get; private set; }

        /// <summary>
        /// Gets the <see cref="Padding"/> length in bytes.
        /// </summary>
        public byte PadLength { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public ReadOnlyCollection<byte> Name { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrustAnchorIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TrustAnchorIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.TrustAnchor)
        {
            Contract.Requires(reader != null);

            NameType = (NameType)reader.ReadByte();
            PadLength = reader.ReadByte();
            Name = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2 - PadLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(PadLength));
        }
    }

    /// <summary>
    /// Constants that represent certificate types.
    /// </summary>
    public enum CertificateType
    {
        /// <summary>
        /// Public Key Infrastructure X.509v3 (PKIX)
        /// </summary>
        X509v3 = 1
    }

    /// <summary>
    /// Certificate ICMP6 option (RFC 3971).
    /// </summary>
    public sealed class CertificateIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the certificate type.
        /// </summary>
        public CertificateType CertificateType { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the certificate.
        /// </summary>
        public ReadOnlyCollection<byte> Certificate { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificateIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CertificateIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.Certificate)
        {
            Contract.Requires(reader != null);

            CertificateType = (CertificateType)reader.ReadByte();
            Reserved = reader.ReadByte();

            // unknown length
            Certificate = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2));

            // unknown length
            Padding = new ReadOnlyCollection<byte>(new byte[0]);
        }
    }

    /// <summary>
    /// Address Or Prefix ICMP6 option (RFC 5568).
    /// </summary>
    public sealed class AddressOrPrefixIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public AddressOrPrefixCode Code { get; private set; }

        /// <summary>
        /// Gets the <see cref="AddressOrPrefix"/> length in bits.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the IP6 address or prefix.
        /// </summary>
        public ReadOnlyCollection<byte> AddressOrPrefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressOrPrefixIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddressOrPrefixIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.AddressOrPrefix)
        {
            Contract.Requires(reader != null);

            Code = (AddressOrPrefixCode)reader.ReadByte();
            PrefixLength = reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            AddressOrPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 6));
        }
    }

    /// <summary>
    /// New Router Prefix Information ICMP6 option (RFC 4068)
    /// </summary>
    public sealed class NewRouterPrefixInformationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public byte Code { get; private set; }

        /// <summary>
        /// Gets the number of leading bits in the <see cref="Prefix"/> that are valid.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> Prefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NewRouterPrefixInformationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NewRouterPrefixInformationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.NewRouterPrefixInformation)
        {
            Contract.Requires(reader != null);

            Code = reader.ReadByte();
            PrefixLength = reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            Prefix = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 6));
        }
    }

    /// <summary>
    /// Constants that represent link-layer address codes.
    /// </summary>
    public enum LinkLayerAddressCode
    {
        /// <summary>
        /// Wildcard
        /// </summary>
        Wildcard = 0,

        /// <summary>
        /// New Access Point
        /// </summary>
        NewAccessPoint = 1,

        /// <summary>
        /// Mobile Node
        /// </summary>
        MobileNode = 2,

        /// <summary>
        /// New Access Router
        /// </summary>
        NewAccessRouter = 3,

        /// <summary>
        /// Source of fast neighborhood discovery.
        /// </summary>
        SourceOfNeighborhoodDiscovery = 4,

        /// <summary>
        /// Current interface of router.
        /// </summary>
        CurrentInterfaceOfRouter = 5,

        /// <summary>
        /// No prefix information available.
        /// </summary>
        NoPrefixInformationAvailable = 6,

        /// <summary>
        /// No fast handover support available.
        /// </summary>
        NoFastHandoverSupportAvailable = 7
    }

    /// <summary>
    /// Link-Layer Address ICMP6 option (RFC 5568).
    /// </summary>
    public sealed class LinkLayerAddressIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public LinkLayerAddressCode Code { get; private set; }

        /// <summary>
        /// Gets LLA.
        /// </summary>
        public ReadOnlyCollection<byte> LinkLayerAddress { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkLayerAddressIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public LinkLayerAddressIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.LinkLayerAddress)
        {
            Contract.Requires(reader != null);

            Code = (LinkLayerAddressCode)reader.ReadByte();

            // unknown length
            LinkLayerAddress = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 1));

            // unknown length
            Padding = new ReadOnlyCollection<byte>(new byte[0]);
        }
    }

    /// <summary>
    /// Constants that represent neighbor advertisement acknowledgment statuses.
    /// </summary>
    public enum NeighborAdvertisementAcknowledgmentStatus
    {
        /// <summary>
        /// Perform address configuration.
        /// </summary>
        PerformAddressConfiguration = 1,

        /// <summary>
        /// Use supplied new care of address.
        /// </summary>
        UseSuppliedNewCareOfAddress = 2,

        /// <summary>
        /// Use new access router address.
        /// </summary>
        UseNewAccessRouterAddress = 3,

        /// <summary>
        /// Previous care of address supplied.
        /// </summary>
        PreviousCareOfAddressSupplied = 4,

        /// <summary>
        /// Link-layer address unrecognized.
        /// </summary>
        LinkLayerAddressUnrecognized = 128
    }

    /// <summary>
    /// Neighbor Advertisement Acknowledgment ICMP6 option (RFC 5568).
    /// </summary>
    public sealed class NeighborAdvertisementAcknowledgmentIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public byte Code { get; private set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public NeighborAdvertisementAcknowledgmentStatus Status { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the care-of IP6 address.
        /// </summary>
        public IPAddress CareOfAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeighborAdvertisementAcknowledgmentIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NeighborAdvertisementAcknowledgmentIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.NeighborAdvertisementAcknowledgment)
        {
            Contract.Requires(reader != null);

            Code = reader.ReadByte();
            Status = (NeighborAdvertisementAcknowledgmentStatus)reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            if(Length > 1)
                CareOfAddress = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Mobility Anchor Point ICMP6 option (RFC 4140).
    /// </summary>
    public sealed class MobilityAnchorPointIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the distance.
        /// </summary>
        public byte Distance { get; private set; }

        /// <summary>
        /// Gets the preference.
        /// </summary>
        public byte Preference { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool RegionalCareOfAddress { get; private set; }

        /// <summary>
        /// Gets the reserved 7 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the valid lifetime in seconds.
        /// </summary>
        public uint ValidLifetime { get; private set; }

        /// <summary>
        /// Gets the global IP6 address.
        /// </summary>
        public IPAddress GlobalAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobilityAnchorPointIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MobilityAnchorPointIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.MobilityAnchorPoint)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Distance = (byte)(bits >> 4);
            Preference = (byte)((byte)(bits << 4) >> 4);
            bits = reader.ReadByte();
            RegionalCareOfAddress = Convert.ToBoolean((byte)(bits >> 7));
            Reserved = (byte)((byte)(bits << 1) >> 1);
            ValidLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            GlobalAddress = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Route Information ICMP6 option (RFC 4191).
    /// </summary>
    public sealed class RouteInformationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the number of leading bits in the <see cref="Prefix"/> that are valid.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the route preference.
        /// </summary>
        public byte RoutePreference { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the router lifetime in seconds.
        /// </summary>
        public uint RouterLifetime { get; private set; }

        /// <summary>
        /// Gets the IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> Prefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteInformationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouteInformationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.RouteInformation)
        {
            Contract.Requires(reader != null);

            PrefixLength = reader.ReadByte();
            var bits = reader.ReadByte();
            Reserved = (byte)(bits >> 5);
            RoutePreference = (byte)((byte)(bits << 3) >> 6);
            Reserved2 = (byte)((byte)(bits << 5) >> 5);
            RouterLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Prefix = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 6));
        }
    }

    /// <summary>
    /// Recursive DNS Server ICMP6 option (RFC 5006, RFC 6106).
    /// </summary>
    public sealed class RecursiveDnsServerIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the lifetime in seconds.
        /// </summary>
        public uint Lifetime { get; private set; }

        /// <summary>
        /// Gets the list of IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> Addresses { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursiveDnsServerIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RecursiveDnsServerIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.RecursiveDNSServer)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            Lifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            var ips = new IPAddress[(Length - 1) / 2];
            for(int i = 0, j = ips.Length; i < j; i++)
                ips[i] = new IPAddress(reader.ReadBytes(16));
            Addresses = new ReadOnlyCollection<IPAddress>(ips);
        }
    }

    /// <summary>
    /// Expanded Flags ICMP6 option (RFC 5175).
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flags")]
    public sealed class ExpandedFlagsIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool ManagedAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether O flag is set.
        /// </summary>
        public bool OtherStatefulConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether H flag is set.
        /// </summary>
        public bool HomeAgent { get; private set; }

        /// <summary>
        /// Gets the default router preference.
        /// </summary>
        public DefaultRouterPreference DefaultRouterPreference { get; private set; }

        /// <summary>
        /// Gets a value indicating whether P flag is set.
        /// </summary>
        public bool NeighborDiscoveryProxy { get; private set; }

        /// <summary>
        /// Gets the reserved 42 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpandedFlagsIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ExpandedFlagsIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.ExpandedFlags)
        {
            Contract.Requires(reader != null);

            var bytes = reader.ReadBytes((1 * 8) - 2);
            ManagedAddressConfiguration = Convert.ToBoolean((byte)(bytes[0] >> 7));
            OtherStatefulConfiguration = Convert.ToBoolean((byte)((byte)(bytes[0] << 1) >> 7));
            HomeAgent = Convert.ToBoolean((byte)((byte)(bytes[0] << 2) >> 7));
            DefaultRouterPreference = (DefaultRouterPreference)(byte)((byte)(bytes[0] << 3) >> 6);
            NeighborDiscoveryProxy = Convert.ToBoolean((byte)((byte)(bytes[0] << 5) >> 7));
            bytes[0] = (byte)((byte)(bytes[0] << 6) >> 6);
            Reserved = new ReadOnlyCollection<byte>(bytes);
        }
    }

    /// <summary>
    /// Handover Key Request ICMP6 option (RFC 5269).
    /// </summary>
    public sealed class HandoverKeyRequestIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the <see cref="Padding"/> length in bytes.
        /// </summary>
        public byte PadLength { get; private set; }

        /// <summary>
        /// Gets AT.
        /// </summary>
        public byte AlgorithmType { get; private set; }

        /// <summary>
        /// Gets the reserved 4 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the handover key encryption public key.
        /// </summary>
        public ReadOnlyCollection<byte> HandoverKeyEncryptionPublicKey { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverKeyRequestIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoverKeyRequestIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.HandoverKeyRequest)
        {
            Contract.Requires(reader != null);

            PadLength = reader.ReadByte();
            var bits = reader.ReadByte();
            AlgorithmType = (byte)(bits >> 4);
            Reserved = (byte)((byte)(bits << 4) >> 4);
            HandoverKeyEncryptionPublicKey = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2 - PadLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(PadLength));
        }
    }

    /// <summary>
    /// Handover Key Reply ICMP6 option (RFC 5269).
    /// </summary>
    public sealed class HandoverKeyReplyIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the <see cref="Padding"/> length in bytes.
        /// </summary>
        public byte PadLength { get; private set; }

        /// <summary>
        /// Gets AT.
        /// </summary>
        public byte AlgorithmType { get; private set; }

        /// <summary>
        /// Gets the reserved 4 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the key lifetime in seconds.
        /// </summary>
        public uint KeyLifetime { get; private set; }

        /// <summary>
        /// Gets the encrypted handover key.
        /// </summary>
        public ReadOnlyCollection<byte> EncryptedHandoverKey { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverKeyReplyIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoverKeyReplyIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.HandoverKeyReply)
        {
            Contract.Requires(reader != null);

            PadLength = reader.ReadByte();
            var bits = reader.ReadByte();
            AlgorithmType = (byte)(bits >> 4);
            Reserved = (byte)((byte)(bits << 4) >> 4);
            KeyLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            EncryptedHandoverKey = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 4 - PadLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(PadLength));
        }
    }

    /// <summary>
    /// Constants that represent handover assist information codes.
    /// </summary>
    public enum HandoverAssistInformationCode
    {
        /// <summary>
        /// Access Network Identifier
        /// </summary>
        ANID = 1,

        /// <summary>
        /// Sector Identifier
        /// </summary>
        SID = 2
    }

    /// <summary>
    /// Handover Assist Information ICMP6 option (RFC 5269).
    /// </summary>
    public sealed class HandoverAssistInformationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public HandoverAssistInformationCode Code { get; private set; }

        /// <summary>
        /// Gets the <see cref="HaiValue"/> length in bytes.
        /// </summary>
        public byte HaiLength { get; private set; }

        /// <summary>
        /// Gets the HAI value.
        /// </summary>
        public ReadOnlyCollection<byte> HaiValue { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverAssistInformationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public HandoverAssistInformationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.HandoverAssistInformation)
        {
            Contract.Requires(reader != null);

            Code = (HandoverAssistInformationCode)reader.ReadByte();
            HaiLength = reader.ReadByte();
            HaiValue = new ReadOnlyCollection<byte>(reader.ReadBytes(HaiLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2 - HaiLength));
        }
    }

    /// <summary>
    /// Constants that represent mobile node identifier code.
    /// </summary>
    public enum MobileNodeIdentifierCode
    {
        /// <summary>
        /// Network Access Identifier
        /// </summary>
        NAI = 1,

        /// <summary>
        /// International Mobile Subscriber Identity
        /// </summary>
        IMSI = 2
    }

    /// <summary>
    /// Mobile Node Identifier ICMP6 option (RFC 5271).
    /// </summary>
    public sealed class MobileNodeIdentifierIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public MobileNodeIdentifierCode Code { get; private set; }

        /// <summary>
        /// Gets the <see cref="MnidValue"/> length in bytes.
        /// </summary>
        public byte MnidLength { get; private set; }

        /// <summary>
        /// Gets the MNID value.
        /// </summary>
        public ReadOnlyCollection<byte> MnidValue { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobileNodeIdentifierIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MobileNodeIdentifierIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.MobileNodeIdentifier)
        {
            Contract.Requires(reader != null);

            Code = (MobileNodeIdentifierCode)reader.ReadByte();
            MnidLength = reader.ReadByte();
            MnidValue = new ReadOnlyCollection<byte>(reader.ReadBytes(MnidLength));
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 2 - MnidLength));
        }
    }

    /// <summary>
    /// DNS SearchList ICMP6 option (RFC 6106).
    /// </summary>
    public sealed class DnsSearchListIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the lifetime in seconds.
        /// </summary>
        public uint Lifetime { get; private set; }

        /// <summary>
        /// Gets the list of domain names.
        /// </summary>
        public ReadOnlyCollection<string> DomainNames { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DnsSearchListIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DnsSearchListIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.DNSSearchList)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            Lifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            var names = new List<string>();
            long i = reader.BaseStream.Position + ((Length * 8) - 2 - 6);
            while(reader.BaseStream.Position < i)
            {
                var n = DnsMessage.DomainName(reader);
                if(n != null)
                    names.Add(n);
            }
            DomainNames = new ReadOnlyCollection<string>(names.ToArray());
        }
    }

    /// <summary>
    /// Proxy Signature ICMP6 option (RFC 6496).
    /// </summary>
    public sealed class ProxySignatureIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the key hash.
        /// </summary>
        public ReadOnlyCollection<byte> KeyHash { get; private set; }

        /// <summary>
        /// Gets the digital signature.
        /// </summary>
        public ReadOnlyCollection<byte> DigitalSignature { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxySignatureIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ProxySignatureIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.ProxySignature)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            KeyHash = new ReadOnlyCollection<byte>(reader.ReadBytes(16));

            // unknown length
            DigitalSignature = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 2 - 18));

            // unknown length
            Padding = new ReadOnlyCollection<byte>(new byte[0]);
        }
    }

    /// <summary>
    /// Constants that represent address registration status.
    /// </summary>
    public enum AddressRegistrationStatus
    {
        /// <summary>
        /// Success.
        /// </summary>
        Success = 0,

        /// <summary>
        /// Duplicate address.
        /// </summary>
        DuplicateAddress = 1,

        /// <summary>
        /// Neighbor cache full.
        /// </summary>
        NeighborCacheFull = 2
    }

    /// <summary>
    /// Address Registration ICMP6 option (RFC 6775).
    /// </summary>
    public sealed class AddressRegistrationIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public AddressRegistrationStatus Status { get; private set; }

        /// <summary>
        /// Gets the reserved 24 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the registration lifetime in 60-second units.
        /// </summary>
        public ushort RegistrationLifetime { get; private set; }

        /// <summary>
        /// Gets EUI-64.
        /// </summary>
        public ReadOnlyCollection<byte> ExtendedUniqueIdentifier64 { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressRegistrationIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddressRegistrationIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.AddressRegistration)
        {
            Contract.Requires(reader != null);

            Status = (AddressRegistrationStatus)reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(3));
            RegistrationLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ExtendedUniqueIdentifier64 = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// LoWPAN Context ICMP6 option (RFC 6775).
    /// </summary>
    public sealed class LowpanContextIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the number of leading bits in the <see cref="ContextPrefix"/> that are valid.
        /// </summary>
        public byte ContextLength { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether C flag is set.
        /// </summary>
        public bool ContextCompression { get; private set; }

        /// <summary>
        /// Gets CID.
        /// </summary>
        public byte ContextIdentifier { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ushort Reserved2 { get; private set; }

        /// <summary>
        /// Gets the valid lifetime in 60-second units.
        /// </summary>
        public ushort ValidLifetime { get; private set; }

        /// <summary>
        /// Gets the context IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> ContextPrefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LowpanContextIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public LowpanContextIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.LoWPANContext)
        {
            Contract.Requires(reader != null);

            ContextLength = reader.ReadByte();
            var bits = reader.ReadByte();
            Reserved = (byte)(bits >> 5);
            ContextCompression = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            ContextIdentifier = (byte)((byte)(bits << 4) >> 4);
            Reserved2 = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ValidLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ContextPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes((Length * 8) - 8));
        }
    }

    /// <summary>
    /// Authoritative Border Router ICMP6 option (RFC 6775).
    /// </summary>
    public sealed class AuthoritativeBorderRouterIcmp6Option : Icmp6Option
    {
        /// <summary>
        /// Gets the version number least significant 16 bits.
        /// </summary>
        public ushort VersionLow { get; private set; }

        /// <summary>
        /// Gets the version number most significant 16 bits.
        /// </summary>
        public ushort VersionHigh { get; private set; }

        /// <summary>
        /// Gets the valid lifetime in 60-second units.
        /// </summary>
        public ushort ValidLifetime { get; private set; }

        /// <summary>
        /// Gets the border router IP6 address.
        /// </summary>
        public IPAddress BorderRouterAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthoritativeBorderRouterIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AuthoritativeBorderRouterIcmp6Option(BinaryReader reader) : base(reader, Icmp6OptionType.AuthoritativeBorderRouter)
        {
            Contract.Requires(reader != null);

            VersionLow = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            VersionHigh = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ValidLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            if(ValidLifetime == 0)
                ValidLifetime = 10000;
            BorderRouterAddress = new IPAddress(reader.ReadBytes(16));
        }
    }
}