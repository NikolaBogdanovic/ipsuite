﻿namespace IPSuite.ICMP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;
    using System.Numerics;

    /// <summary>
    /// Constants that represent RPL ICMP6 option types.
    /// </summary>
    public enum RplIcmp6OptionType
    {
        /// <summary>
        /// Pad 0 (RFC 6550)
        /// </summary>
        Pad1 = 0,

        /// <summary>
        /// Pad 1 (RFC 6550)
        /// </summary>
        PadN = 1,

        /// <summary>
        /// DAG Metric Container (RFC 6550)
        /// </summary>
        DAGMetricContainer = 2,

        /// <summary>
        /// Route Information (RFC 6550)
        /// </summary>
        RouteInformation = 3,

        /// <summary>
        /// DODAG Configuration (RFC 6550)
        /// </summary>
        DODAGConfiguration = 4,

        /// <summary>
        /// Target (RFC 6550)
        /// </summary>
        Target = 5,

        /// <summary>
        /// Transit Information (RFC 6550)
        /// </summary>
        TransitInformation = 6,

        /// <summary>
        /// Solicited Information (RFC 6550)
        /// </summary>
        SolicitedInformation = 7,

        /// <summary>
        /// Prefix Information (RFC 6550)
        /// </summary>
        PrefixInformation = 8,

        /// <summary>
        /// Target Descriptor (RFC 6550)
        /// </summary>
        TargetDescriptor = 9
    }

    /// <summary>
    /// RPL ICMP6 option (RFC 6550).
    /// </summary>
    public abstract class RplIcmp6Option
    {
        /// <summary>
        /// Gets the option type.
        /// </summary>
        public RplIcmp6OptionType RplType { get; private set; }

        /// <summary>
        /// Gets the data length in bytes.
        /// </summary>
        public byte DataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The RPL type.</param>
        protected RplIcmp6Option(BinaryReader reader, RplIcmp6OptionType type)
        {
            Contract.Requires(reader != null);

            RplType = type;
            if(RplType == RplIcmp6OptionType.Pad1)
                return;
            DataLength = reader.ReadByte();
        }

        /// <summary>
        /// Creates a RPL ICMP6 option.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A RPL ICMP6 option.
        /// </returns>
        public static RplIcmp6Option Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var type = (RplIcmp6OptionType)reader.ReadByte();
            switch(type)
            {
                case RplIcmp6OptionType.RouteInformation:
                    return new RouteInformationRplIcmp6Option(reader);
                case RplIcmp6OptionType.DODAGConfiguration:
                    return new DodagConfigurationRplIcmp6Option(reader);
                case RplIcmp6OptionType.Target:
                    return new TargetRplIcmp6Option(reader);
                case RplIcmp6OptionType.TransitInformation:
                    return new TransitInformationRplIcmp6Option(reader);
                case RplIcmp6OptionType.SolicitedInformation:
                    return new SolicitedInformationRplIcmp6Option(reader);
                case RplIcmp6OptionType.PrefixInformation:
                    return new PrefixInformationRplIcmp6Option(reader);
                case RplIcmp6OptionType.TargetDescriptor:
                    return new TargetRplIcmp6Option(reader);
                default:
                    return new GenericRplIcmp6Option(reader, type);
            }
        }
    }

    /// <summary>
    /// Generic RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class GenericRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the option data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The RPL type.</param>
        public GenericRplIcmp6Option(BinaryReader reader, RplIcmp6OptionType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// Constants that represent routing metric or constraint types.
    /// </summary>
    public enum RoutingMetricOrConstraintType
    {
        /// <summary>
        /// Node State And Attribute
        /// </summary>
        NodeStateAndAttribute = 1,

        /// <summary>
        /// Node Energy
        /// </summary>
        NodeEnergy = 2,

        /// <summary>
        /// Hop Count
        /// </summary>
        HopCount = 3,

        /// <summary>
        /// Link Throughput
        /// </summary>
        LinkThroughput = 4,

        /// <summary>
        /// Link Latency
        /// </summary>
        LinkLatency = 5,

        /// <summary>
        /// Link Quality Level
        /// </summary>
        LinkQualityLevel = 6,

        /// <summary>
        /// Link ETX
        /// </summary>
        LinkETX = 7,

        /// <summary>
        /// Link Color
        /// </summary>
        LinkColor = 8
    }

    /// <summary>
    /// Constants that represent aggregated routing metrics.
    /// </summary>
    public enum AggregatedRoutingMetric
    {
        /// <summary>
        /// Additive
        /// </summary>
        Additive = 0,

        /// <summary>
        /// Reports Maximum
        /// </summary>
        ReportsMaximum = 1,

        /// <summary>
        /// Reports Minimum
        /// </summary>
        ReportsMinimum = 2,

        /// <summary>
        /// Multiplicative
        /// </summary>
        Multiplicative = 3
    }

    /// <summary>
    /// DAG Metric Container RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class DagMetricContainerRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Routing Metric Or Constraint class.
        /// </summary>
        public sealed class RoutingMetricOrConstraint
        {
            /// <summary>
            /// Gets the routing type.
            /// </summary>
            public RoutingMetricOrConstraintType RoutingType { get; private set; }

            /// <summary>
            /// Gets the reserved 5 bits.
            /// </summary>
            public byte Reserved { get; private set; }

            /// <summary>
            /// Gets a value indicating whether P flag is set.
            /// </summary>
            public bool Partial { get; private set; }

            /// <summary>
            /// Gets a value indicating whether C flag is set.
            /// </summary>
            public bool ConstraintOrMetric { get; private set; }

            /// <summary>
            /// Gets a value indicating whether O flag is set.
            /// </summary>
            public bool OptionalConstraint { get; private set; }

            /// <summary>
            /// Gets a value indicating whether R flag is set.
            /// </summary>
            public bool RecordedOrAggregated { get; private set; }

            /// <summary>
            /// Gets A.
            /// </summary>
            public AggregatedRoutingMetric AggregatedRoutingMetric { get; private set; }

            /// <summary>
            /// Gets the precedence.
            /// </summary>
            public byte Precedence { get; private set; }

            /// <summary>
            /// Gets the body length in bytes.
            /// </summary>
            public byte BodyLength { get; private set; }

            /// <summary>
            /// Gets the object body.
            /// </summary>
            public ReadOnlyCollection<byte> Body { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="RoutingMetricOrConstraint"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            public RoutingMetricOrConstraint(BinaryReader reader)
            {
                Contract.Requires(reader != null);

                RoutingType = (RoutingMetricOrConstraintType)reader.ReadByte();
                var bits = reader.ReadByte();
                Reserved = (byte)(bits >> 3);
                Partial = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
                ConstraintOrMetric = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
                OptionalConstraint = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
                bits = reader.ReadByte();
                RecordedOrAggregated = Convert.ToBoolean((byte)(bits >> 7));
                AggregatedRoutingMetric = (AggregatedRoutingMetric)(byte)((byte)(bits << 1) >> 5);
                Precedence = (byte)((byte)(bits << 4) >> 4);
                BodyLength = reader.ReadByte();
                Body = new ReadOnlyCollection<byte>(reader.ReadBytes(BodyLength));
            }
        }

        /// <summary>
        /// Gets the list of metric data.
        /// </summary>
        public ReadOnlyCollection<RoutingMetricOrConstraint> MetricData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DagMetricContainerRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DagMetricContainerRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.DAGMetricContainer)
        {
            Contract.Requires(reader != null);

            var metrics = new List<RoutingMetricOrConstraint>();
            for(int i = 0, j = DataLength; i < j;)
            {
                var o = new RoutingMetricOrConstraint(reader);
                metrics.Add(o);
                i += 4 + o.BodyLength;
            }
            MetricData = new ReadOnlyCollection<RoutingMetricOrConstraint>(metrics);
        }
    }

    /// <summary>
    /// Route Information RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class RouteInformationRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the number of leading bits in the <see cref="Prefix"/> that are valid.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the route preference.
        /// </summary>
        public byte RoutePreference { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the route lifetime in seconds.
        /// </summary>
        public uint RouteLifetime { get; private set; }

        /// <summary>
        /// Gets the IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> Prefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteInformationRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouteInformationRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.RouteInformation)
        {
            Contract.Requires(reader != null);

            PrefixLength = reader.ReadByte();
            var bits = reader.ReadByte();
            Reserved = (byte)(bits >> 5);
            RoutePreference = (byte)((byte)(bits << 3) >> 6);
            Reserved2 = (byte)((byte)(bits << 5) >> 5);
            RouteLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Prefix = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 6));
        }
    }

    /// <summary>
    /// DODAG Configuration RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class DodagConfigurationRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the unused 4 bits.
        /// </summary>
        public byte Unused { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool AuthenticationEnabled { get; private set; }
        
        /// <summary>
        /// Gets PCS.
        /// </summary>
        public byte PathControlSize { get; private set; }

        /// <summary>
        /// Gets the DIO interval doublings.
        /// </summary>
        public byte DioIntervalDoublings { get; private set; }

        /// <summary>
        /// Gets the DIO interval minimum.
        /// </summary>
        public byte DioIntervalMin { get; private set; }

        /// <summary>
        /// Gets the DIO redundancy constant.
        /// </summary>
        public byte DioRedundancyConstant { get; private set; }

        /// <summary>
        /// Gets the maximum rank increase.
        /// </summary>
        public ushort MaxRankIncrease { get; private set; }

        /// <summary>
        /// Gets the minimum hop rank increase.
        /// </summary>
        public ushort MinHopRankIncrease { get; private set; }

        /// <summary>
        /// Gets OCP.
        /// </summary>
        public ushort ObjectiveCodePoint { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the default lifetime in the <see cref="LifetimeUnit"/> units.
        /// </summary>
        public byte DefaultLifetime { get; private set; }

        /// <summary>
        /// Gets the lifetime unit in seconds.
        /// </summary>
        public ushort LifetimeUnit { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DodagConfigurationRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DodagConfigurationRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.DODAGConfiguration)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Unused = (byte)(bits >> 4);
            AuthenticationEnabled = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            PathControlSize = (byte)((byte)(bits << 5) >> 5);
            DioIntervalDoublings = reader.ReadByte();
            DioIntervalMin = reader.ReadByte();
            DioRedundancyConstant = reader.ReadByte();
            MaxRankIncrease = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            MinHopRankIncrease = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ObjectiveCodePoint = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Reserved = reader.ReadByte();
            DefaultLifetime = reader.ReadByte();
            LifetimeUnit = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Target RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class TargetRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the unused 8 bits.
        /// </summary>
        public byte Unused { get; private set; }

        /// <summary>
        /// Gets the number of valid leading bits in the <see cref="TargetPrefix"/>.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets the target IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> TargetPrefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TargetRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TargetRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.Target)
        {
            Contract.Requires(reader != null);

            Unused = reader.ReadByte();
            PrefixLength = reader.ReadByte();
            TargetPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 2));
        }
    }

    /// <summary>
    /// Transit Information RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class TransitInformationRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets a value indicating whether E flag is set.
        /// </summary>
        public bool External { get; private set; }

        /// <summary>
        /// Gets the unused 7 bits.
        /// </summary>
        public byte Unused { get; private set; }
        
        /// <summary>
        /// Gets PC1.
        /// </summary>
        public byte PathControl1 { get; private set; }

        /// <summary>
        /// Gets PC2.
        /// </summary>
        public byte PathControl2 { get; private set; }

        /// <summary>
        /// Gets PC3.
        /// </summary>
        public byte PathControl3 { get; private set; }

        /// <summary>
        /// Gets PC4.
        /// </summary>
        public byte PathControl4 { get; private set; }

        /// <summary>
        /// Gets the path sequence number.
        /// </summary>
        public byte PathSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the path lifetime in the <see cref="DodagConfigurationRplIcmp6Option.LifetimeUnit"/> units.
        /// </summary>
        public byte PathLifetime { get; private set; }

        /// <summary>
        /// Gets the parent IP6 address.
        /// </summary>
        public IPAddress ParentAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransitInformationRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TransitInformationRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.TransitInformation)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            External = Convert.ToBoolean((byte)(bits >> 7));
            Unused = (byte)((byte)(bits << 1) >> 1);
            bits = reader.ReadByte();
            PathControl1 = (byte)(bits >> 6);
            PathControl2 = (byte)((byte)(bits << 2) >> 6);
            PathControl3 = (byte)((byte)(bits << 4) >> 6);
            PathControl4 = (byte)((byte)(bits << 6) >> 6);
            PathSequenceNumber = reader.ReadByte();
            PathLifetime = reader.ReadByte();
            if(DataLength > 4)
                ParentAddress = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Solicited Information RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class SolicitedInformationRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether V flag is set.
        /// </summary>
        public bool VersionPredicate { get; private set; }

        /// <summary>
        /// Gets a value indicating whether I flag is set.
        /// </summary>
        public bool InstanceIdPredicate { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool DodagIdPredicate { get; private set; }

        /// <summary>
        /// Gets the unused 5 bits.
        /// </summary>
        public byte Unused { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Gets the DODAG version number.
        /// </summary>
        public byte VersionNumber { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SolicitedInformationRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SolicitedInformationRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.SolicitedInformation)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            VersionPredicate = Convert.ToBoolean((byte)(bits >> 7));
            InstanceIdPredicate = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            DodagIdPredicate = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Unused = (byte)((byte)(bits << 3) >> 3);
            DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            VersionNumber = reader.ReadByte();
        }
    }

    /// <summary>
    /// Prefix Information RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class PrefixInformationRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the number of leading bits in the <see cref="Prefix"/> that are valid.
        /// </summary>
        public byte PrefixLength { get; private set; }

        /// <summary>
        /// Gets a value indicating whether L flag is set.
        /// </summary>
        public bool OnLink { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool AutonomousAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool RouterAddress { get; private set; }

        /// <summary>
        /// Gets the reserved 5 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the valid lifetime in seconds.
        /// </summary>
        public uint ValidLifetime { get; private set; }

        /// <summary>
        /// Gets the preferred lifetime in seconds.
        /// </summary>
        public uint PreferredLifetime { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public uint Reserved2 { get; private set; }

        /// <summary>
        /// Gets the IP6 prefix.
        /// </summary>
        public ReadOnlyCollection<byte> Prefix { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixInformationRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public PrefixInformationRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.PrefixInformation)
        {
            Contract.Requires(reader != null);

            PrefixLength = reader.ReadByte();
            var bits = reader.ReadByte();
            OnLink = Convert.ToBoolean((byte)(bits >> 7));
            AutonomousAddressConfiguration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            RouterAddress = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Reserved = (byte)((byte)(bits << 3) >> 3);
            ValidLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            PreferredLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Reserved2 = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            Prefix = new ReadOnlyCollection<byte>(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Target Descriptor RPL ICMP6 option (RFC 6550).
    /// </summary>
    public sealed class TargetDescriptorRplIcmp6Option : RplIcmp6Option
    {
        /// <summary>
        /// Gets the descriptor.
        /// </summary>
        public uint Descriptor { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TargetDescriptorRplIcmp6Option"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TargetDescriptorRplIcmp6Option(BinaryReader reader) : base(reader, RplIcmp6OptionType.TargetDescriptor)
        {
            Contract.Requires(reader != null);

            Descriptor = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }
}