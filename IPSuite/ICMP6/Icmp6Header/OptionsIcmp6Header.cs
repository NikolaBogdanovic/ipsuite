﻿namespace IPSuite.ICMP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Options ICMP6 header (RFC 4443).
    /// </summary>
    public abstract class OptionsIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the list of ICMP6 options.
        /// </summary>
        public ReadOnlyCollection<Icmp6Option> Options { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="headerType">One of options header types.</param>
        protected OptionsIcmp6Header(BinaryReader reader, Icmp6HeaderType headerType) : base(reader, headerType)
        {
            Contract.Requires(reader != null);
        }

        /// <summary>
        /// Sets the ICMP6 options.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="length">The options length.</param>
        protected void SetOptions(BinaryReader reader, ushort length)
        {
            Contract.Requires(reader != null);

            var opts = new List<Icmp6Option>();
            int index = 0;
            while(index < length)
            {
                var o = Icmp6Option.Create(reader);
                opts.Add(o);
                index += o.Length * 8;
            }
            Options = new ReadOnlyCollection<Icmp6Option>(opts.ToArray());
        }
    }

    /// <summary>
    /// Multicast Listener Options ICMP6 header (RFC 2710).
    /// </summary>
    public class MulticastListenerOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the maximum response code.
        /// </summary>
        public ushort MaximumResponseCode { get; private set; }

        /// <summary>
        /// Gets the maximum response delay in milliseconds.
        /// </summary>
        public uint MaximumResponseDelay { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the multicast IP6 address.
        /// </summary>
        public IPAddress MulticastAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MulticastListenerOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="headerType">One of multicast listener header types.</param>
        public MulticastListenerOptionsIcmp6Header(BinaryReader reader, Icmp6HeaderType headerType) : base(reader, headerType)
        {
            Contract.Requires(reader != null);
            Contract.Requires(headerType == Icmp6HeaderType.MulticastListenerQuery || headerType == Icmp6HeaderType.MulticastListenerReport || headerType == Icmp6HeaderType.MulticastListenerDone);

            var bytes = reader.ReadBytes(2);
            MaximumResponseCode = NetworkToHostOrder.ToUInt16(bytes);
            if(MaximumResponseCode < 32768)
                MaximumResponseDelay = MaximumResponseCode;
            else
                MaximumResponseDelay = (uint)(NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(bytes[0] << 4) >> 4), bytes[1] }) | 0x100) << ((byte)((byte)(bytes[0] << 1) >> 5) + 3);
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            MulticastAddress = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Query Multicast Listener Options ICMP6 header (RFC 2710).
    /// </summary>
    public sealed class QueryMulticastListenerOptionsIcmp6Header : MulticastListenerOptionsIcmp6Header
    {
        /// <summary>
        /// Gets the reserved 4 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets a value indicating whether S flag is set.
        /// </summary>
        public bool SuppressRouterSideProcessing { get; private set; }

        /// <summary>
        /// Gets QRV.
        /// </summary>
        public byte QueriersRobustnessVariable { get; private set; }

        /// <summary>
        /// Gets QQIC.
        /// </summary>
        public byte QueriersQueryIntervalCode { get; private set; }

        /// <summary>
        /// Gets QQI.
        /// </summary>
        public uint QueriersQueryInterval { get; private set; }

        /// <summary>
        /// Gets the number of sources.
        /// </summary>
        public ushort NumberOfSources { get; private set; }

        /// <summary>
        /// Gets the list of source IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> SourceAddresses { get; private set; }

        /// <summary>
        /// Gets the additional data.
        /// </summary>
        public ReadOnlyCollection<byte> AdditionalData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryMulticastListenerOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public QueryMulticastListenerOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.MulticastListenerQuery)
        {
            Contract.Requires(reader != null);

            if(DataLength == 20)
                return;
            var bits = reader.ReadByte();
            Reserved2 = (byte)(bits >> 4);
            SuppressRouterSideProcessing = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            QueriersRobustnessVariable = (byte)((byte)(bits << 5) >> 5);
            QueriersQueryIntervalCode = reader.ReadByte();
            if(MaximumResponseCode < 32768)
                QueriersQueryInterval = QueriersQueryIntervalCode;
            else
                QueriersQueryInterval = (uint)((byte)((byte)(QueriersQueryIntervalCode << 4) >> 4) | 0x100) << ((byte)((byte)(QueriersQueryIntervalCode << 1) >> 5) + 3);
            NumberOfSources = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var ips = new IPAddress[NumberOfSources];
            for(int i = 0, j = ips.Length; i < j; i++)
                ips[i] = new IPAddress(reader.ReadBytes(16));
            SourceAddresses = new ReadOnlyCollection<IPAddress>(ips);
            AdditionalData = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 20 - 4 - (NumberOfSources * 16)));
        }
    }

    /// <summary>
    /// Router Solicitation Options ICMP6 header (RFC 4861).
    /// </summary>
    public sealed class RouterSolicitationOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterSolicitationOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterSolicitationOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.RouterSolicitation)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            SetOptions(reader, (ushort)(DataLength - 4));
        }
    }

    /// <summary>
    /// Constants that represent default router preferences.
    /// </summary>
    public enum DefaultRouterPreference
    {
        /// <summary>
        /// Medium
        /// </summary>
        Medium = 0,

        /// <summary>
        /// High
        /// </summary>
        High = 1,

        /// <summary>
        /// Low
        /// </summary>
        Low = 3
    }

    /// <summary>
    /// Router Advertisement Options ICMP6 header (RFC 4861).
    /// </summary>
    public sealed class RouterAdvertisementOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the current hop limit.
        /// </summary>
        public byte CurrentHopLimit { get; private set; }

        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool ManagedAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether O flag is set.
        /// </summary>
        public bool OtherStatefulConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether H flag is set.
        /// </summary>
        public bool HomeAgent { get; private set; }

        /// <summary>
        /// Gets a value indicating whether P flag is set.
        /// </summary>
        public bool NeighborDiscoveryProxy { get; private set; }

        /// <summary>
        /// Gets the default router preference.
        /// </summary>
        public DefaultRouterPreference DefaultRouterPreference { get; private set; }

        /// <summary>
        /// Gets the reserved 2 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the router lifetime in seconds.
        /// </summary>
        public ushort RouterLifetime { get; private set; }

        /// <summary>
        /// Gets the reachable time in milliseconds.
        /// </summary>
        public uint ReachableTime { get; private set; }

        /// <summary>
        /// Gets the retransmission timer in milliseconds.
        /// </summary>
        public uint RetransmissionTimer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterAdvertisementOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterAdvertisementOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.RouterAdvertisement)
        {
            Contract.Requires(reader != null);

            CurrentHopLimit = reader.ReadByte();
            var bits = reader.ReadByte();
            ManagedAddressConfiguration = Convert.ToBoolean((byte)(bits >> 7));
            OtherStatefulConfiguration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            HomeAgent = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            DefaultRouterPreference = (DefaultRouterPreference)(byte)((byte)(bits << 3) >> 6);
            NeighborDiscoveryProxy = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            Reserved = (byte)((byte)(bits << 6) >> 6);
            RouterLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ReachableTime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            RetransmissionTimer = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SetOptions(reader, (ushort)(DataLength - 12));
        }
    }

    /// <summary>
    /// Neighbor Solicitation Options ICMP6 header (RFC 4861, RFC 3122).
    /// </summary>
    public sealed class NeighborSolicitationOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the target IP6 address.
        /// </summary>
        public IPAddress TargetAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeighborSolicitationOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="headerType">One of neighbor header types, excluding neighbor advertisement.</param>
        public NeighborSolicitationOptionsIcmp6Header(BinaryReader reader, Icmp6HeaderType headerType) : base(reader, headerType)
        {
            Contract.Requires(reader != null);
            Contract.Requires(headerType == Icmp6HeaderType.NeighborSolicitation || headerType == Icmp6HeaderType.InverseNeighborSolicitation || headerType == Icmp6HeaderType.InverseNeighborAdvertisement);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            TargetAddress = new IPAddress(reader.ReadBytes(16));
            SetOptions(reader, (ushort)(DataLength - 20));
        }
    }

    /// <summary>
    /// Neighbor Advertisement Options ICMP6 header (RFC 4861).
    /// </summary>
    public sealed class NeighborAdvertisementOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool Router { get; private set; }

        /// <summary>
        /// Gets a value indicating whether S flag is set.
        /// </summary>
        public bool Solicited { get; private set; }

        /// <summary>
        /// Gets a value indicating whether O flag is set.
        /// </summary>
        public bool Override { get; private set; }

        /// <summary>
        /// Gets the reserved 29 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the target IP6 address.
        /// </summary>
        public IPAddress TargetAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeighborAdvertisementOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NeighborAdvertisementOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.NeighborAdvertisement)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            Router = Convert.ToBoolean((byte)(bits >> 7));
            Solicited = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Override = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 3) >> 3), reader.ReadByte(), reader.ReadByte(), reader.ReadByte() });
            TargetAddress = new IPAddress(reader.ReadBytes(16));
            SetOptions(reader, (ushort)(DataLength - 20));
        }
    }

    /// <summary>
    /// Redirect Options ICMP6 header (RFC 4861).
    /// </summary>
    public sealed class RedirectOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the target IP6 address.
        /// </summary>
        public IPAddress TargetAddress { get; private set; }

        /// <summary>
        /// Gets the destination IP6 address.
        /// </summary>
        public IPAddress DestinationAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedirectOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RedirectOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.Redirect)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            TargetAddress = new IPAddress(reader.ReadBytes(16));
            DestinationAddress = new IPAddress(reader.ReadBytes(16));
            SetOptions(reader, (ushort)(DataLength - 36));
        }
    }

    /// <summary>
    /// Multicast Listener Report Version 2 Options ICMP6 header (RFC 3810).
    /// </summary>
    public sealed class MulticastListenerReportV2OptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Multicast Address Record class.
        /// </summary>
        public sealed class MulticastAddressRecord
        {
            /// <summary>
            /// Gets the record type.
            /// </summary>
            public MulticastAddressRecordType RecordType { get; private set; }

            /// <summary>
            /// Gets the <see cref="AuxiliaryData"/> length in 4-byte units.
            /// </summary>
            public byte AuxiliaryDataLength { get; private set; }

            /// <summary>
            /// Gets N, the number of the <see cref="SourceAddresses"/>.
            /// </summary>
            public ushort NumberOfSources { get; private set; }

            /// <summary>
            /// Gets the multicast IP6 address.
            /// </summary>
            public IPAddress MulticastAddress { get; private set; }

            /// <summary>
            /// Gets the list of source unicast IP6 addresses.
            /// </summary>
            public ReadOnlyCollection<IPAddress> SourceAddresses { get; private set; }

            /// <summary>
            /// Gets the auxiliary data.
            /// </summary>
            public ReadOnlyCollection<byte> AuxiliaryData { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="MulticastAddressRecord"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            public MulticastAddressRecord(BinaryReader reader)
            {
                Contract.Requires(reader != null);

                RecordType = (MulticastAddressRecordType)reader.ReadByte();
                AuxiliaryDataLength = reader.ReadByte();
                NumberOfSources = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
                MulticastAddress = new IPAddress(reader.ReadBytes(16));
                var sources = new IPAddress[NumberOfSources];
                for(int i = 0, j = sources.Length; i < j; i++)
                    sources[i] = new IPAddress(reader.ReadBytes(16));
                SourceAddresses = new ReadOnlyCollection<IPAddress>(sources);
                AuxiliaryData = new ReadOnlyCollection<byte>(reader.ReadBytes(AuxiliaryDataLength * 4));
            }
        }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets M, the number of the <see cref="MulticastAddressRecords"/>.
        /// </summary>
        public ushort NumberOfMulticastAddressRecords { get; private set; }

        /// <summary>
        /// Gets the list of multicast address records.
        /// </summary>
        public ReadOnlyCollection<MulticastAddressRecord> MulticastAddressRecords { get; private set; }

        /// <summary>
        /// Gets the additional data.
        /// </summary>
        public ReadOnlyCollection<byte> AdditionalData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MulticastListenerReportV2OptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MulticastListenerReportV2OptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.MulticastListenerReportV2)
        {
            Contract.Requires(reader != null);

            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            NumberOfMulticastAddressRecords = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var records = new MulticastAddressRecord[NumberOfMulticastAddressRecords];
            int length = 4;
            for(int i = 0, j = records.Length; i < j; i++)
            {
                var r = new MulticastAddressRecord(reader);
                records[i] = r;
                length += 20 + (r.NumberOfSources * 16) + (r.AuxiliaryDataLength * 4);
            }
            MulticastAddressRecords = new ReadOnlyCollection<MulticastAddressRecord>(records);
            AdditionalData = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - length));
        }
    }

    /// <summary>
    /// Mobile Prefix Solicitation Options ICMP6 header (RFC 6275).
    /// </summary>
    public sealed class MobilePrefixSolicitationOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobilePrefixSolicitationOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MobilePrefixSolicitationOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.MobilePrefixSolicitation)
        {
            Contract.Requires(reader != null);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            SetOptions(reader, (ushort)(DataLength - 4));
        }
    }

    /// <summary>
    /// Mobile Prefix Advertisement Options ICMP6 header (RFC 6275).
    /// </summary>
    public sealed class MobilePrefixAdvertisementOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets a value indicating whether M flag is set.
        /// </summary>
        public bool ManagedAddressConfiguration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether O flag is set.
        /// </summary>
        public bool OtherStatefulConfiguration { get; private set; }

        /// <summary>
        /// Gets the reserved 14 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MobilePrefixAdvertisementOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MobilePrefixAdvertisementOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.MobilePrefixAdvertisement)
        {
            Contract.Requires(reader != null);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            ManagedAddressConfiguration = Convert.ToBoolean((byte)(bits >> 7));
            OtherStatefulConfiguration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 2) >> 2), reader.ReadByte() });
            SetOptions(reader, (ushort)(DataLength - 4));
        }
    }

    /// <summary>
    /// Certification Path Solicitation Options ICMP6 header (RFC 3971).
    /// </summary>
    public sealed class CertificationPathSolicitationOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets the component corresponding to the certificate that the receiver wants to retrieve.
        /// </summary>
        public ushort Component { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificationPathSolicitationOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CertificationPathSolicitationOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.CertificationPathSolicitation)
        {
            Contract.Requires(reader != null);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Component = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetOptions(reader, (ushort)(DataLength - 4));
        }
    }

    /// <summary>
    /// Certification Path Advertisement Options ICMP6 header (RFC 3971).
    /// </summary>
    public sealed class CertificationPathAdvertisementOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets the number of all components, used to inform the receiver of the number of certificates in the entire path.
        /// </summary>
        public ushort AllComponents { get; private set; }

        /// <summary>
        /// Gets the component used to inform the receiver which certificate is being sent.
        /// </summary>
        public ushort Component { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificationPathAdvertisementOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public CertificationPathAdvertisementOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.CertificationPathAdvertisement)
        {
            Contract.Requires(reader != null);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            AllComponents = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Component = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            SetOptions(reader, (ushort)(DataLength - 8));
        }
    }

    /// <summary>
    /// Constants that represent fast mobility subtypes.
    /// </summary>
    public enum FastMobilitySubType
    {
        /// <summary>
        /// Router Solicitation for Proxy
        /// </summary>
        RtSolPr = 2,

        /// <summary>
        /// Proxy Router Advertisement
        /// </summary>
        PrRtAdv = 3,

        /// <summary>
        /// Handover Initiate
        /// </summary>
        HI = 4,

        /// <summary>
        /// Handover Acknowledge
        /// </summary>
        HAck = 5
    }

    /// <summary>
    /// Fast Mobility Options ICMP6 header (RFC 5568).
    /// </summary>
    public sealed class FastMobilityOptionsIcmp6Header : OptionsIcmp6Header
    {
        /// <summary>
        /// Gets the sub type.
        /// </summary>
        public FastMobilitySubType SubType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool OnlyReactiveFastHandoverMode { get; private set; }

        /// <summary>
        /// Gets the reserved 7 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FastMobilityOptionsIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public FastMobilityOptionsIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.FastMobility)
        {
            Contract.Requires(reader != null);

            SubType = (FastMobilitySubType)reader.ReadByte();
            var bits = reader.ReadByte();
            OnlyReactiveFastHandoverMode = Convert.ToBoolean((byte)(bits >> 7));
            Reserved = (byte)((byte)(bits << 1) >> 1);
            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetOptions(reader, (ushort)(DataLength - 4));
        }
    }
}