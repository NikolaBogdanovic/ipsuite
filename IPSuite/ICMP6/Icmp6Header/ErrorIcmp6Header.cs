﻿namespace IPSuite.ICMP6
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;

    using IPSuite.ICMP4;
    using IPSuite.IP6;

    /// <summary>
    /// Error ICMP6 header (RFC 4443).
    /// </summary>
    public abstract class ErrorIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the IP6 header.
        /// </summary>
        public IP6Header IP6Header { get; private set; }

        /// <summary>
        /// Gets the error data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="headerType">One of error header types.</param>
        protected ErrorIcmp6Header(BinaryReader reader, Icmp6HeaderType headerType) : base(reader, headerType)
        {
            Contract.Requires(reader != null);
        }

        /// <summary>
        /// Sets the IP6 header.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        protected void SetIP6Header(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            IP6Header = new IP6Header(reader);
            Data = new ReadOnlyCollection<byte>(reader.ReadBytes((int)(DataLength - 4 - 40 - IP6Header.ExtensionsLength)));
        }
    }

    /// <summary>
    /// Constants that represent destination unreachable codes.
    /// </summary>
    public enum DestinationUnreachableCode
    {
        /// <summary>
        /// No route to destination.
        /// </summary>
        NoRouteToDestination = 0,

        /// <summary>
        /// Communication with destination administratively prohibited.
        /// </summary>
        CommunicationWithDestinationAdministrativelyProhibited = 1,

        /// <summary>
        /// Beyond scope of source address.
        /// </summary>
        BeyondScopeOfSourceAddress = 2,

        /// <summary>
        /// Address unreachable.
        /// </summary>
        AddressUnreachable = 3,

        /// <summary>
        /// Port unreachable.
        /// </summary>
        PortUnreachable = 4,

        /// <summary>
        /// Source address failed ingress or egress policy.
        /// </summary>
        SourceAddressFailedIngressOrEgressPolicy = 5,

        /// <summary>
        /// Reject route to destination.
        /// </summary>
        RejectRouteToDestination = 6,

        /// <summary>
        /// Error in source routing header.
        /// </summary>
        ErrorInSourceRoutingHeader = 7
    }

    /// <summary>
    /// Destination Unreachable Error ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class DestinationUnreachableErrorIcmp6Header : ErrorIcmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new DestinationUnreachableCode Code { get; private set; }

        /// <summary>
        /// Gets the unused 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Gets the next hop maximum transmission unit in bytes.
        /// </summary>
        public ushort NextHopMtu { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationUnreachableErrorIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationUnreachableErrorIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.DestinationUnreachable)
        {
            Contract.Requires(reader != null);

            Code = (DestinationUnreachableCode)base.Code;
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            NextHopMtu = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SetIP6Header(reader);
        }
    }

    /// <summary>
    /// Packet Too Big Error ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class PacketTooBigErrorIcmp6Header : ErrorIcmp6Header
    {
        /// <summary>
        /// Gets the next hop maximum transmission unit in bytes.
        /// </summary>
        public uint Mtu { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PacketTooBigErrorIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public PacketTooBigErrorIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.PacketTooBig)
        {
            Contract.Requires(reader != null);

            Mtu = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SetIP6Header(reader);
        }
    }

    /// <summary>
    /// Time Exceeded Error ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class TimeExceededErrorIcmp6Header : ErrorIcmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new TimeExceededCode Code { get; private set; }

        /// <summary>
        /// Gets the unused 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeExceededErrorIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public TimeExceededErrorIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.TimeExceeded)
        {
            Contract.Requires(reader != null);

            Code = (TimeExceededCode)base.Code;
            Unused = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            SetIP6Header(reader);
        }
    }

    /// <summary>
    /// Constants that represent parameter problem codes.
    /// </summary>
    public enum ParameterProblemCode
    {
        /// <summary>
        /// Erroneous header field.
        /// </summary>
        ErroneousHeaderField = 0,

        /// <summary>
        /// Unrecognized next header type.
        /// </summary>
        UnrecognizedNextHeaderType = 1,

        /// <summary>
        /// Unrecognized IP6 option.
        /// </summary>
        UnrecognizedIP6Option = 2
    }

    /// <summary>
    /// Parameter Problem Error ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class ParameterProblemErrorIcmp6Header : ErrorIcmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new ParameterProblemCode Code { get; private set; }

        /// <summary>
        /// Gets the pointer to the byte where an error was detected.
        /// </summary>
        public uint Pointer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterProblemErrorIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ParameterProblemErrorIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.ParameterProblem)
        {
            Contract.Requires(reader != null);

            Code = (ParameterProblemCode)base.Code;
            Pointer = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SetIP6Header(reader);
        }
    }
}