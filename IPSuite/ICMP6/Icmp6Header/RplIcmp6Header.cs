﻿namespace IPSuite.ICMP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;
    using System.Numerics;

    /// <summary>
    /// Constants that represent RPL ICMP6 header codes.
    /// </summary>
    public enum RplIcmp6HeaderCode
    {
        /// <summary>
        /// DODAG Information Object Solicitation (RFC 6550)
        /// </summary>
        DIS = 0,

        /// <summary>
        /// DODAG Information Object Acknowledgment (RFC 6550)
        /// </summary>
        DIO = 1,

        /// <summary>
        /// Destination Advertisement Object Solicitation (RFC 6550)
        /// </summary>
        DAO = 2,

        /// <summary>
        /// Destination Advertisement Object Acknowledgment (RFC 6550)
        /// </summary>
        DAO_ACK = 3,

        /// <summary>
        /// Secure DODAG Information Object Solicitation (RFC 6550)
        /// </summary>
        Secure_DIS = 128,

        /// <summary>
        /// Secure DODAG Information Object Acknowledgment (RFC 6550)
        /// </summary>
        Secure_DIO = 129,

        /// <summary>
        /// Secure Destination Advertisement Object Solicitation (RFC 6550)
        /// </summary>
        Secure_DAO = 130,

        /// <summary>
        /// Secure Destination Advertisement Object Acknowledgment (RFC 6550)
        /// </summary>
        Secure_DAO_ACK = 131,

        /// <summary>
        /// Consistency Check (RFC 6550)
        /// </summary>
        CC = 138
    }

    /// <summary>
    /// RPL ICMP6 header (RFC 6550). 
    /// </summary>
    public abstract class RplIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new RplIcmp6HeaderCode Code { get; private set; }

        /// <summary>
        /// Gets the list of RPL options.
        /// </summary>
        public ReadOnlyCollection<RplIcmp6Option> RplOptions { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="code">The RPL code.</param>
        protected RplIcmp6Header(BinaryReader reader, RplIcmp6HeaderCode code) : base(reader, Icmp6HeaderType.RPL)
        {
            Contract.Requires(reader != null);

            Code = code;
        }

        /// <summary>
        /// Sets the RPL options.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="length">The options length.</param>
        protected void SetRplOptions(BinaryReader reader, ushort length)
        {
            Contract.Requires(reader != null);

            var opts = new List<RplIcmp6Option>();
            int index = 0;
            while(index < length)
            {
                var o = RplIcmp6Option.Create(reader);
                opts.Add(o);
                if(o.RplType == RplIcmp6OptionType.Pad1)
                    index += 1;
                else
                    index += 2 + o.DataLength;
            }
            RplOptions = new ReadOnlyCollection<RplIcmp6Option>(opts.ToArray());
        }

        /// <summary>
        /// Creates a RPL ICMP6 header.
        /// </summary>
        /// <param name="buffer">The binary buffer.</param>
        /// <returns>
        /// A RPL ICMP6 header.
        /// </returns>
        public static new RplIcmp6Header Create(byte[] buffer)
        {
            Contract.Requires(buffer != null);
            Contract.Requires(buffer.Length >= 4);

            using(var ms = new MemoryStream(buffer, false))
            {
                var reader = new BinaryReader(ms);
                var code = (RplIcmp6HeaderCode)reader.ReadByte();
                reader.BaseStream.Position -= 1;
                switch(code)
                {
                    case RplIcmp6HeaderCode.DIS:
                        return new DodagInformationObjectSolicitationRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.DIO:
                        return new DodagInformationObjectAcknowledgmentRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.DAO:
                        return new DestinationAdvertisementObjectSolicitationRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.DAO_ACK:
                        return new DestinationAdvertisementObjectAcknowledgmentRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.Secure_DIS:
                        return new DodagInformationObjectSolicitationSecureRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.Secure_DIO:
                        return new DodagInformationObjectAcknowledgmentSecureRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.Secure_DAO:
                        return new DestinationAdvertisementObjectSolicitationSecureRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.Secure_DAO_ACK:
                        return new DestinationAdvertisementObjectAcknowledgmentSecureRplIcmp6Header(reader);
                    case RplIcmp6HeaderCode.CC:
                        return new ConsistencyCheckRplIcmp6Header(reader);
                    default:
                        return new GenericRplIcmp6Header(reader, code);
                }
            }
        }
    }

    /// <summary>
    /// Generic RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class GenericRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="code">The RPL code.</param>
        public GenericRplIcmp6Header(BinaryReader reader, RplIcmp6HeaderCode code) : base(reader, code)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }

    /// <summary>
    /// DODAG Information Object Solicitation RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DodagInformationObjectSolicitationRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DodagInformationObjectSolicitationRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DodagInformationObjectSolicitationRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.DIS)
        {
            Contract.Requires(reader != null);

            Base = reader.ReadByte();
            Reserved = reader.ReadByte();
            SetRplOptions(reader, (ushort)(DataLength - 2));
        }
    }

    /// <summary>
    /// Constants that represent modes of operation (MOP).
    /// </summary>
    public enum ModeOfOperation
    {
        /// <summary>
        /// No Downward Routes Maintained By RPL
        /// </summary>
        NoDownwardRoutesMaintainedByRPL = 0,

        /// <summary>
        /// Non Storing Mode Of Operation
        /// </summary>
        NonStoringModeOfOperation = 1,

        /// <summary>
        /// Storing Mode Of Operation With No Multicast Support
        /// </summary>
        StoringModeOfOperationWithNoMulticastSupport = 2,

        /// <summary>
        /// Storing Mode Of Operation With Multicast Support
        /// </summary>
        StoringModeOfOperationWithMulticastSupport = 3
    }

    /// <summary>
    /// DODAG Information Object Acknowledgment RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DodagInformationObjectAcknowledgmentRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets the version number.
        /// </summary>
        public byte VersionNumber { get; private set; }

        /// <summary>
        /// Gets the rank.
        /// </summary>
        public ushort Rank { get; private set; }

        /// <summary>
        /// Gets a value indicating whether G flag is set.
        /// </summary>
        public bool Grounded { get; private set; }

        /// <summary>
        /// Gets a value indicating whether 0 flag is set.
        /// </summary>
        public bool Zero { get; private set; }

        /// <summary>
        /// Gets MOP, the mode of operation.
        /// </summary>
        public ModeOfOperation ModeOfOperation { get; private set; }

        /// <summary>
        /// Gets the DODAG preference.
        /// </summary>
        public byte Preference { get; private set; }

        /// <summary>
        /// Gets DTSN.
        /// </summary>
        public byte DestinationAdvertisementTriggerSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier as an IP6 address.
        /// </summary>
        public IPAddress DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DodagInformationObjectAcknowledgmentRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DodagInformationObjectAcknowledgmentRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.DIO)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            VersionNumber = reader.ReadByte();
            Rank = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            Grounded = Convert.ToBoolean((byte)(bits >> 7));
            Zero = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            ModeOfOperation = (ModeOfOperation)(byte)((byte)(bits << 2) >> 5);
            Preference = (byte)((byte)(bits << 5) >> 5);
            DestinationAdvertisementTriggerSequenceNumber = reader.ReadByte();
            Base = reader.ReadByte();
            Reserved = reader.ReadByte();
            DodagId = new IPAddress(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 24));
        }
    }

    /// <summary>
    /// Destination Advertisement Object Solicitation RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DestinationAdvertisementObjectSolicitationRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether K flag is set.
        /// </summary>
        public bool Acknowledgment { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool DodagIdPresent { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets DOSN.
        /// </summary>
        public byte DestinationAdvertisementObjectSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationAdvertisementObjectSolicitationRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationAdvertisementObjectSolicitationRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.DAO)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            Acknowledgment = Convert.ToBoolean((byte)(bits >> 7));
            DodagIdPresent = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Base = (byte)((byte)(bits << 2) >> 2);
            Reserved = reader.ReadByte();
            DestinationAdvertisementObjectSequenceNumber = reader.ReadByte();
            if(DodagIdPresent)
                DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 4 - (DodagIdPresent ? 16 : 0)));
        }
    }

    /// <summary>
    /// Constants that represent destination advertisement object acknowledgment statuses.
    /// </summary>
    public enum DestinationAdvertisementObjectAcknowledgmentStatus
    {
        /// <summary>
        /// Unqualified Acceptance
        /// </summary>
        UnqualifiedAcceptance = 0,

        /// <summary>
        /// Not An Outright Rejection
        /// </summary>
        NotAnOutrightRejection = 1,

        /// <summary>
        /// Rejection
        /// </summary>
        Rejection = 128
    }

    /// <summary>
    /// Destination Advertisement Object Acknowledgment RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DestinationAdvertisementObjectAcknowledgmentRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool DodagIdPresent { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets DOSN.
        /// </summary>
        public byte DestinationAdvertisementObjectSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public DestinationAdvertisementObjectAcknowledgmentStatus Status { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationAdvertisementObjectAcknowledgmentRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationAdvertisementObjectAcknowledgmentRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.DAO_ACK)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            DodagIdPresent = Convert.ToBoolean((byte)(bits >> 7));
            Base = (byte)((byte)(bits << 1) >> 1);
            DestinationAdvertisementObjectSequenceNumber = reader.ReadByte();
            Status = (DestinationAdvertisementObjectAcknowledgmentStatus)reader.ReadByte();
            if(DodagIdPresent)
                DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 4 - (DodagIdPresent ? 16 : 0)));
        }
    }

    /// <summary>
    /// Consistency Check RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class ConsistencyCheckRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool Response { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the CC nonce.
        /// </summary>
        public ushort ConsistencyCheckNonce { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Gets the destination counter.
        /// </summary>
        public uint DestinationCounter { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsistencyCheckRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ConsistencyCheckRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.CC)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            Response = Convert.ToBoolean((byte)(bits >> 7));
            Base = (byte)((byte)(bits << 1) >> 1);
            ConsistencyCheckNonce = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            DestinationCounter = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SetRplOptions(reader, (ushort)(DataLength - 24));
        }
    }

    /// <summary>
    /// Constants that represent security algorithms.
    /// </summary>
    public enum SecurityAlgorithm
    {
        /// <summary>
        /// CCM with AES-128 Encryption/MAC, or RSA with SHA-256 Signature.
        /// </summary>
        Algorithm0 = 0
    }

    /// <summary>
    /// Constants that represent key identifier modes (KIM).
    /// </summary>
    public enum KeyIdentifierMode
    {
        /// <summary>
        /// Group key used.
        /// </summary>
        Mode0 = 0,

        /// <summary>
        /// Per-pair key used.
        /// </summary>
        Mode1 = 1,

        /// <summary>
        /// Group key used.
        /// </summary>
        Mode2 = 2,

        /// <summary>
        /// Node's signature key used.
        /// </summary>
        Mode3 = 3
    }

    /// <summary>
    /// Constants that represent security levels.
    /// </summary>
    public enum SecurityLevel
    {
        /// <summary>
        /// MAC-32 length = 4 bytes, or Sign-3072 length = 384 bytes.
        /// </summary>
        Level0 = 0,

        /// <summary>
        /// ENC-MAC-32 length = 4 bytes, or ENC-Sign-3072 length = 384 bytes.
        /// </summary>
        Level1 = 1,

        /// <summary>
        /// MAC-64 length = 8 bytes, or Sign-2048 length = 256 bytes.
        /// </summary>
        Level2 = 2,

        /// <summary>
        /// ENC-MAC-64 length = 8 bytes, or ENC-Sign-2048 length = 256 bytes.
        /// </summary>
        Level3 = 3
    }

    /// <summary>
    /// SecureRPL ICMP6 header (RFC 6550).
    /// </summary>
    public abstract class SecureRplIcmp6Header : RplIcmp6Header
    {
        /// <summary>
        /// Gets a value indicating whether T flag is set.
        /// </summary>
        public bool CounterIsTime { get; private set; }

        /// <summary>
        /// Gets the reserved 7 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the security algorithm.
        /// </summary>
        public SecurityAlgorithm SecurityAlgorithm { get; private set; }

        /// <summary>
        /// Gets KIM, the key identifier mode.
        /// </summary>
        public KeyIdentifierMode KeyIdentifierMode { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved2 { get; private set; }

        /// <summary>
        /// Gets the security level.
        /// </summary>
        public SecurityLevel SecurityLevel { get; private set; }

        /// <summary>
        /// Gets the security object.
        /// </summary>
        public byte Security { get; private set; }

        /// <summary>
        /// Gets the counter.
        /// </summary>
        public uint Counter { get; private set; }

        /// <summary>
        /// Gets the key identifier length in bytes.
        /// </summary>
        public byte KeyIdentifierLength { get; private set; }

        /// <summary>
        /// Gets the key identifier source.
        /// </summary>
        public ReadOnlyCollection<byte> KeyIdentifierSource { get; private set; }

        /// <summary>
        /// Gets the key identifier index.
        /// </summary>
        public byte KeyIdentifierIndex { get; private set; }

        /// <summary>
        /// Gets the padding.
        /// </summary>
        public ReadOnlyCollection<byte> Padding { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="code">The RPL code.</param>
        protected SecureRplIcmp6Header(BinaryReader reader, RplIcmp6HeaderCode code) : base(reader, code)
        {
            Contract.Requires(reader != null);

            var bits = reader.ReadByte();
            CounterIsTime = Convert.ToBoolean((byte)(bits >> 7));
            Reserved = (byte)((byte)(bits << 1) >> 1);
            SecurityAlgorithm = (SecurityAlgorithm)reader.ReadByte();
            bits = reader.ReadByte();
            KeyIdentifierMode = (KeyIdentifierMode)(byte)(bits >> 6);
            Reserved2 = (byte)((byte)(bits << 2) >> 5);
            SecurityLevel = (SecurityLevel)(byte)((byte)(bits << 5) >> 5);
            Security = reader.ReadByte();
            Counter = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            if(KeyIdentifierMode == KeyIdentifierMode.Mode2 || (KeyIdentifierMode == KeyIdentifierMode.Mode3 && (SecurityLevel == SecurityLevel.Level1 || SecurityLevel == SecurityLevel.Level3)))
            {
                KeyIdentifierLength += 8;
                KeyIdentifierSource = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
            }
            if(KeyIdentifierMode == KeyIdentifierMode.Mode0 || KeyIdentifierMode == KeyIdentifierMode.Mode2 || (KeyIdentifierMode == KeyIdentifierMode.Mode3 && (SecurityLevel == SecurityLevel.Level1 || SecurityLevel == SecurityLevel.Level3)))
            {
                KeyIdentifierLength += 1;
                KeyIdentifierIndex = reader.ReadByte();
            }
            Padding = new ReadOnlyCollection<byte>(reader.ReadBytes(KeyIdentifierLength % 4));
        }
    }

    /// <summary>
    /// DODAG Information Object Solicitation Secure RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DodagInformationObjectSolicitationSecureRplIcmp6Header : SecureRplIcmp6Header
    {
        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved3 { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DodagInformationObjectSolicitationSecureRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DodagInformationObjectSolicitationSecureRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.Secure_DIS)
        {
            Contract.Requires(reader != null);

            Base = reader.ReadByte();
            Reserved3 = reader.ReadByte();
            SetRplOptions(reader, (ushort)(DataLength - 8 - KeyIdentifierLength - Padding.Count - 2));
        }
    }

    /// <summary>
    /// DODAG Information Object Acknowledgment Secure RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DodagInformationObjectAcknowledgmentSecureRplIcmp6Header : SecureRplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets the version number.
        /// </summary>
        public byte VersionNumber { get; private set; }

        /// <summary>
        /// Gets the rank.
        /// </summary>
        public ushort Rank { get; private set; }

        /// <summary>
        /// Gets a value indicating whether G flag is set.
        /// </summary>
        public bool Grounded { get; private set; }

        /// <summary>
        /// Gets a value indicating whether 0 flag is set.
        /// </summary>
        public bool Zero { get; private set; }

        /// <summary>
        /// Gets MOP, the mode of operation.
        /// </summary>
        public ModeOfOperation ModeOfOperation { get; private set; }

        /// <summary>
        /// Gets the DODAG preference.
        /// </summary>
        public byte Preference { get; private set; }

        /// <summary>
        /// Gets DTSN.
        /// </summary>
        public byte DestinationAdvertisementTriggerSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved3 { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier as an IP6 address.
        /// </summary>
        public IPAddress DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DodagInformationObjectAcknowledgmentSecureRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DodagInformationObjectAcknowledgmentSecureRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.Secure_DIO)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            VersionNumber = reader.ReadByte();
            Rank = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            Grounded = Convert.ToBoolean((byte)(bits >> 7));
            Zero = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            ModeOfOperation = (ModeOfOperation)(byte)((byte)(bits << 2) >> 5);
            Preference = (byte)((byte)(bits << 5) >> 5);
            DestinationAdvertisementTriggerSequenceNumber = reader.ReadByte();
            Base = reader.ReadByte();
            Reserved3 = reader.ReadByte();
            DodagId = new IPAddress(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 8 - KeyIdentifierLength - Padding.Count - 24));
        }
    }

    /// <summary>
    /// Destination Advertisement Object Solicitation Secure RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DestinationAdvertisementObjectSolicitationSecureRplIcmp6Header : SecureRplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether K flag is set.
        /// </summary>
        public bool Acknowledgment { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool DodagIdPresent { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved3 { get; private set; }

        /// <summary>
        /// Gets DOSN.
        /// </summary>
        public byte DestinationAdvertisementObjectSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationAdvertisementObjectSolicitationSecureRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationAdvertisementObjectSolicitationSecureRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.Secure_DAO)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            Acknowledgment = Convert.ToBoolean((byte)(bits >> 7));
            DodagIdPresent = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            Base = (byte)((byte)(bits << 2) >> 2);
            Reserved3 = reader.ReadByte();
            DestinationAdvertisementObjectSequenceNumber = reader.ReadByte();
            if(DodagIdPresent)
                DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 8 - KeyIdentifierLength - Padding.Count - 4 - (DodagIdPresent ? 16 : 0)));
        }
    }

    /// <summary>
    /// Destination Advertisement Object Acknowledgment Secure RPL ICMP6 header (RFC 6550).
    /// </summary>
    public sealed class DestinationAdvertisementObjectAcknowledgmentSecureRplIcmp6Header : SecureRplIcmp6Header
    {
        /// <summary>
        /// Gets the RPL instance identifier.
        /// </summary>
        public byte RplInstanceId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether D flag is set.
        /// </summary>
        public bool DodagIdPresent { get; private set; }

        /// <summary>
        /// Gets the base object.
        /// </summary>
        public byte Base { get; private set; }

        /// <summary>
        /// Gets DOSN.
        /// </summary>
        public byte DestinationAdvertisementObjectSequenceNumber { get; private set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public DestinationAdvertisementObjectAcknowledgmentStatus Status { get; private set; }

        /// <summary>
        /// Gets the DODAG identifier.
        /// </summary>
        public BigInteger DodagId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationAdvertisementObjectAcknowledgmentSecureRplIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DestinationAdvertisementObjectAcknowledgmentSecureRplIcmp6Header(BinaryReader reader) : base(reader, RplIcmp6HeaderCode.Secure_DAO_ACK)
        {
            Contract.Requires(reader != null);

            RplInstanceId = reader.ReadByte();
            var bits = reader.ReadByte();
            DodagIdPresent = Convert.ToBoolean((byte)(bits >> 7));
            Base = (byte)((byte)(bits << 1) >> 1);
            DestinationAdvertisementObjectSequenceNumber = reader.ReadByte();
            Status = (DestinationAdvertisementObjectAcknowledgmentStatus)reader.ReadByte();
            if(DodagIdPresent)
                DodagId = NetworkToHostOrder.ToUInt128(reader.ReadBytes(16));
            SetRplOptions(reader, (ushort)(DataLength - 8 - KeyIdentifierLength - Padding.Count - 4 - (DodagIdPresent ? 16 : 0)));
        }
    }
}