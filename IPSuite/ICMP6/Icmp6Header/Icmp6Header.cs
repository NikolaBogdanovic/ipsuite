﻿namespace IPSuite.ICMP6
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;

    using IPSuite.DNS;
    using IPSuite.ICMP4;

    /// <summary>
    /// Constants that represent ICMP6 header types.
    /// </summary>
    public enum Icmp6HeaderType
    {
        /// <summary>
        /// Destination Unreachable (RFC 4443)
        /// </summary>
        DestinationUnreachable = 1,

        /// <summary>
        /// Packet Too Big (RFC 4443)
        /// </summary>
        PacketTooBig = 2,

        /// <summary>
        /// Time Exceeded (RFC 4443)
        /// </summary>
        TimeExceeded = 3,

        /// <summary>
        /// Parameter Problem (RFC 4443)
        /// </summary>
        ParameterProblem = 4,

        /// <summary>
        /// Echo Request (RFC 4443)
        /// </summary>
        EchoRequest = 128,

        /// <summary>
        /// Echo Reply (RFC 4443)
        /// </summary>
        EchoReply = 129,

        /// <summary>
        /// Multicast Listener Query (RFC 2710)
        /// </summary>
        MulticastListenerQuery = 130,

        /// <summary>
        /// Multicast Listener Report (RFC 2710)
        /// </summary>
        MulticastListenerReport = 131,

        /// <summary>
        /// Multicast Listener Done (RFC 2710)
        /// </summary>
        MulticastListenerDone = 132,

        /// <summary>
        /// Router Solicitation (RFC 4861)
        /// </summary>
        RouterSolicitation = 133,

        /// <summary>
        /// Router Advertisement (RFC 4861)
        /// </summary>
        RouterAdvertisement = 134,

        /// <summary>
        /// Neighbor Solicitation (RFC 4861)
        /// </summary>
        NeighborSolicitation = 135,

        /// <summary>
        /// Neighbor Advertisement (RFC 4861)
        /// </summary>
        NeighborAdvertisement = 136,

        /// <summary>
        /// Redirect (RFC 4861)
        /// </summary>
        Redirect = 137,

        /// <summary>
        /// Router Renumbering (RFC 2894)
        /// </summary>
        RouterRenumbering = 138,

        /// <summary>
        /// Node Information Query (RFC 4620)
        /// </summary>
        NodeInformationQuery = 139,

        /// <summary>
        /// Node Information Reply (RFC 4620)
        /// </summary>
        NodeInformationReply = 140,

        /// <summary>
        /// Inverse Neighbor Solicitation (RFC 3122)
        /// </summary>
        InverseNeighborSolicitation = 141,

        /// <summary>
        /// Inverse Neighbor Advertisement (RFC 3122)
        /// </summary>
        InverseNeighborAdvertisement = 142,

        /// <summary>
        /// Multicast Listener Report V2 (RFC 3810)
        /// </summary>
        MulticastListenerReportV2 = 143,

        /// <summary>
        /// Home Agent Address Request (RFC 6275)
        /// </summary>
        HomeAgentAddressRequest = 144,

        /// <summary>
        /// Home Agent Address Reply (RFC 6275)
        /// </summary>
        HomeAgentAddressReply = 145,

        /// <summary>
        /// Mobile Prefix Solicitation (RFC 6275)
        /// </summary>
        MobilePrefixSolicitation = 146,

        /// <summary>
        /// Mobile Prefix Advertisement (RFC 6275)
        /// </summary>
        MobilePrefixAdvertisement = 147,

        /// <summary>
        /// Certification Path Solicitation (RFC 3971)
        /// </summary>
        CertificationPathSolicitation = 148,

        /// <summary>
        /// Certification Path Advertisement (RFC 3971)
        /// </summary>
        CertificationPathAdvertisement = 149,

        /// <summary>
        /// Seamoby (RFC 4065)
        /// </summary>
        Seamoby = 150,

        /// <summary>
        /// Multicast Router Advertisement (RFC 4286)
        /// </summary>
        MulticastRouterAdvertisement = 151,

        /// <summary>
        /// Multicast Router Solicitation (RFC 4286)
        /// </summary>
        MulticastRouterSolicitation = 152,

        /// <summary>
        /// Multicast Router Termination (RFC 4286)
        /// </summary>
        MulticastRouterTermination = 153,

        /// <summary>
        /// Fast Mobility (RFC 5568)
        /// </summary>
        FastMobility = 154,

        /// <summary>
        /// Routing Protocol for Low-power and Lossy networks (RFC 6550)
        /// </summary>
        RPL = 155,

        /// <summary>
        /// Locator Update (RFC 6743)
        /// </summary>
        LocatorUpdate = 156,

        /// <summary>
        /// Duplicate Address Request (RFC 6775)
        /// </summary>
        DuplicateAddressRequest = 157,

        /// <summary>
        /// Duplicate Address Confirmation (RFC 6775)
        /// </summary>
        DuplicateAddressConfirmation = 158
    }

    /// <summary>
    /// Constants that represent ICMP6 header classes.
    /// </summary>
    public enum Icmp6HeaderClass
    {
        /// <summary>
        /// Error Message
        /// </summary>
        ErrorMessage = 0,

        /// <summary>
        /// Query Message
        /// </summary>
        QueryMessage = 1
    }

    /// <summary>
    /// Internet Control Message Protocol version 6 header (RFC 4443).
    /// </summary>
    public abstract class Icmp6Header
    {
        /// <summary>
        /// Gets the header type.
        /// </summary>
        public Icmp6HeaderType HeaderType { get; private set; }

        /// <summary>
        /// Gets the header class.
        /// </summary>
        public Icmp6HeaderClass HeaderClass { get; private set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public byte Code { get; private set; }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// Gets the data length in bytes.
        /// </summary>
        public ushort DataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Icmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        protected Icmp6Header(BinaryReader reader, Icmp6HeaderType type)
        {
            Contract.Requires(reader != null);

            HeaderType = type;
            HeaderClass = HeaderType < Icmp6HeaderType.EchoRequest ? Icmp6HeaderClass.ErrorMessage : Icmp6HeaderClass.QueryMessage;
            Code = reader.ReadByte();
            Checksum = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DataLength = (ushort)(reader.BaseStream.Length - 4);
        }

        /// <summary>
        /// Creates an ICMP6 header.
        /// </summary>
        /// <param name="buffer">The binary buffer.</param>
        /// <returns>
        /// An ICMP6 header.
        /// </returns>
        public static Icmp6Header Create(byte[] buffer)
        {
            Contract.Requires(buffer != null);
            Contract.Requires(buffer.Length >= 4);

            using(var ms = new MemoryStream(buffer, false))
            {
                var reader = new BinaryReader(ms);
                var type = (Icmp6HeaderType)reader.ReadByte();
                switch(type)
                {
                    case Icmp6HeaderType.DestinationUnreachable:
                        return new DestinationUnreachableErrorIcmp6Header(reader);
                    case Icmp6HeaderType.PacketTooBig:
                        return new PacketTooBigErrorIcmp6Header(reader);
                    case Icmp6HeaderType.TimeExceeded:
                        return new TimeExceededErrorIcmp6Header(reader);
                    case Icmp6HeaderType.ParameterProblem:
                        return new ParameterProblemErrorIcmp6Header(reader);
                    case Icmp6HeaderType.EchoRequest:
                    case Icmp6HeaderType.EchoReply:
                        return new EchoIcmp6Header(reader, type);
                    case Icmp6HeaderType.MulticastListenerQuery:
                        return new QueryMulticastListenerOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.MulticastListenerReport:
                    case Icmp6HeaderType.MulticastListenerDone:
                        return new MulticastListenerOptionsIcmp6Header(reader, type);
                    case Icmp6HeaderType.RouterSolicitation:
                        return new RouterSolicitationOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.RouterAdvertisement:
                        return new RouterAdvertisementOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.NeighborSolicitation:
                    case Icmp6HeaderType.InverseNeighborSolicitation:
                    case Icmp6HeaderType.InverseNeighborAdvertisement:
                        return new NeighborSolicitationOptionsIcmp6Header(reader, type);
                    case Icmp6HeaderType.NeighborAdvertisement:
                        return new NeighborAdvertisementOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.Redirect:
                        return new RedirectOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.RouterRenumbering:
                        return new RouterRenumberingIcmp6Header(reader);
                    case Icmp6HeaderType.NodeInformationQuery:
                        return QueryNodeInformationIcmp6Header.Create(reader);
                    case Icmp6HeaderType.NodeInformationReply:
                        return ReplyNodeInformationIcmp6Header.Create(reader);
                    case Icmp6HeaderType.MulticastListenerReportV2:
                        return new MulticastListenerReportV2OptionsIcmp6Header(reader);
                    case Icmp6HeaderType.HomeAgentAddressRequest:
                        return new RequestHomeAgentAddressIcmp6Header(reader);
                    case Icmp6HeaderType.HomeAgentAddressReply:
                        return new ReplyHomeAgentAddressIcmp6Header(reader);
                    case Icmp6HeaderType.MobilePrefixSolicitation:
                        return new MobilePrefixSolicitationOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.MobilePrefixAdvertisement:
                        return new MobilePrefixAdvertisementOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.CertificationPathSolicitation:
                        return new CertificationPathSolicitationOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.CertificationPathAdvertisement:
                        return new CertificationPathAdvertisementOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.Seamoby:
                        return new SeamobyIcmp6Header(reader);
                    case Icmp6HeaderType.MulticastRouterAdvertisement:
                        return new MulticastRouterAdvertisementIcmp6Header(reader);
                    case Icmp6HeaderType.FastMobility:
                        return new FastMobilityOptionsIcmp6Header(reader);
                    case Icmp6HeaderType.LocatorUpdate:
                        return new LocatorUpdateIcmp6Header(reader);
                    case Icmp6HeaderType.DuplicateAddressRequest:
                    case Icmp6HeaderType.DuplicateAddressConfirmation:
                        return new DuplicateAddressIcmp6Header(reader, type);
                    default:
                        return new GenericIcmp6Header(reader, type);
                }
            }
        }
    }

    /// <summary>
    /// Generic ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class GenericIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the header data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        public GenericIcmp6Header(BinaryReader reader, Icmp6HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength));
        }
    }
    
    /// <summary>
    /// Echo ICMP6 header (RFC 4443).
    /// </summary>
    public sealed class EchoIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public ushort SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the echo data.
        /// </summary>
        public ReadOnlyCollection<byte> Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EchoIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">One of echo header types.</param>
        public EchoIcmp6Header(BinaryReader reader, Icmp6HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp6HeaderType.EchoRequest || type == Icmp6HeaderType.EchoReply);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SequenceNumber = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Data = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4));
        }
    }

    /// <summary>
    /// Constants that represent router renumbering codes.
    /// </summary>
    public enum RouterRenumberingCode
    {
        /// <summary>
        /// Router Renumbering Command
        /// </summary>
        RouterRenumberingCommand = 0,

        /// <summary>
        /// Router Renumbering Result
        /// </summary>
        RouterRenumberingResult = 1,

        /// <summary>
        /// Sequence Number Reset
        /// </summary>
        SequenceNumberReset = 255
    }

    /// <summary>
    /// Constants that represent prefix operation codes.
    /// </summary>
    public enum PrefixControlOperationCode
    {
        /// <summary>
        /// ADD
        /// </summary>
        ADD = 0,

        /// <summary>
        /// CHANGE
        /// </summary>
        CHANGE = 1,

        /// <summary>
        /// SET-GLOBAL
        /// </summary>
        SET_GLOBAL = 2
    }

    /// <summary>
    /// Router Renumbering ICMP6 header (RFC 2894).
    /// </summary>
    public sealed class RouterRenumberingIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Prefix Control Operation class.
        /// </summary>
        public sealed class PrefixControlOperation
        {
            /// <summary>
            /// Match-Prefix Part class.
            /// </summary>
            public sealed class MatchPrefixPart
            {
                /// <summary>
                /// Gets the operation code.
                /// </summary>
                public PrefixControlOperationCode OperationCode { get; private set; }

                /// <summary>
                /// Gets the operation length in 8-byte units.
                /// </summary>
                public byte OperationLength { get; private set; }

                /// <summary>
                /// Gets the ordinal.
                /// </summary>
                public byte Ordinal { get; private set; }

                /// <summary>
                /// Gets the match length in bits.
                /// </summary>
                public byte MatchLength { get; private set; }

                /// <summary>
                /// Gets the minimum length in bits.
                /// </summary>
                public byte MinLength { get; private set; }

                /// <summary>
                /// Gets the maximum length in bits.
                /// </summary>
                public byte MaxLength { get; private set; }

                /// <summary>
                /// Gets the reserved 16 bits.
                /// </summary>
                public ReadOnlyCollection<byte> Reserved { get; private set; }

                /// <summary>
                /// Gets the IP6 match-prefix.
                /// </summary>
                public ReadOnlyCollection<byte> MatchPrefix { get; private set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="MatchPrefixPart"/> class.
                /// </summary>
                /// <param name="reader">The binary reader.</param>
                public MatchPrefixPart(BinaryReader reader)
                {
                    Contract.Requires(reader != null);

                    OperationCode = (PrefixControlOperationCode)reader.ReadByte();
                    OperationLength = reader.ReadByte();
                    Ordinal = reader.ReadByte();
                    MatchLength = reader.ReadByte();
                    MinLength = reader.ReadByte();
                    MaxLength = reader.ReadByte();
                    Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
                    MatchPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(16));
                }
            }

            /// <summary>
            /// Use-Prefix Part class.
            /// </summary>
            public sealed class UsePrefixPart
            {
                /// <summary>
                /// Gets the use length in bits.
                /// </summary>
                public byte UseLength { get; private set; }

                /// <summary>
                /// Gets the keep length in bits.
                /// </summary>
                public byte KeepLength { get; private set; }

                /// <summary>
                /// Gets the mask.
                /// </summary>
                public byte Mask { get; private set; }

                /// <summary>
                /// Gets a value indicating whether L flag is set.
                /// </summary>
                public bool OnLink { get; private set; }

                /// <summary>
                /// Gets a value indicating whether A flag is set.
                /// </summary>
                public bool AutonomousConfiguration { get; private set; }

                /// <summary>
                /// Gets the reserved 6 bits.
                /// </summary>
                public byte Reserved { get; private set; }

                /// <summary>
                /// Gets the valid lifetime in seconds.
                /// </summary>
                public uint ValidLifetime { get; private set; }

                /// <summary>
                /// Gets the preferred lifetime in seconds.
                /// </summary>
                public uint PreferredLifetime { get; private set; }

                /// <summary>
                /// Gets a value indicating whether V flag is set.
                /// </summary>
                public bool ValidLifetimeDecrement { get; private set; }

                /// <summary>
                /// Gets a value indicating whether P flag is set.
                /// </summary>
                public bool PreferedLifetimeDecrement { get; private set; }

                /// <summary>
                /// Gets the reserved 30 bits.
                /// </summary>
                public ReadOnlyCollection<byte> Reserved2 { get; private set; }

                /// <summary>
                /// Gets the IP6 use-prefix.
                /// </summary>
                public ReadOnlyCollection<byte> UsePrefix { get; private set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="UsePrefixPart"/> class.
                /// </summary>
                /// <param name="reader">The binary reader.</param>
                public UsePrefixPart(BinaryReader reader)
                {
                    Contract.Requires(reader != null);

                    UseLength = reader.ReadByte();
                    KeepLength = reader.ReadByte();
                    Mask = reader.ReadByte();
                    var bits = reader.ReadByte();
                    OnLink = Convert.ToBoolean((byte)(bits >> 7));
                    AutonomousConfiguration = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
                    Reserved = (byte)((byte)(bits << 2) >> 2);
                    ValidLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                    PreferredLifetime = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                    bits = reader.ReadByte();
                    ValidLifetimeDecrement = Convert.ToBoolean((byte)(bits >> 7));
                    PreferedLifetimeDecrement = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
                    Reserved2 = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 2) >> 2), reader.ReadByte(), reader.ReadByte(), reader.ReadByte() });
                    UsePrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(16));
                }
            }

            /// <summary>
            /// Gets the match-prefix part.
            /// </summary>
            public MatchPrefixPart MatchPrefix { get; private set; }

            /// <summary>
            /// Gets the list of use-prefix parts.
            /// </summary>
            public ReadOnlyCollection<UsePrefixPart> UsePrefixes { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="PrefixControlOperation"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            public PrefixControlOperation(BinaryReader reader)
            {
                Contract.Requires(reader != null);

                MatchPrefix = new MatchPrefixPart(reader);
                var uses = new UsePrefixPart[(MatchPrefix.MatchLength - 3) / 4];
                for(int i = 0, j = uses.Length; i < j; i++)
                    uses[i] = new UsePrefixPart(reader);
                UsePrefixes = new ReadOnlyCollection<UsePrefixPart>(uses);
            }
        }

        /// <summary>
        /// Match Report class.
        /// </summary>
        public sealed class MatchReport
        {
            /// <summary>
            /// Gets the reserved 14 bits.
            /// </summary>
            public ReadOnlyCollection<byte> Reserved { get; private set; }

            /// <summary>
            /// Gets a value indicating whether B flag is set.
            /// </summary>
            public bool OutOfBounds { get; private set; }

            /// <summary>
            /// Gets a value indicating whether F flag is set.
            /// </summary>
            public bool ForbiddenPrefixFormat { get; private set; }

            /// <summary>
            /// Gets the ordinal.
            /// </summary>
            public byte Ordinal { get; private set; }

            /// <summary>
            /// Gets the matched length in bits.
            /// </summary>
            public byte MatchedLength { get; private set; }

            /// <summary>
            /// Gets the interface index.
            /// </summary>
            public uint InterfaceIndex { get; private set; }

            /// <summary>
            /// Gets the matched IP6 prefix.
            /// </summary>
            public ReadOnlyCollection<byte> MatchedPrefix { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="MatchReport"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            public MatchReport(BinaryReader reader)
            {
                Contract.Requires(reader != null);

                var bytes = reader.ReadBytes(2);
                Reserved = new ReadOnlyCollection<byte>(new[] { (byte)(bytes[0] >> 2), (byte)((byte)(bytes[0] << 6) + (byte)(bytes[1] >> 2)) });
                OutOfBounds = Convert.ToBoolean((byte)((byte)(bytes[1] << 6) >> 7));
                ForbiddenPrefixFormat = Convert.ToBoolean((byte)((byte)(bytes[1] << 7) >> 7));
                Ordinal = reader.ReadByte();
                MatchedLength = reader.ReadByte();
                InterfaceIndex = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                MatchedPrefix = new ReadOnlyCollection<byte>(reader.ReadBytes(16));
            }
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public new RouterRenumberingCode Code { get; private set; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        public uint SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the segment number.
        /// </summary>
        public byte SegmentNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether T flag is set.
        /// </summary>
        public bool TestCommand { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool ResultRequested { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool AllInterfaces { get; private set; }

        /// <summary>
        /// Gets a value indicating whether S flag is set.
        /// </summary>
        public bool SiteSpecific { get; private set; }

        /// <summary>
        /// Gets a value indicating whether P flag is set.
        /// </summary>
        public bool ProcessedPreviously { get; private set; }

        /// <summary>
        /// Gets the reserved 3 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the maximum delay in milliseconds.
        /// </summary>
        public ushort MaximumDelay { get; private set; }

        /// <summary>
        /// Gets the reserved 32 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved2 { get; private set; }

        /// <summary>
        /// Gets the list of prefix control operations.
        /// </summary>
        public ReadOnlyCollection<PrefixControlOperation> PrefixControlOperations { get; private set; }

        /// <summary>
        /// Gets the list of match reports.
        /// </summary>
        public ReadOnlyCollection<MatchReport> MatchReports { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouterRenumberingIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RouterRenumberingIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.RouterRenumbering)
        {
            Contract.Requires(reader != null);

            Code = (RouterRenumberingCode)base.Code;
            SequenceNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            SegmentNumber = reader.ReadByte();
            var bits = reader.ReadByte();
            TestCommand = Convert.ToBoolean((byte)(bits >> 7));
            ResultRequested = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            AllInterfaces = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            SiteSpecific = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            ProcessedPreviously = Convert.ToBoolean((byte)((byte)(bits << 4) >> 7));
            Reserved = (byte)((byte)(bits << 5) >> 5);
            MaximumDelay = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Reserved2 = new ReadOnlyCollection<byte>(reader.ReadBytes(4));
            switch(Code)
            {
                case RouterRenumberingCode.RouterRenumberingCommand:
                case RouterRenumberingCode.SequenceNumberReset:
                {
                    var ops = new List<PrefixControlOperation>();
                    int i = 0, j = DataLength - 12;
                    while(i < j)
                    {
                        var o = new PrefixControlOperation(reader);
                        ops.Add(o);
                        i += o.MatchPrefix.OperationLength * 8;
                    }
                    PrefixControlOperations = new ReadOnlyCollection<PrefixControlOperation>(ops);
                    break;
                }
                case RouterRenumberingCode.RouterRenumberingResult:
                {
                    var matches = new MatchReport[(DataLength - 12) / 24];
                    for(int i = 0, j = matches.Length; i < j; i++)
                        matches[i] = new MatchReport(reader);
                    MatchReports = new ReadOnlyCollection<MatchReport>(matches);
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException("reader", (int)Code, typeof(RouterRenumberingCode));
            }
        }
    }

    /// <summary>
    /// Constants that represent node information query types.
    /// </summary>
    public enum NodeInformationQueryType
    {
        /// <summary>
        /// No Operation
        /// </summary>
        NOOP = 0,

        /// <summary>
        /// Node Domain Names
        /// </summary>
        NodeNames = 2,

        /// <summary>
        /// Node IP6 Addresses
        /// </summary>
        NodeAddresses = 3,

        /// <summary>
        /// Node IP4 Addresses
        /// </summary>
        IP4Addresses = 4
    }

    /// <summary>
    /// Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public abstract class NodeInformationIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the query type.
        /// </summary>
        public NodeInformationQueryType QueryType { get; private set; }

        /// <summary>
        /// Gets the unused 10 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Unused { get; private set; }

        /// <summary>
        /// Gets a value indicating whether G flag is set.
        /// </summary>
        public bool GlobalScope { get; private set; }

        /// <summary>
        /// Gets a value indicating whether S flag is set.
        /// </summary>
        public bool LocalScope { get; private set; }

        /// <summary>
        /// Gets a value indicating whether L flag is set.
        /// </summary>
        public bool LinkLocal { get; private set; }

        /// <summary>
        /// Gets a value indicating whether C flag is set.
        /// </summary>
        public bool IP4Mapped { get; private set; }

        /// <summary>
        /// Gets a value indicating whether A flag is set.
        /// </summary>
        public bool UnicastScope { get; private set; }

        /// <summary>
        /// Gets a value indicating whether T flag is set.
        /// </summary>
        public bool IncompleteSet { get; private set; }

        /// <summary>
        /// Gets the nonce.
        /// </summary>
        public ReadOnlyCollection<byte> Nonce { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        protected NodeInformationIcmp6Header(BinaryReader reader, Icmp6HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            QueryType = (NodeInformationQueryType)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bytes = reader.ReadBytes(2);
            Unused = new ReadOnlyCollection<byte>(new[] { (byte)(bytes[0] >> 6), (byte)((byte)(bytes[0] << 2) + (byte)(bytes[1] >> 6)) });
            GlobalScope = Convert.ToBoolean((byte)((byte)(bytes[1] << 2) >> 7));
            LocalScope = Convert.ToBoolean((byte)((byte)(bytes[1] << 3) >> 7));
            LinkLocal = Convert.ToBoolean((byte)((byte)(bytes[1] << 4) >> 7));
            IP4Mapped = Convert.ToBoolean((byte)((byte)(bytes[1] << 5) >> 7));
            UnicastScope = Convert.ToBoolean((byte)((byte)(bytes[1] << 6) >> 7));
            IncompleteSet = Convert.ToBoolean((byte)((byte)(bytes[1] << 7) >> 7));
            Nonce = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
        }
    }

    /// <summary>
    /// Constants that represent node information query code.
    /// </summary>
    public enum NodeInformationQueryCode
    {
        /// <summary>
        /// IP6 Address
        /// </summary>
        IP6Address = 0,

        /// <summary>
        /// Domain Name
        /// </summary>
        DomainName = 1,

        /// <summary>
        /// IP4 Address
        /// </summary>
        IP4Address = 2
    }

    /// <summary>
    /// Query Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public abstract class QueryNodeInformationIcmp6Header : NodeInformationIcmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new NodeInformationQueryCode Code { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        protected QueryNodeInformationIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.NodeInformationQuery)
        {
            Contract.Requires(reader != null);

            Code = (NodeInformationQueryCode)base.Code;
        }

        /// <summary>
        /// Creates a Query Node Information ICMP6 header.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A Query Node Information ICMP6 header.
        /// </returns>
        public static QueryNodeInformationIcmp6Header Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var code = (NodeInformationQueryCode)reader.ReadByte();
            reader.BaseStream.Position -= 1;
            switch(code)
            {
                case NodeInformationQueryCode.IP6Address:
                case NodeInformationQueryCode.IP4Address:
                    return new AddressQueryNodeInformationIcmp6Header(reader);
                case NodeInformationQueryCode.DomainName:
                    return new NameQueryNodeInformationIcmp6Header(reader);
                default:
                    throw new InvalidEnumArgumentException("reader", (int)code, typeof(NodeInformationQueryCode));
            }
        }
    }

    /// <summary>
    /// Address Query Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public sealed class AddressQueryNodeInformationIcmp6Header : QueryNodeInformationIcmp6Header
    {
        /// <summary>
        /// Gets the subject IP4 or IP6 address.
        /// </summary>
        public IPAddress SubjectAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressQueryNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddressQueryNodeInformationIcmp6Header(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            if(DataLength - 12 > 0)
                SubjectAddress = new IPAddress(reader.ReadBytes(Code == NodeInformationQueryCode.IP4Address ? 4 : 16));
        }
    }

    /// <summary>
    /// Name Query Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public sealed class NameQueryNodeInformationIcmp6Header : QueryNodeInformationIcmp6Header
    {
        /// <summary>
        /// Gets the subject domain name.
        /// </summary>
        public string SubjectName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NameQueryNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NameQueryNodeInformationIcmp6Header(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            if(DataLength - 12 > 0)
                SubjectName = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// Constants that represent node information reply code.
    /// </summary>
    public enum NodeInformationReplyCode
    {
        /// <summary>
        /// Reply Successful
        /// </summary>
        ReplySuccessful = 0,

        /// <summary>
        /// Answer Refused
        /// </summary>
        AnswerRefused = 1,

        /// <summary>
        /// Query Type Unknown
        /// </summary>
        QueryTypeUnknown = 2
    }

    /// <summary>
    /// Reply Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public abstract class ReplyNodeInformationIcmp6Header : NodeInformationIcmp6Header
    {
        /// <summary>
        /// Gets the code.
        /// </summary>
        public new NodeInformationReplyCode Code { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplyNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        protected ReplyNodeInformationIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.NodeInformationReply)
        {
            Contract.Requires(reader != null);

            Code = (NodeInformationReplyCode)base.Code;
        }

        /// <summary>
        /// Creates a Reply Node Information ICMP6 header.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A Reply Node Information ICMP6 header.
        /// </returns>
        /// <exception cref="System.ComponentModel.InvalidEnumArgumentException">reader</exception>
        public static ReplyNodeInformationIcmp6Header Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            reader.ReadBytes(3);
            var query = (NodeInformationQueryType)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            reader.BaseStream.Position -= 5;
            switch(query)
            {
                case NodeInformationQueryType.NOOP:
                    return new NoOpReplyNodeInformationIcmp6Header(reader);
                case NodeInformationQueryType.NodeNames:
                    return new NamesReplyNodeInformationIcmp6Header(reader);
                case NodeInformationQueryType.NodeAddresses:
                case NodeInformationQueryType.IP4Addresses:
                    return new AddressesReplyNodeInformationIcmp6Header(reader);
                default:
                    throw new InvalidEnumArgumentException("reader", (int)query, typeof(NodeInformationQueryType));
            }
        }
    }

    /// <summary>
    /// NoOp Reply Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public sealed class NoOpReplyNodeInformationIcmp6Header : ReplyNodeInformationIcmp6Header
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoOpReplyNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NoOpReplyNodeInformationIcmp6Header(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);
        }
    }

    /// <summary>
    /// Names Reply Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public sealed class NamesReplyNodeInformationIcmp6Header : ReplyNodeInformationIcmp6Header
    {
        /// <summary>
        /// Gets TTL.
        /// </summary>
        public uint TimeToLive { get; private set; }

        /// <summary>
        /// Gets the list of node domain names.
        /// </summary>
        public ReadOnlyCollection<string> NodeNames { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NamesReplyNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public NamesReplyNodeInformationIcmp6Header(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            if(DataLength - 12 == 0)
                return;
            using(var ms = new MemoryStream(reader.ReadBytes(DataLength - 12), false))
            {
                var data = new BinaryReader(ms);
                TimeToLive = NetworkToHostOrder.ToUInt32(data.ReadBytes(4));
                var names = new List<string>();
                while(data.BaseStream.Position < data.BaseStream.Length)
                {
                    var n = DnsMessage.DomainName(data);
                    if(n != null)
                        names.Add(n);
                }
                NodeNames = new ReadOnlyCollection<string>(names.ToArray());
            }
        }
    }

    /// <summary>
    /// Addresses Reply Node Information ICMP6 header (RFC 4620).
    /// </summary>
    public sealed class AddressesReplyNodeInformationIcmp6Header : ReplyNodeInformationIcmp6Header
    {
        /// <summary>
        /// Node Address class.
        /// </summary>
        public sealed class NodeAddress
        {
            /// <summary>
            /// Gets TTL.
            /// </summary>
            public uint TimeToLive { get; private set; }

            /// <summary>
            /// Gets the IP4 or IP6 address.
            /// </summary>
            public IPAddress Address { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="NodeAddress"/> class.
            /// </summary>
            /// <param name="reader">The binary reader.</param>
            /// <param name="ip4"><c>true</c> for IP4, <c>false</c> for IP6.</param>
            public NodeAddress(BinaryReader reader, bool ip4)
            {
                Contract.Requires(reader != null);

                TimeToLive = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                Address = new IPAddress(reader.ReadBytes(ip4 ? 4 : 16));
            }
        }

        /// <summary>
        /// Gets the list of node IP4 or IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<NodeAddress> NodeAddresses { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressesReplyNodeInformationIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public AddressesReplyNodeInformationIcmp6Header(BinaryReader reader) : base(reader)
        {
            Contract.Requires(reader != null);

            bool ip4 = QueryType == NodeInformationQueryType.IP4Addresses;
            var nodes = new NodeAddress[(DataLength - 12) / (ip4 ? 8 : 20)];
            for(int i = 0, j = nodes.Length; i < j; i++)
                nodes[i] = new NodeAddress(reader, ip4);
            NodeAddresses = new ReadOnlyCollection<NodeAddress>(nodes);
        }
    }

    /// <summary>
    /// Constants that represent multicast address record types.
    /// </summary>
    public enum MulticastAddressRecordType
    {
        /// <summary>
        /// The interface has a filter mode of INCLUDE for the specified multicast address.
        /// </summary>
        MODE_IS_INCLUDE = 1,

        /// <summary>
        /// The interface has a filter mode of EXCLUDE for the specified multicast address.
        /// </summary>
        MODE_IS_EXCLUDE = 2,

        /// <summary>
        /// The interface has changed to INCLUDE filter mode for the specified multicast address.
        /// </summary>
        CHANGE_TO_INCLUDE_MODE = 3,

        /// <summary>
        /// The interface has changed to EXCLUDE filter mode for the specified multicast address.
        /// </summary>
        CHANGE_TO_EXCLUDE_MODE = 4,

        /// <summary>
        /// A list of additional sources that the node wishes to listen to, for packets sent to the specified multicast address.
        /// </summary>
        ALLOW_NEW_SOURCES = 5,

        /// <summary>
        /// A list of sources that the node no longer wishes to listen to, for packets sent to the specified multicast address.
        /// </summary>
        BLOCK_OLD_SOURCES = 6
    }

    /// <summary>
    /// Home Agent Address ICMP6 header (RFC 6275).
    /// </summary>
    public abstract class HomeAgentAddressIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Identifier { get; private set; }

        /// <summary>
        /// Gets a value indicating whether R flag is set.
        /// </summary>
        public bool MobileRouterSupport { get; private set; }

        /// <summary>
        /// Gets the reserved 15 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeAgentAddressIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        protected HomeAgentAddressIcmp6Header(BinaryReader reader, Icmp6HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);

            Identifier = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            var bits = reader.ReadByte();
            MobileRouterSupport = Convert.ToBoolean((byte)(bits >> 7));
            Reserved = new ReadOnlyCollection<byte>(new[] { (byte)((byte)(bits << 1) >> 1), reader.ReadByte() });
        }
    }

    /// <summary>
    /// Request Home Agent Address ICMP6 header (RFC 6275).
    /// </summary>
    public sealed class RequestHomeAgentAddressIcmp6Header : HomeAgentAddressIcmp6Header
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestHomeAgentAddressIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public RequestHomeAgentAddressIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.HomeAgentAddressRequest)
        {
            Contract.Requires(reader != null);
        }
    }

    /// <summary>
    /// Reply Home Agent Address ICMP6 header (RFC 6275).
    /// </summary>
    public sealed class ReplyHomeAgentAddressIcmp6Header : HomeAgentAddressIcmp6Header
    {
        /// <summary>
        /// Gets the list of home agent IP6 addresses.
        /// </summary>
        public ReadOnlyCollection<IPAddress> HomeAgentAddresses { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplyHomeAgentAddressIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public ReplyHomeAgentAddressIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.HomeAgentAddressReply)
        {
            Contract.Requires(reader != null);

            var ips = new IPAddress[(DataLength - 4) / 16];
            for(int i = 0, j = ips.Length; i < j; i++)
                ips[i] = new IPAddress(reader.ReadBytes(16));
            HomeAgentAddresses = new ReadOnlyCollection<IPAddress>(ips);
        }
    }

    /// <summary>
    /// Seamoby ICMP6 header (RFC 4065).
    /// </summary>
    public sealed class SeamobyIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the sub type.
        /// </summary>
        public SeamobySubType SubType { get; private set; }

        /// <summary>
        /// Gets the reserved 24 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the mobility options.
        /// </summary>
        public ReadOnlyCollection<byte> MobilityOptions { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeamobyIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public SeamobyIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.Seamoby)
        {
            Contract.Requires(reader != null);

            SubType = (SeamobySubType)reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(3));
            MobilityOptions = new ReadOnlyCollection<byte>(reader.ReadBytes(DataLength - 4));
        }
    }

    /// <summary>
    /// Multicast Router Advertisement ICMP6 header (RFC 4286).
    /// </summary>
    public sealed class MulticastRouterAdvertisementIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets QQI, the querier's query interval.
        /// </summary>
        public ushort QueryInterval { get; private set; }

        /// <summary>
        /// Gets the robustness variable.
        /// </summary>
        public ushort RobustnessVariable { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MulticastRouterAdvertisementIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MulticastRouterAdvertisementIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.MulticastRouterAdvertisement)
        {
            Contract.Requires(reader != null);

            QueryInterval = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            RobustnessVariable = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// Constants that represent locator update operations.
    /// </summary>
    public enum LocatorUpdateOperation
    {
        /// <summary>
        /// Advertisement
        /// </summary>
        Advertisement = 1,

        /// <summary>
        /// Acknowledgment
        /// </summary>
        Acknowledgment = 2
    }

    /// <summary>
    /// Locator Update ICMP6 header (RFC 6743).
    /// </summary>
    public sealed class LocatorUpdateIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the number of the <see cref="Locators"/>.
        /// </summary>
        public byte NumberOfLocators { get; private set; }

        /// <summary>
        /// Gets the operation.
        /// </summary>
        public LocatorUpdateOperation Operation { get; private set; }

        /// <summary>
        /// Gets the reserved 16 bits.
        /// </summary>
        public ReadOnlyCollection<byte> Reserved { get; private set; }

        /// <summary>
        /// Gets the list of locators.
        /// </summary>
        public ReadOnlyCollection<uint> Locators { get; private set; }

        /// <summary>
        /// Gets the list of locator preferences.
        /// </summary>
        public ReadOnlyCollection<ushort> Preferences { get; private set; }

        /// <summary>
        /// Gets the list of locator lifetimes in seconds.
        /// </summary>
        public ReadOnlyCollection<ushort> Lifetimes { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocatorUpdateIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public LocatorUpdateIcmp6Header(BinaryReader reader) : base(reader, Icmp6HeaderType.LocatorUpdate)
        {
            Contract.Requires(reader != null);

            NumberOfLocators = reader.ReadByte();
            Operation = (LocatorUpdateOperation)reader.ReadByte();
            Reserved = new ReadOnlyCollection<byte>(reader.ReadBytes(2));
            var locs = new uint[NumberOfLocators];
            var prefs = new ushort[locs.Length];
            var lifes = new ushort[locs.Length];
            for(int i = 0, j = locs.Length; i < j; i++)
            {
                locs[i] = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
                prefs[i] = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
                lifes[i] = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            }
            Locators = new ReadOnlyCollection<uint>(locs);
            Preferences = new ReadOnlyCollection<ushort>(prefs);
            Lifetimes = new ReadOnlyCollection<ushort>(lifes);
        }
    }

    /// <summary>
    /// Constants that represent duplicate address statuses.
    /// </summary>
    public enum DuplicateAddressStatus
    {
        /// <summary>
        /// Success
        /// </summary>
        Success = 0,

        /// <summary>
        /// Duplicate Address
        /// </summary>
        DuplicateAddress = 1,

        /// <summary>
        /// Neighbor CacheFull
        /// </summary>
        NeighborCacheFull = 2
    }

    /// <summary>
    /// Duplicate Address ICMP6 header (RFC 6775).
    /// </summary>
    public sealed class DuplicateAddressIcmp6Header : Icmp6Header
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        public DuplicateAddressStatus Status { get; private set; }

        /// <summary>
        /// Gets the reserved 8 bits.
        /// </summary>
        public byte Reserved { get; private set; }

        /// <summary>
        /// Gets the registration lifetime in 60-second units.
        /// </summary>
        public ushort RegistrationLifetime { get; private set; }

        /// <summary>
        /// Gets EUI-64.
        /// </summary>
        public ReadOnlyCollection<byte> ExtendedUniqueIdentifier64 { get; private set; }

        /// <summary>
        /// Gets the registered IP6 address.
        /// </summary>
        public IPAddress RegisteredAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateAddressIcmp6Header"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="type">The header type.</param>
        public DuplicateAddressIcmp6Header(BinaryReader reader, Icmp6HeaderType type) : base(reader, type)
        {
            Contract.Requires(reader != null);
            Contract.Requires(type == Icmp6HeaderType.DuplicateAddressRequest || type == Icmp6HeaderType.DuplicateAddressConfirmation);

            Status = (DuplicateAddressStatus)reader.ReadByte();
            Reserved = reader.ReadByte();
            RegistrationLifetime = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            ExtendedUniqueIdentifier64 = new ReadOnlyCollection<byte>(reader.ReadBytes(8));
            RegisteredAddress = new IPAddress(reader.ReadBytes(16));
        }
    }
}