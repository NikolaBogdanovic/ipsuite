﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: CLSCompliant(false)]
[assembly: ComVisible(false)]
[assembly: Guid("9153153a-f826-4a0d-936c-893d182c8dc3")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDefaultAlias("Internet Protocol Suite")]
[assembly: AssemblyTitle("Internet Protocol Suite")]
[assembly: AssemblyProduct("Internet Protocol Suite")]
[assembly: AssemblyDescription("Internet Protocol Suite")]
[assembly: AssemblyCompany("Nikola Bogdanović")]
[assembly: AssemblyCopyright("© 2013-2014 http://rs.linkedin.com/in/NikolaBogdanovic")]
[assembly: AssemblyTrademark("pOcHa")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]