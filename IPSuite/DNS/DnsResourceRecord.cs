﻿namespace IPSuite.DNS
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Net;
    using System.Text;

    using IPSuite.IP4;

    /// <summary>
    /// DNS Resource Record (RFC 1035).
    /// </summary>
    public abstract class DnsResourceRecord
    {
        /// <summary>
        /// Gets the resource record domain name.
        /// </summary>
        public string ResourceRecordName { get; private set; }

        /// <summary>
        /// Gets the resource record type.
        /// </summary>
        public DnsQueryType ResourceRecordType { get; private set; }

        /// <summary>
        /// Gets the resource record class.
        /// </summary>
        public DnsQueryClass ResourceRecordClass { get; private set; }
        
        /// <summary>
        /// Gets the time to live in seconds.
        /// </summary>
        public uint TimeToLive { get; private set; }
        
        /// <summary>
        /// Gets the resource data length in bytes.
        /// </summary>
        public ushort ResourceDataLength { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        protected DnsResourceRecord(BinaryReader reader, DnsQuestion question)
        {
            Contract.Requires(reader != null);

            ResourceRecordName = question.QueryName;
            ResourceRecordType = question.DnsQueryType;
            ResourceRecordClass = question.DnsQueryClass;
            TimeToLive = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            ResourceDataLength = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }

        /// <summary>
        /// Creates a DNS Resource Record.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A DNS Resource Record.
        /// </returns>
        public static DnsResourceRecord Create(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            var question = new DnsQuestion(reader);
            switch(question.DnsQueryType)
            {
                case DnsQueryType.A:
                    return new IP4AddressDnsResourceRecord(reader, question);
                case DnsQueryType.NS:
                case DnsQueryType.MD:
                case DnsQueryType.MF:
                case DnsQueryType.CNAME:
                case DnsQueryType.MB:
                case DnsQueryType.PTR:
                case DnsQueryType.NSAP_PTR:
                    return new DomainNameDnsResourceRecord(reader, question);
                case DnsQueryType.MG:
                case DnsQueryType.MR:
                    return new EmailAddressDnsResourceRecord(reader, question);
                case DnsQueryType.SOA:
                    return new StartOfAuthorityDnsResourceRecord(reader, question);
                case DnsQueryType.WKS:
                    return new WellKnownServiceDnsResourceRecord(reader, question);
                case DnsQueryType.HINFO:
                    return new HostInformationDnsResourceRecord(reader, question);
                case DnsQueryType.MINFO:
                    return new MailInformationDnsResourceRecord(reader, question);
                case DnsQueryType.MX:
                    return new MailExchangeDnsResourceRecord(reader, question);
                case DnsQueryType.TXT:
                case DnsQueryType.SPF:
                    return new TextDnsResourceRecord(reader, question);
                case DnsQueryType.RP:
                    return new ResponsiblePersonDnsResourceRecord(reader, question);
                case DnsQueryType.AFSDB:
                    return new AndrewFileSystemDataBaseDnsResourceRecord(reader, question);
                case DnsQueryType.X25:
                    return new X25AddressDnsResourceRecord(reader, question);
                case DnsQueryType.ISDN:
                    return new IsdnAddressDnsResourceRecord(reader, question);
                case DnsQueryType.RT:
                    return new RouteThroughDnsResourceRecord(reader, question);
                case DnsQueryType.AAAA:
                    return new IP6AddressDnsResourceRecord(reader, question);
                case DnsQueryType.SRV:
                    return new ServerDnsResourceRecord(reader, question);
                default:
                    return new GenericDnsResourceRecord(reader, question);
            }
        }
    }

    /// <summary>
    /// Generic DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class GenericDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets resource data.
        /// </summary>
        public ReadOnlyCollection<byte> ResourceData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public GenericDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);

            ResourceData = new ReadOnlyCollection<byte>(reader.ReadBytes(ResourceDataLength));
        }
    }

    /// <summary>
    /// IP4 Address DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class IP4AddressDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the IP4 address.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP4AddressDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public IP4AddressDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.A);

            Address = new IPAddress(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Domain Name DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class DomainNameDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the domain name.
        /// </summary>
        public string DomainName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainNameDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public DomainNameDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.NS || question.DnsQueryType == DnsQueryType.MD || question.DnsQueryType == DnsQueryType.MF || question.DnsQueryType == DnsQueryType.CNAME || question.DnsQueryType == DnsQueryType.MB || question.DnsQueryType == DnsQueryType.PTR);

            DomainName = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// Email Address DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class EmailAddressDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the email address.
        /// </summary>
        public string EmailAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailAddressDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public EmailAddressDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.MG || question.DnsQueryType == DnsQueryType.MR);

            EmailAddress = DnsMessage.EmailAddress(reader);
        }
    }

    /// <summary>
    /// Start Of Authority DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class StartOfAuthorityDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the master name.
        /// </summary>
        public string MasterName { get; private set; }

        /// <summary>
        /// Gets the responsible name.
        /// </summary>
        public string ResponsibleName { get; private set; }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        public uint SerialNumber { get; private set; }

        /// <summary>
        /// Gets the refresh interval in seconds.
        /// </summary>
        public uint RefreshInterval { get; private set; }

        /// <summary>
        /// Gets the retry interval in seconds.
        /// </summary>
        public uint RetryInterval { get; private set; }

        /// <summary>
        /// Gets the expire interval in seconds.
        /// </summary>
        public uint ExpireInterval { get; private set; }

        /// <summary>
        /// Gets MINIMUM, the negative caching time to live in seconds.
        /// </summary>
        public uint NegativeCachingTimeToLive { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StartOfAuthorityDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public StartOfAuthorityDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.SOA);

            MasterName = DnsMessage.DomainName(reader);
            ResponsibleName = DnsMessage.EmailAddress(reader);
            SerialNumber = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            RefreshInterval = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            RetryInterval = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            ExpireInterval = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
            NegativeCachingTimeToLive = NetworkToHostOrder.ToUInt32(reader.ReadBytes(4));
        }
    }

    /// <summary>
    /// Well Known Service DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class WellKnownServiceDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the IP4 address.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Gets the internet protocol.
        /// </summary>
        public InternetProtocol Protocol { get; private set; }

        /// <summary>
        /// Gets the list of server ports.
        /// </summary>
        public ReadOnlyCollection<byte> Ports { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WellKnownServiceDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public WellKnownServiceDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.WKS);

            Address = new IPAddress(reader.ReadBytes(4));
            Protocol = (InternetProtocol)reader.ReadByte();
            Ports = new ReadOnlyCollection<byte>(reader.ReadBytes(ResourceDataLength - 5));
        }
    }

    /// <summary>
    /// Host Information DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class HostInformationDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets CPU.
        /// </summary>
        public string MachineName { get; private set; }

        /// <summary>
        /// Gets OS.
        /// </summary>
        public string SystemName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HostInformationDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public HostInformationDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.HINFO);

            string[] s = Encoding.UTF8.GetString(reader.ReadBytes(ResourceDataLength)).Split(' ');
            MachineName = s[0];
            SystemName = s[1];
        }
    }

    /// <summary>
    /// Mail Information DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class MailInformationDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the responsible mailbox.
        /// </summary>
        public string ResponsibleMailBox { get; private set; }

        /// <summary>
        /// Gets the error mailbox.
        /// </summary>
        public string ErrorMailBox { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MailInformationDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public MailInformationDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.MINFO);

            ResponsibleMailBox = DnsMessage.EmailAddress(reader);
            ErrorMailBox = DnsMessage.EmailAddress(reader);
        }
    }

    /// <summary>
    /// Mail Exchange DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class MailExchangeDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the resource record preference.
        /// </summary>
        public ushort Preference { get; private set; }

        /// <summary>
        /// Gets the exchange domain name.
        /// </summary>
        public string Exchange { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MailExchangeDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public MailExchangeDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.MX);

            Preference = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Exchange = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// Text DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class TextDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the text.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public TextDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.TXT);

            Text = Encoding.UTF8.GetString(reader.ReadBytes(ResourceDataLength));
        }
    }

    /// <summary>
    /// Responsible Person DNS Resource Record (RFC 1183).
    /// </summary>
    public sealed class ResponsiblePersonDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the mailbox.
        /// </summary>
        public string MailBox { get; private set; }

        /// <summary>
        /// Gets the text.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponsiblePersonDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public ResponsiblePersonDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.RP);

            MailBox = DnsMessage.EmailAddress(reader);
            Text = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// Constants that represent server subtypes.
    /// </summary>
    public enum ServerSubType
    {
        /// <summary>
        /// Andrew FileSystem
        /// </summary>
        AFS = 1,

        /// <summary>
        /// Distributed Computing Environment / Network Computing Architecture
        /// </summary>
        DCE_NCA = 2
    }

    /// <summary>
    /// Andrew FileSystem DataBase DNS Resource Record (RFC 1183, RFC 5864).
    /// </summary>
    public sealed class AndrewFileSystemDataBaseDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the subtype.
        /// </summary>
        public ServerSubType SubType { get; private set; }

        /// <summary>
        /// Gets the host name.
        /// </summary>
        public string HostName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AndrewFileSystemDataBaseDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public AndrewFileSystemDataBaseDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.AFSDB);

            SubType = (ServerSubType)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            HostName = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// X.25 Address DNS Resource Record (RFC 1183).
    /// </summary>
    public sealed class X25AddressDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the Public Switched Data Network address.
        /// </summary>
        public ushort PsdnAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="X25AddressDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public X25AddressDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.X25);

            PsdnAddress = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }

    /// <summary>
    /// ISDN Address DNS Resource Record (RFC 1183).
    /// </summary>
    public sealed class IsdnAddressDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the Integrated Service Digital Network address.
        /// </summary>
        public ushort IsdnAddress { get; private set; }

        /// <summary>
        /// Gets the sub address.
        /// </summary>
        public byte SubAddress { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsdnAddressDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public IsdnAddressDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.ISDN);

            IsdnAddress = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            SubAddress = reader.ReadByte();
        }
    }

    /// <summary>
    /// Route Through DNS Resource Record (RFC 1183).
    /// </summary>
    public sealed class RouteThroughDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the route preference.
        /// </summary>
        public ushort Preference { get; private set; }

        /// <summary>
        /// Gets the intermediate host domain name.
        /// </summary>
        public string IntermediateHost { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteThroughDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public RouteThroughDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.RT);

            Preference = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            IntermediateHost = DnsMessage.DomainName(reader);
        }
    }

    /// <summary>
    /// IP6 Address DNS Resource Record (RFC 1035).
    /// </summary>
    public sealed class IP6AddressDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the IP6 address.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IP6AddressDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public IP6AddressDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.AAAA);

            Address = new IPAddress(reader.ReadBytes(16));
        }
    }

    /// <summary>
    /// Server DNS Resource Record (RFC 2782).
    /// </summary>
    public sealed class ServerDnsResourceRecord : DnsResourceRecord
    {
        /// <summary>
        /// Gets the <see cref="Target"/> priority.
        /// </summary>
        public ushort Priority { get; private set; }

        /// <summary>
        /// Gets the <see cref="Target"/> weight.
        /// </summary>
        public ushort Weight { get; private set; }

        /// <summary>
        /// Gets the <see cref="Target"/> port.
        /// </summary>
        public ushort Port { get; private set; }

        /// <summary>
        /// Gets the target host.
        /// </summary>
        public string Target { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerDnsResourceRecord"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <param name="question">The DNS question.</param>
        public ServerDnsResourceRecord(BinaryReader reader, DnsQuestion question) : base(reader, question)
        {
            Contract.Requires(reader != null);
            Contract.Requires(question != null);
            Contract.Requires(question.DnsQueryType == DnsQueryType.SRV);

            Priority = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Weight = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Port = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            Target = DnsMessage.DomainName(reader);
        }
    }
}