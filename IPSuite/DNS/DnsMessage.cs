﻿namespace IPSuite.DNS
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Domain Name System message (RFC 1035).
    /// </summary>
    public sealed class DnsMessage
    {
        /// <summary>
        /// Gets the header.
        /// </summary>
        public DnsHeader Header { get; private set; }

        /// <summary>
        /// Gets the list of questions.
        /// </summary>
        public ReadOnlyCollection<DnsQuestion> Questions { get; private set; }

        /// <summary>
        /// Gets the list of answer records.
        /// </summary>
        public ReadOnlyCollection<DnsResourceRecord> AnswerRecords { get; private set; }

        /// <summary>
        /// Gets the list of authority records.
        /// </summary>
        public ReadOnlyCollection<DnsResourceRecord> AuthorityRecords { get; private set; }

        /// <summary>
        /// Gets the list of additional records.
        /// </summary>
        public ReadOnlyCollection<DnsResourceRecord> AdditionalRecords { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DnsMessage"/> class.
        /// </summary>
        /// <param name="buffer">The binary buffer.</param>
        public DnsMessage(byte[] buffer)
        {
            Contract.Requires(buffer != null);
            Contract.Requires(buffer.Length >= 12);

            using(var ms = new MemoryStream(buffer, false))
            {
                var reader = new BinaryReader(ms);
                Header = new DnsHeader(reader);
                var qs = new DnsQuestion[Header.QuestionCount];
                for(int i = 0, j = qs.Length; i < j; i++)
                    qs[i] = new DnsQuestion(reader);
                Questions = new ReadOnlyCollection<DnsQuestion>(qs);
                var rr = new DnsResourceRecord[Header.AnswerRecordCount];
                for(int i = 0, j = rr.Length; i < j; i++)
                    rr[i] = DnsResourceRecord.Create(reader);
                AnswerRecords = new ReadOnlyCollection<DnsResourceRecord>(rr);
                rr = new DnsResourceRecord[Header.AuthorityRecordCount];
                for(int i = 0, j = rr.Length; i < j; i++)
                    rr[i] = DnsResourceRecord.Create(reader);
                AuthorityRecords = new ReadOnlyCollection<DnsResourceRecord>(rr);
                rr = new DnsResourceRecord[Header.AdditionalRecordCount];
                for(int i = 0, j = rr.Length; i < j; i++)
                    rr[i] = DnsResourceRecord.Create(reader);
                AdditionalRecords = new ReadOnlyCollection<DnsResourceRecord>(rr);
            }
        }

        /// <summary>
        /// Gets a domain name.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// A domain name.
        /// </returns>
        public static string DomainName(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            byte length = reader.ReadByte();
            if(length == 0)
                return null;
            var sb = new StringBuilder();
            long pos = -1;
            do
            {
                if(length >= 192)
                {
                    ushort ptr = NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(length << 2) >> 2), reader.ReadByte() });
                    pos = reader.BaseStream.Position;
                    reader.BaseStream.Position = ptr;
                    length = reader.ReadByte();
                }
                sb.Append(reader.ReadChars(length));
                sb.Append('.');
                if(pos > -1)
                    reader.BaseStream.Position = pos;
            }
            while((length = reader.ReadByte()) > 0);
            return sb.ToString(0, sb.Length - 1);
        }

        /// <summary>
        /// Gets an email address.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        /// <returns>
        /// An email address.
        /// </returns>
        public static string EmailAddress(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            byte length = reader.ReadByte();
            if(length == 0)
                return null;
            var sb = new StringBuilder();
            bool at = false;
            long pos = -1;
            do
            {
                if(length >= 192)
                {
                    ushort ptr = NetworkToHostOrder.ToUInt16(new[] { (byte)((byte)(length << 2) >> 2), reader.ReadByte() });
                    pos = reader.BaseStream.Position;
                    reader.BaseStream.Position = ptr;
                    length = reader.ReadByte();
                }
                char[] label = reader.ReadChars(length);
                if(!at && length > 0)
                {
                    if(label[length - 1] == '\\')
                    {
                        label[length - 1] = '.';
                        sb.Append(label);
                    }
                    else
                    {
                        sb.Append(label);
                        sb.Append('@');
                        at = true;
                    }
                }
                else
                {
                    sb.Append(label);
                    sb.Append('.');
                }
                if(pos > -1)
                    reader.BaseStream.Position = pos;
            }
            while((length = reader.ReadByte()) > 0);
            return sb.ToString(0, sb.Length - 1);
        }
    }
}