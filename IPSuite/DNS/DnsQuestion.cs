﻿namespace IPSuite.DNS
{
    using System.Diagnostics.Contracts;
    using System.IO;

    /// <summary>
    /// Constants that represent DNS query types.
    /// </summary>
    public enum DnsQueryType
    {
        /// <summary>
        /// IP4 Address (RFC 1035)
        /// </summary>
        A = 1,

        /// <summary>
        /// Name Server (RFC 1035)
        /// </summary>
        NS = 2,

        /// <summary>
        /// Mail Destination (RFC 1035)
        /// </summary>
        MD = 3,

        /// <summary>
        /// Mail Forwarder (RFC 1035)
        /// </summary>
        MF = 4,

        /// <summary>
        /// Canonical Name (RFC 1035)
        /// </summary>
        CNAME = 5,

        /// <summary>
        /// Start Of Authority (RFC 1035)
        /// </summary>
        SOA = 6,

        /// <summary>
        /// MailBox (RFC 1035)
        /// </summary>
        MB = 7,

        /// <summary>
        /// Mail Group (RFC 1035)
        /// </summary>
        MG = 8,

        /// <summary>
        /// Mail Rename (RFC 1035)
        /// </summary>
        MR = 9,

        /// <summary>
        /// Null (RFC 1035)
        /// </summary>
        NULL = 10,

        /// <summary>
        /// Well Known Service (RFC 1035)
        /// </summary>
        WKS = 11,

        /// <summary>
        /// Pointer (RFC 1035)
        /// </summary>
        PTR = 12,

        /// <summary>
        /// Host Information (RFC 1035)
        /// </summary>
        HINFO = 13,

        /// <summary>
        /// Mail Information (RFC 1035)
        /// </summary>
        MINFO = 14,

        /// <summary>
        /// Mail Exchange (RFC 1035)
        /// </summary>
        MX = 15,

        /// <summary>
        /// Text (RFC 1035)
        /// </summary>
        TXT = 16,

        /// <summary>
        /// Responsible Person (RFC 1183)
        /// </summary>
        RP = 17,

        /// <summary>
        /// Andrew File System Data Base (RFC 1183, RFC 5864)
        /// </summary>
        AFSDB = 18,

        /// <summary>
        /// X.25 Public Switched Data Network Address (RFC 1183)
        /// </summary>
        X25 = 19,

        /// <summary>
        /// Integrated Services Digital Network Address (RFC 1183)
        /// </summary>
        ISDN = 20,

        /// <summary>
        /// Route Through (RFC 1183)
        /// </summary>
        RT = 21,

        /// <summary>
        /// Network Service Access Point Address (RFC 1706)
        /// </summary>
        NSAP = 22,

        /// <summary>
        /// Network Service Access Point Pointer (RFC 1348, RFC 1637, RFC 1706)
        /// </summary>
        NSAP_PTR = 23,

        /// <summary>
        /// Security Signature (RFC 4034, RFC 3755, RFC 2535, RFC 2536, RFC 2537, RFC 2931, RFC 3110, RFC 3008)
        /// </summary>
        SIG = 24,

        /// <summary>
        /// Security Key (RFC 4034, RFC 3755, RFC 2535, RFC 2536, RFC 2537, RFC 2539, RFC 3008, RFC 3110)
        /// </summary>
        KEY = 25,

        /// <summary>
        /// Pointer to X.400 mail mapping information (RFC 2163)
        /// </summary>
        PX = 26,

        /// <summary>
        /// Geographical Position (RFC 1712)
        /// </summary>
        GPOS = 27,

        /// <summary>
        /// IP6 Address (RFC 3596)
        /// </summary>
        AAAA = 28,

        /// <summary>
        /// Location (RFC 1876)
        /// </summary>
        LOC = 29,

        /// <summary>
        /// Next (RFC 3755, RFC 2535)
        /// </summary>
        NXT = 30,

        /// <summary>
        /// Endpoint Identifier
        /// </summary>
        EID = 31,

        /// <summary>
        /// Nimrod Locator
        /// </summary>
        NIMLOC = 32,

        /// <summary>
        /// Server (RFC 2782)
        /// </summary>
        SRV = 33,

        /// <summary>
        /// Asynchronous Transfer Mode Address
        /// </summary>
        ATMA = 34,

        /// <summary>
        /// Naming Authority Pointer (RFC 2915, RFC 2168, RFC 3403)
        /// </summary>
        NAPTR = 35,

        /// <summary>
        /// Key Exchanger (RFC 2230)
        /// </summary>
        KX = 36,

        /// <summary>
        /// CERT
        /// </summary>
        CERT = 37,

        /// <summary>
        /// IP6 Address Aggregation and Renumbering (RFC 3226, RFC 2874, RFC 6563)
        /// </summary>
        A6 = 38,

        /// <summary>
        /// Domain Name (RFC 6672)
        /// </summary>
        DNAME = 39,

        /// <summary>
        /// SINK
        /// </summary>
        SINK = 40,

        /// <summary>
        /// Option (RFC 6891, RFC 3225)
        /// </summary>
        OPT = 41,

        /// <summary>
        /// Address Prefix List (RFC 3123)
        /// </summary>
        APL = 42,

        /// <summary>
        /// Delegation Signer (RFC 4034, RFC 3658)
        /// </summary>
        DS = 43,

        /// <summary>
        /// SSH Key Fingerprint (RFC 4255)
        /// </summary>
        SSHFP = 44,

        /// <summary>
        /// IPsec Key (RFC 4025)
        /// </summary>
        IPSECKEY = 45,

        /// <summary>
        /// Resource Record Signature (RFC 4034, RFC 3755)
        /// </summary>
        RRSIG = 46,

        /// <summary>
        /// Authenticated Denial of Existence (RFC 4034, RFC 3755)
        /// </summary>
        NSEC = 47,

        /// <summary>
        /// Public Key (RFC 4034, RFC 3755)
        /// </summary>
        DNSKEY = 48,

        /// <summary>
        /// Dynamic Host Configuration Identifier (RFC 4701)
        /// </summary>
        DHCID = 49,

        /// <summary>
        /// Hashed Authenticated Denial of Existence (RFC 5155)
        /// </summary>
        NSEC3 = 50,

        /// <summary>
        /// Hashed Authenticated Denial of Existence Parameters (RFC 5155)
        /// </summary>
        NSEC3PARAM = 51,

        /// <summary>
        /// Transport Layer Security Authentication (RFC 6698)
        /// </summary>
        TLSA = 52,

        /// <summary>
        /// Host Identity Protocol (RFC 5205)
        /// </summary>
        HIP = 55,

        /// <summary>
        /// NINFO
        /// </summary>
        NINFO = 56,

        /// <summary>
        /// RKEY
        /// </summary>
        RKEY = 57,

        /// <summary>
        /// Trust Anchor LINK
        /// </summary>
        TALINK = 58,

        /// <summary>
        /// Child DS
        /// </summary>
        CDS = 59,

        /// <summary>
        /// Sender Policy Framework
        /// </summary>
        SPF = 99,

        /// <summary>
        /// UINFO
        /// </summary>
        UINFO = 100,

        /// <summary>
        /// UID
        /// </summary>
        UID = 101,

        /// <summary>
        /// GID
        /// </summary>
        GID = 102,

        /// <summary>
        /// UNSPEC
        /// </summary>
        UNSPEC = 103,

        /// <summary>
        /// Node Identifier (RFC 6742)
        /// </summary>
        NID = 104,

        /// <summary>
        /// Locator 32-bit (RFC 6742)
        /// </summary>
        L32 = 105,

        /// <summary>
        /// Locator 64-bit (RFC 6742)
        /// </summary>
        L64 = 106,

        /// <summary>
        /// Locator Pointer (RFC 6742)
        /// </summary>
        LP = 107,

        /// <summary>
        /// Extended Unique Identifier 48-bit (RFC 7043)
        /// </summary>
        EUI48 = 108,

        /// <summary>
        /// Extended Unique Identifier 64-bit (RFC 7043)
        /// </summary>
        EUI64 = 109,

        /// <summary>
        /// Transaction Key (RFC 2930)
        /// </summary>
        TKEY = 249,

        /// <summary>
        /// Transaction Signature (RFC 2845)
        /// </summary>
        TSIG = 250,

        /// <summary>
        /// Incremental Transfer (RFC 1995)
        /// </summary>
        IXFR = 251,

        /// <summary>
        /// Authoritative Transfer (RFC 1035, RFC 5936)
        /// </summary>
        AXFR = 252,

        /// <summary>
        /// MailBox-Related (RFC 1035)
        /// </summary>
        MAILB = 253,

        /// <summary>
        /// Mail Agent (RFC 1035)
        /// </summary>
        MAILA = 254,

        /// <summary>
        /// All (RFC 1035, RFC 6895)
        /// </summary>
        ALL = 255,

        /// <summary>
        /// Uniform Resource Identifier
        /// </summary>
        URI = 256,

        /// <summary>
        /// Certification Authority Restriction (RFC 6844)
        /// </summary>
        CAA = 257,

        /// <summary>
        /// Trust Authorities
        /// </summary>
        TA = 32768,

        /// <summary>
        /// Look-aside Validation (RFC 4431)
        /// </summary>
        DLV = 32769
    }

    /// <summary>
    /// Constants that represent DNS query classes.
    /// </summary>
    public enum DnsQueryClass
    {
        /// <summary>
        /// Internet
        /// </summary>
        IN = 1,

        /// <summary>
        /// CSNET
        /// </summary>
        CS = 2,

        /// <summary>
        /// Chaos
        /// </summary>
        CH = 3,

        /// <summary>
        /// Hesiod
        /// </summary>
        HS = 4,

        /// <summary>
        /// None
        /// </summary>
        NONE = 254,

        /// <summary>
        /// Any
        /// </summary>
        ANY = 255
    }

    /// <summary>
    /// DNS Question (RFC 1035).
    /// </summary>
    public sealed class DnsQuestion
    {
        /// <summary>
        /// Gets the query domain name.
        /// </summary>
        public string QueryName { get; private set; }

        /// <summary>
        /// Gets the query type.
        /// </summary>
        public DnsQueryType DnsQueryType { get; private set; }

        /// <summary>
        /// Gets the query class.
        /// </summary>
        public DnsQueryClass DnsQueryClass { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DnsQuestion"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DnsQuestion(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            QueryName = DnsMessage.DomainName(reader);
            DnsQueryType = (DnsQueryType)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            DnsQueryClass = (DnsQueryClass)NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }
}