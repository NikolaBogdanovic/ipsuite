﻿namespace IPSuite.DNS
{
    using System;
    using System.Diagnostics.Contracts;
    using System.IO;

    /// <summary>
    /// Constants that represent DNS operation codes.
    /// </summary>
    public enum DnsOperationCode
    {
        /// <summary>
        /// Query
        /// </summary>
        Query = 0,

        /// <summary>
        /// Inverse Query
        /// </summary>
        IQuery = 1,

        /// <summary>
        /// Status
        /// </summary>
        Status = 2,

        /// <summary>
        /// Notify
        /// </summary>
        Notify = 4,

        /// <summary>
        /// Update
        /// </summary>
        Update = 5
    }

    /// <summary>
    /// Constants that represent DNS response codes.
    /// </summary>
    public enum DnsResponseCode
    {
        /// <summary>
        /// No Error
        /// </summary>
        NoError = 0,

        /// <summary>
        /// Format Error
        /// </summary>
        FormErr = 1,

        /// <summary>
        /// Server Failure
        /// </summary>
        ServFail = 2,

        /// <summary>
        /// Non-Existent Domain
        /// </summary>
        NXDomain = 3,

        /// <summary>
        /// Not Implemented
        /// </summary>
        NotImp = 4,

        /// <summary>
        /// Query Refused
        /// </summary>
        Refused = 5,

        /// <summary>
        /// Name Exists when it should not.
        /// </summary>
        YXDomain = 6,

        /// <summary>
        /// RR Set Exists when it should not.
        /// </summary>
        YXRRSet = 7,

        /// <summary>
        /// RR Set Non-Existent when it should.
        /// </summary>
        NXRRSet = 8,

        /// <summary>
        /// Server Not Authoritative for zone.
        /// </summary>
        SrvNotAuth = 9,

        /// <summary>
        /// Not Authorized
        /// </summary>
        NotAuth = 9,

        /// <summary>
        /// Name not contained in zone.
        /// </summary>
        NotZone = 10,

        /// <summary>
        /// Bad OPT Version
        /// </summary>
        BadVers = 16,

        /// <summary>
        /// TSIG Signature Failure
        /// </summary>
        BadSig = 16,

        /// <summary>
        /// Key not recognized.
        /// </summary>
        BadKey = 17,

        /// <summary>
        /// Signature out of time window.
        /// </summary>
        BadTime = 18,

        /// <summary>
        /// Bad TKEY Mode
        /// </summary>
        BadMode = 19,

        /// <summary>
        /// Duplicate key name.
        /// </summary>
        BadName = 20,

        /// <summary>
        /// Algorithm not supported.
        /// </summary>
        BadAlg = 21,

        /// <summary>
        /// Bad Truncation
        /// </summary>
        BadTrunc = 22
    }

    /// <summary>
    /// Domain Name System header (RFC 1035).
    /// </summary>
    public sealed class DnsHeader
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public ushort Id { get; private set; }

        /// <summary>
        /// Gets a value indicating whether QR flag is set.
        /// </summary>
        public bool QueryResponse { get; private set; }

        /// <summary>
        /// Gets OpCode.
        /// </summary>
        public DnsOperationCode DnsOperationCode { get; private set; }

        /// <summary>
        /// Gets a value indicating whether AA flag is set.
        /// </summary>
        public bool AuthoritativeAnswer { get; private set; }

        /// <summary>
        /// Gets a value indicating whether TC flag is set.
        /// </summary>
        public bool TruncatedResponse { get; private set; }

        /// <summary>
        /// Gets a value indicating whether RD flag is set.
        /// </summary>
        public bool RecursionDesired { get; private set; }

        /// <summary>
        /// Gets a value indicating whether RA flag is set.
        /// </summary>
        public bool RecursionAllowed { get; private set; }
        
        /// <summary>
        /// Gets a value indicating whether Z flag is set.
        /// </summary>
        public bool Zero { get; private set; }

        /// <summary>
        /// Gets a value indicating whether AD flag is set.
        /// </summary>
        public bool AuthenticData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether CD flag is set.
        /// </summary>
        public bool CheckingDisabled { get; private set; }

        /// <summary>
        /// Gets RCode.
        /// </summary>
        public DnsResponseCode DnsResponseCode { get; private set; }

        /// <summary>
        /// Gets QDCount.
        /// </summary>
        public ushort QuestionCount { get; private set; }

        /// <summary>
        /// Gets ANCount.
        /// </summary>
        public ushort AnswerRecordCount { get; private set; }

        /// <summary>
        /// Gets NSCount.
        /// </summary>
        public ushort AuthorityRecordCount { get; private set; }

        /// <summary>
        /// Gets ARCount.
        /// </summary>
        public ushort AdditionalRecordCount { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DnsHeader"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public DnsHeader(BinaryReader reader)
        {
            Contract.Requires(reader != null);

            Id = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            byte bits = reader.ReadByte();
            QueryResponse = Convert.ToBoolean((byte)(bits >> 7));
            DnsOperationCode = (DnsOperationCode)(byte)((byte)(bits << 1) >> 4);
            AuthoritativeAnswer = Convert.ToBoolean((byte)((byte)(bits << 5) >> 7));
            TruncatedResponse = Convert.ToBoolean((byte)((byte)(bits << 6) >> 7));
            RecursionDesired = Convert.ToBoolean((byte)((byte)(bits << 7) >> 7));
            bits = reader.ReadByte();
            RecursionAllowed = Convert.ToBoolean((byte)(bits >> 7));
            Zero = Convert.ToBoolean((byte)((byte)(bits << 1) >> 7));
            AuthenticData = Convert.ToBoolean((byte)((byte)(bits << 2) >> 7));
            CheckingDisabled = Convert.ToBoolean((byte)((byte)(bits << 3) >> 7));
            DnsResponseCode = (DnsResponseCode)(byte)((byte)(bits << 4) >> 4);
            QuestionCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            AnswerRecordCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            AuthorityRecordCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
            AdditionalRecordCount = NetworkToHostOrder.ToUInt16(reader.ReadBytes(2));
        }
    }
}