Internet Protocol Suite

Wiki: https://bitbucket.org/NikolaBogdanovic/ipsuite/wiki/

Help: http://NikolaBogdanovic.bitbucket.org/IPSuite/

namespace ExampleUsage
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    using IPSuite.DNS;
    using IPSuite.ICMP4;
    using IPSuite.IP4;
    using IPSuite.TCP;
    using IPSuite.UDP;

    internal static class SocketSniffer
    {
        internal static void ReceiveAll()
        {
            Socket socket = null;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.IP)
                {
                    ExclusiveAddressUse = false,
                    SendBufferSize = 1372,
                    ReceiveBufferSize = 65535
                };
                socket.Bind(new IPEndPoint(IPAddress.Loopback, 12345));
                socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.HeaderIncluded, true);
                socket.IOControl(IOControlCode.ReceiveAll, new byte[] { 1, 0, 0, 0 }, new byte[] { 1, 0, 0, 0 });
                Task previous = null;
                while(true)
                {
                    var buffer = new byte[socket.ReceiveBufferSize];
                    var count = (ushort)socket.Receive(buffer, 0, buffer.Length, SocketFlags.None);
                    if(previous == null)
                        previous = Task.Factory.StartNew(
                            state =>
                            {
                                var tuple = (Tuple<byte[], ushort>)state;
                                Payload(tuple.Item1, tuple.Item2);
                            },
                            new Tuple<byte[], ushort>(buffer, count),
                            TaskCreationOptions.PreferFairness);
                    else
                        previous = previous.ContinueWith(task => Payload(buffer, count), TaskContinuationOptions.NotOnFaulted);
                    previous.ContinueWith(task => Console.WriteLine(task.Exception.ToString()), TaskContinuationOptions.OnlyOnFaulted);
                }
            }
            finally
            {
                if(socket != null)
                {
                    using(socket)
                        socket.Shutdown(SocketShutdown.Both);
                }
            }
        }

        private static void Payload(byte[] buffer, ushort count)
        {
            using(var ms = new MemoryStream(buffer, 0, count, false))
            {
                var reader = new BinaryReader(ms);
                while(reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    var ip = new IP4Header(reader);
                    switch(ip.Protocol)
                    {
                        case InternetProtocol.IP4:
                            continue;
                        case InternetProtocol.ICMP4:
                        {
                            var icmp = Icmp4Header.Create(reader.ReadBytes(ip.PayloadLength));
                            // TODO
                            break;
                        }
                        case InternetProtocol.TCP:
                        {
                            var tcp = new TcpHeader(reader, ip.PayloadLength);
                            if(tcp.SourcePort == 12345 || tcp.DestinationPort == 12345)
                            {
                                var data = reader.ReadBytes((ushort)Math.Min(65535, tcp.JumboPayloadLength));
                                // TODO
                            }
                            else if(tcp.SourcePort == 53 || tcp.DestinationPort == 53)
                            {
                                var dns = new DnsMessage(reader.ReadBytes((ushort)Math.Min(65535, tcp.JumboPayloadLength)));
                                // TODO
                            }
                            else
                                reader.BaseStream.Position += Math.Min(65535, tcp.JumboPayloadLength);
                            break;
                        }
                        case InternetProtocol.UDP:
                        {
                            var udp = new UdpHeader(reader, ip.PayloadLength);
                            if(udp.SourcePort == 12345 || udp.DestinationPort == 12345)
                            {
                                var data = reader.ReadBytes((ushort)Math.Min(65535, udp.JumboPayloadLength));
                                // TODO
                            }
                            else if(udp.SourcePort == 53 || udp.DestinationPort == 53)
                            {
                                var dns = new DnsMessage(reader.ReadBytes((ushort)Math.Min(65535, udp.JumboPayloadLength)));
                                // TODO
                            }
                            else
                                reader.BaseStream.Position += Math.Min(65535, udp.JumboPayloadLength);
                            break;
                        }
                        default:
                        {
                            reader.BaseStream.Position += ip.PayloadLength;
                            break;
                        }
                    }
                }
            }
        }
    }
}